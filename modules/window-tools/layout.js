"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var LAYOUT_MARGIN = 0;
var LAYOUT_GUTTER_WIDTH = 1; //Unused
var LAYOUT_CLASS_SECTION = "layout-section";
var LAYOUT_CLASS_ROW = "layout-row";
var LAYOUT_CLASS_SECTION_GUTTER = "layout-section-gutter";
var LAYOUT_CLASS_ROW_GUTTER = "layout-row-gutter";
var LAYOUT_CLASS_CONTEXT = "context";
var pointer_1 = require("./pointer");
var layout_manager_1 = require("./layout-manager");
var dom_object_1 = require("./dom-object");
var view_1 = require("./view");
var Layout = /** @class */ (function (_super) {
    __extends(Layout, _super);
    function Layout(container) {
        var _this = _super.call(this, null, container) || this;
        _this.layoutClientSize = { width: 0, height: 0 };
        _this.container = container;
        layout_manager_1.LayoutManager.SetupLayoutReference(_this);
        _this.SetupEventListeners();
        return _this;
    }
    Layout.prototype.Init = function () {
        dom_object_1.HTMLRepresentation.RemoveChildElements(this.container);
        var row = dom_object_1.HTMLRepresentation.CreateHTMLElement(LAYOUT_CLASS_ROW, this.container);
        this.FitRow(row);
        var section = dom_object_1.HTMLRepresentation.CreateHTMLElement(LAYOUT_CLASS_SECTION, row);
        this.FitSection(section);
        this.SetLayoutClientMinSize();
        this.UpdateLayoutClientSize();
        return this.CreateContext(section);
    };
    Layout.prototype.SetupEventListeners = function () {
        var _this = this;
        window.addEventListener("load", function (e) {
        });
        window.addEventListener("resize", function (e) {
            e.preventDefault();
            _this.Update();
        });
    };
    Layout.prototype.CreateContext = function (section) {
        return dom_object_1.HTMLRepresentation.CreateHTMLElement(LAYOUT_CLASS_CONTEXT, section);
    };
    Layout.prototype.GetContext = function (section) {
        var context = section.querySelector("." + LAYOUT_CLASS_CONTEXT);
        return context;
    };
    Layout.prototype.CloneContext = function (fromSection, toSection) {
        var contextToClone = this.GetContext(fromSection);
        var contextCloned = null;
        if (contextToClone) {
            contextCloned = contextToClone.cloneNode(true);
            toSection.appendChild(contextCloned);
        }
        return contextCloned;
    };
    Layout.prototype.moveContext = function (fromSection, toSection) {
        var contextToMove = this.GetContext(fromSection);
        if (contextToMove) {
            toSection.appendChild(contextToMove);
        }
        return contextToMove;
    };
    Layout.prototype.FitSection = function (element) {
        var container = element.parentNode;
        var previousElement = element.previousSibling;
        var nextElement = element.nextSibling;
        if (previousElement)
            element.style.left = previousElement.offsetLeft + previousElement.clientWidth + LAYOUT_MARGIN + "px";
        else
            element.style.left = LAYOUT_MARGIN + "px";
        if (nextElement)
            element.style.width = nextElement.offsetLeft - (previousElement !== null ? (previousElement.offsetLeft + previousElement.clientWidth) : 0) - 2 * LAYOUT_MARGIN + "px";
        else
            element.style.width = container.clientWidth - (previousElement !== null ? previousElement.offsetLeft + previousElement.clientWidth : 0) - 2 * LAYOUT_MARGIN + "px";
        element.style.top = LAYOUT_MARGIN + "px";
        element.style.height = container.clientHeight - 2 * LAYOUT_MARGIN + "px";
    };
    Layout.prototype.FitRow = function (element) {
        var container = element.parentNode;
        var previousElement = element.previousSibling;
        var nextElement = element.nextSibling;
        if (previousElement)
            element.style.top = previousElement.offsetTop + previousElement.clientHeight + LAYOUT_MARGIN + "px";
        else
            element.style.top = LAYOUT_MARGIN + "px";
        if (nextElement)
            element.style.height = nextElement.offsetTop - (previousElement !== null ? (previousElement.offsetTop + previousElement.clientHeight) : 0) - 2 * LAYOUT_MARGIN + "px";
        else
            element.style.height = container.clientHeight - (previousElement !== null ? (previousElement.offsetTop + previousElement.clientHeight) : 0) - 2 * LAYOUT_MARGIN + "px";
        element.style.left = LAYOUT_MARGIN + "px";
        element.style.width = container.clientWidth - 2 * LAYOUT_MARGIN + "px";
    };
    Layout.prototype.CreateSectionGutter = function (section) {
        var row = section.parentNode;
        var gutter = dom_object_1.HTMLRepresentation.CreateHTMLElement(LAYOUT_CLASS_SECTION_GUTTER, row, section);
        gutter.style.cursor = "col-resize";
        gutter.style.left = pointer_1.oddRound(section.offsetLeft + section.clientWidth / 2.0 - gutter.clientWidth / 2) + "px";
        gutter.style.height = section.clientHeight + 2 * LAYOUT_MARGIN + "px";
        layout_manager_1.LayoutManager.ObserveViewObject(layout_manager_1.LayoutManager.WrapHTML(gutter));
        var properties = {
            position: row.clientWidth / gutter.offsetLeft
        };
        return gutter;
    };
    Layout.prototype.UpdateSectionGutter = function (gutter, offsetLeft) {
        var section = gutter.parentNode;
        gutter.style.height = section.clientHeight + 2 * LAYOUT_MARGIN + "px";
        if (offsetLeft)
            gutter.style.left = pointer_1.oddRound(offsetLeft) + "px";
    };
    Layout.prototype.SplitSection = function (section) {
        var row = section.parentNode;
        if (row && row.className == LAYOUT_CLASS_ROW) {
            var gutter = this.CreateSectionGutter(section);
            var newSection = dom_object_1.HTMLRepresentation.CreateHTMLElement(LAYOUT_CLASS_SECTION, row, gutter);
            this.CreateContext(newSection);
            this.FitSection(section);
            this.FitSection(newSection);
            return newSection;
        }
    };
    Layout.prototype.CreateRowGutter = function (section) {
        var row = section.parentNode;
        var container = row.parentNode;
        var gutter = dom_object_1.HTMLRepresentation.CreateHTMLElement(LAYOUT_CLASS_ROW_GUTTER, container, row);
        gutter.style.cursor = "row-resize";
        gutter.style.top = pointer_1.oddRound(row.offsetTop + row.clientHeight / 2.0 - gutter.clientHeight / 2) + "px";
        gutter.style.width = container.clientWidth + "px";
        layout_manager_1.LayoutManager.ObserveViewObject(layout_manager_1.LayoutManager.WrapHTML(gutter));
        var properties = {
            position: container.clientHeight / gutter.offsetTop
        };
        return gutter;
    };
    Layout.prototype.UpdateRowGutter = function (gutter, offsetTop) {
        var row = gutter.parentNode;
        gutter.style.width = row.clientWidth + "px";
        if (offsetTop)
            gutter.style.top = pointer_1.oddRound(offsetTop) + "px";
    };
    Layout.prototype.SplitRow = function (section) {
        var row = section.parentNode;
        if (row && row.className == LAYOUT_CLASS_ROW) {
            var container = row.parentNode;
            if (row.querySelector("." + LAYOUT_CLASS_SECTION_GUTTER) === null) {
                var gutter = this.CreateRowGutter(section);
                var newRow = dom_object_1.HTMLRepresentation.CreateHTMLElement(LAYOUT_CLASS_ROW, container, gutter);
                var rowSection = dom_object_1.HTMLRepresentation.CreateHTMLElement(LAYOUT_CLASS_SECTION, newRow);
                this.CreateContext(rowSection);
                this.FitRow(row);
                this.FitSection(section);
                this.FitRow(newRow);
                this.FitSection(rowSection);
                return rowSection;
            }
            else {
                var newRow = dom_object_1.HTMLRepresentation.CreateHTMLElement(LAYOUT_CLASS_ROW, section);
                var rowSection = dom_object_1.HTMLRepresentation.CreateHTMLElement(LAYOUT_CLASS_SECTION, newRow);
                this.moveContext(section, rowSection);
                this.FitRow(newRow);
                this.FitSection(rowSection);
                return this.SplitRow(rowSection);
            }
        }
    };
    Layout.prototype.UpdateElement = function (element) {
        var elements = element.childNodes;
        for (var idx = 0; idx < elements.length; idx++) {
            var currentElement = elements[idx];
            switch (currentElement.className) {
                case LAYOUT_CLASS_ROW: {
                    this.FitRow(currentElement);
                    this.UpdateElement(currentElement);
                    break;
                }
                case LAYOUT_CLASS_SECTION: {
                    this.FitSection(currentElement);
                    this.UpdateElement(currentElement);
                    break;
                }
                case LAYOUT_CLASS_ROW_GUTTER: {
                    this.UpdateRowGutter(currentElement);
                    break;
                }
                case LAYOUT_CLASS_SECTION_GUTTER: {
                    this.UpdateSectionGutter(currentElement);
                    break;
                }
                case LAYOUT_CLASS_CONTEXT: {
                    return;
                }
            }
        }
    };
    Layout.prototype.GetRowMinWithBeforeGutter = function (row, gutter) {
        var children = row.childNodes;
        var minWidth = 0;
        for (var idx = 0; idx < children.length; idx++) {
            var element = children[idx];
            if (gutter === element) {
                return minWidth;
            }
            if (element.className === LAYOUT_CLASS_SECTION) {
                minWidth += this.GetSectionMinWidth(element);
            }
            if (element.className === LAYOUT_CLASS_SECTION_GUTTER) {
                minWidth += element.clientWidth;
            }
        }
        return minWidth;
    };
    Layout.prototype.GetRowMinWithAfterGutter = function (row, gutter) {
        var children = row.childNodes;
        var minWidth = 0;
        for (var idx = children.length - 1; idx >= 0; idx--) {
            var element = children[idx];
            if (gutter === element) {
                return minWidth;
            }
            if (element.className === LAYOUT_CLASS_SECTION) {
                minWidth += this.GetSectionMinWidth(element);
            }
            if (element.className === LAYOUT_CLASS_SECTION_GUTTER) {
                minWidth += element.clientWidth;
            }
        }
        return minWidth;
    };
    Layout.prototype.GetSectionMinWidth = function (section) {
        var children = section.childNodes;
        var minWidth = 0;
        for (var idx = 0; idx < children.length; idx++) {
            var element = children[idx];
            switch (element.className) {
                case LAYOUT_CLASS_ROW: {
                    minWidth = Math.max(minWidth, this.GetRowMinWithBeforeGutter(element));
                    break;
                }
                case LAYOUT_CLASS_CONTEXT: {
                    return parseInt(window.getComputedStyle(element).minWidth);
                }
            }
        }
        return minWidth;
    };
    Layout.prototype.GetSectionMinHeightBeforeGutter = function (section, gutter) {
        var children = section.childNodes;
        var minHeight = 0;
        for (var idx = 0; idx < children.length; idx++) {
            var element = children[idx];
            if (element === gutter) {
                return minHeight;
            }
            switch (element.className) {
                case LAYOUT_CLASS_ROW: {
                    minHeight += this.GetRowMinHeight(element);
                    break;
                }
                case LAYOUT_CLASS_ROW_GUTTER: {
                    minHeight += element.clientHeight;
                    break;
                }
                case LAYOUT_CLASS_CONTEXT: {
                    return parseInt(window.getComputedStyle(element).minHeight);
                }
            }
        }
        return minHeight;
    };
    Layout.prototype.GetSectionMinHeightAfterGutter = function (section, gutter) {
        var children = section.childNodes;
        var minHeight = 0;
        for (var idx = children.length - 1; idx >= 0; idx--) {
            var element = children[idx];
            if (element === gutter) {
                return minHeight;
            }
            switch (element.className) {
                case LAYOUT_CLASS_ROW: {
                    minHeight += this.GetRowMinHeight(element);
                    break;
                }
                case LAYOUT_CLASS_ROW_GUTTER: {
                    minHeight += element.clientHeight;
                    break;
                }
                case LAYOUT_CLASS_CONTEXT: {
                    return parseInt(window.getComputedStyle(element).minHeight);
                }
            }
        }
        return minHeight;
    };
    Layout.prototype.GetRowMinHeight = function (row) {
        var children = row.childNodes;
        var minHeight = 0;
        for (var idx = 0; idx < children.length; idx++) {
            var element = children[idx];
            switch (element.className) {
                case LAYOUT_CLASS_SECTION: {
                    minHeight = Math.max(minHeight, this.GetSectionMinHeightAfterGutter(element));
                    break;
                }
            }
        }
        return minHeight;
    };
    Layout.prototype.GetLayoutMinWidth = function () {
        return this.GetSectionMinWidth(this.container);
    };
    Layout.prototype.GetLayoutMinHeight = function () {
        return this.GetSectionMinHeightAfterGutter(this.container);
    };
    Layout.prototype.SetLayoutClientMinSize = function () {
        this.container.style.minWidth = this.GetLayoutMinWidth() + "px";
        this.container.style.minHeight = this.GetLayoutMinHeight() + "px";
    };
    Layout.prototype.OnDragStart = function (gutter) {
        var HTMLRep = gutter.HTMLRepresentation;
        delete this.pointer;
        if (!this.pointer) {
            this.pointer = new pointer_1.Pointer();
            this.pointer.InitBuffer(HTMLRep.offsetLeft, HTMLRep.offsetTop);
        }
    };
    ;
    Layout.prototype.UpdateSectionGutterBackward = function (gutter, gutterOffset, update) {
        if (update === void 0) { update = true; }
        var section = gutter.previousSibling;
        var sectionMinWidth = this.GetSectionMinWidth(section);
        var sectionOffset = section.offsetLeft;
        var sectionWidth = gutterOffset - sectionOffset;
        if (sectionWidth <= sectionMinWidth) {
            var parent_1 = section.parentNode;
            if (parent_1.firstChild === section) {
                this.UpdateSectionGutter(gutter, sectionMinWidth);
                return false;
            }
            else {
                var sectionGutter = section.previousSibling;
                if (this.UpdateSectionGutterBackward(sectionGutter, gutterOffset - sectionMinWidth - gutter.clientWidth)) {
                    if (update) {
                        this.UpdateSectionGutter(gutter, gutterOffset);
                    }
                    return true;
                }
                else {
                    this.UpdateSectionGutter(gutter, sectionOffset + sectionMinWidth);
                    return false;
                }
            }
            this.RearrangeSectionGutters(section, sectionMinWidth);
        }
        else {
            if (update) {
                this.UpdateSectionGutter(gutter, gutterOffset);
            }
            this.RearrangeSectionGutters(section, sectionWidth);
            return true;
        }
        return false;
    };
    Layout.prototype.UpdateSectionGutterForward = function (gutter, gutterOffset, update) {
        if (update === void 0) { update = true; }
        var section = gutter.nextSibling;
        var sectionMinWidth = this.GetSectionMinWidth(section);
        var sectionOffset = section.offsetLeft;
        var sectionWidth = section.clientWidth - ((gutterOffset + gutter.clientWidth) - sectionOffset);
        if (sectionWidth <= sectionMinWidth) {
            var parent_2 = section.parentNode;
            if (parent_2.lastChild === section) {
                this.UpdateSectionGutter(gutter, parent_2.clientWidth - sectionMinWidth - gutter.clientWidth);
                return false;
            }
            else {
                var sectionGutter = section.nextSibling;
                if (this.UpdateSectionGutterForward(sectionGutter, gutterOffset + gutter.clientWidth + sectionMinWidth)) {
                    if (update) {
                        this.UpdateSectionGutter(gutter, gutterOffset);
                    }
                    return true;
                }
                else {
                    this.UpdateSectionGutter(gutter, sectionGutter.offsetLeft - sectionMinWidth - gutter.clientWidth);
                    return false;
                }
            }
            this.RearrangeSectionGutters(section, sectionMinWidth);
        }
        else {
            if (update) {
                this.UpdateSectionGutter(gutter, gutterOffset);
            }
            this.RearrangeSectionGutters(section, sectionWidth);
            return true;
        }
        return false;
    };
    Layout.prototype.RearrangeSectionGutters = function (section, width) {
        var currentWidth = section.clientWidth;
        var sectionChildren = section.childNodes;
        for (var ids = 0; ids < sectionChildren.length; ids++) {
            var row = sectionChildren[ids];
            if (row.className === LAYOUT_CLASS_ROW) {
                var rowChildren = row.childNodes;
                if (rowChildren.length > 1) {
                    var section_1 = row.lastChild;
                    var gutter = section_1.previousSibling;
                    var gutterOffset = gutter.offsetLeft;
                    var sectionMinWidth = this.GetSectionMinWidth(section_1);
                    var sectionOffset = section_1.offsetLeft;
                    var sectionWidth = width - gutterOffset - gutter.clientWidth;
                    if (sectionWidth <= sectionMinWidth) {
                        this.UpdateSectionGutterBackward(gutter, gutterOffset - (currentWidth - width) - gutter.clientWidth);
                    }
                    this.RearrangeSectionGutters(section_1, sectionWidth);
                }
            }
        }
    };
    Layout.prototype.UpdateRowGutterBackward = function (gutter, gutterOffset, update) {
        if (update === void 0) { update = true; }
        var row = gutter.previousSibling;
        var rowMinHeight = this.GetRowMinHeight(row);
        var rowOffset = row.offsetTop;
        var rowHeight = gutterOffset - rowOffset;
        if (rowHeight <= rowMinHeight) {
            var parent_3 = row.parentNode;
            if (parent_3.firstChild === row) {
                this.UpdateRowGutter(gutter, rowMinHeight);
                return false;
            }
            else {
                var rowGutter = row.previousSibling;
                if (this.UpdateRowGutterBackward(rowGutter, gutterOffset - rowMinHeight - gutter.clientHeight)) {
                    if (update) {
                        this.UpdateRowGutter(gutter, gutterOffset);
                    }
                    return true;
                }
                else {
                    this.UpdateRowGutter(gutter, rowOffset + rowMinHeight);
                    return false;
                }
            }
            this.RearrangeRowGutters(row, rowMinHeight);
        }
        else {
            if (update) {
                this.UpdateRowGutter(gutter, gutterOffset);
            }
            this.RearrangeRowGutters(row, rowHeight);
            return true;
        }
        return false;
    };
    Layout.prototype.UpdateRowGutterForward = function (gutter, gutterOffset, update) {
        if (update === void 0) { update = true; }
        var row = gutter.nextSibling;
        var rowMinHeight = this.GetRowMinHeight(row);
        var rowOffset = row.offsetTop;
        var rowHeight = row.clientHeight - ((gutterOffset + gutter.clientHeight) - rowOffset);
        if (rowHeight <= rowMinHeight) {
            var parent_4 = row.parentNode;
            if (parent_4.lastChild === row) {
                this.UpdateRowGutter(gutter, parent_4.clientHeight - rowMinHeight - gutter.clientHeight);
                return false;
            }
            else {
                var rowGutter = row.nextSibling;
                if (this.UpdateRowGutterForward(rowGutter, gutterOffset + gutter.clientHeight + rowMinHeight)) {
                    if (update) {
                        this.UpdateRowGutter(gutter, gutterOffset);
                    }
                    return true;
                }
                else {
                    this.UpdateRowGutter(gutter, rowGutter.offsetTop - rowMinHeight - gutter.clientHeight);
                    return false;
                }
            }
            this.RearrangeRowGutters(row, rowMinHeight);
        }
        else {
            if (update) {
                this.UpdateRowGutter(gutter, gutterOffset);
            }
            this.RearrangeRowGutters(row, rowHeight);
            return true;
        }
        return false;
    };
    Layout.prototype.RearrangeRowGutters = function (row, height) {
        var currentHeight = row.clientHeight;
        var rowChildren = row.childNodes;
        for (var idr = 0; idr < rowChildren.length; idr++) {
            var section = rowChildren[idr];
            if (section.className === LAYOUT_CLASS_SECTION) {
                var sectionChildren = section.childNodes;
                if (sectionChildren.length > 1) {
                    var row_1 = section.lastChild;
                    var gutter = row_1.previousSibling;
                    var gutterOffset = gutter.offsetTop;
                    var rowMinHeight = this.GetRowMinHeight(row_1);
                    var rowOffset = row_1.offsetTop;
                    var rowHeight = height - gutterOffset - gutter.clientHeight;
                    if (rowHeight <= rowMinHeight) {
                        this.UpdateRowGutterBackward(gutter, gutterOffset - (currentHeight - height) - gutter.clientHeight);
                    }
                    this.RearrangeRowGutters(row_1, rowHeight);
                }
            }
        }
    };
    Layout.prototype.MapAndScaleLayoutGutters = function (container, clientSize) {
        var guttesOffsetMap = new Map;
        var widthRatio = container.clientWidth / clientSize.width;
        var heightRatio = container.clientHeight / clientSize.height;
        var containerChildren = container.childNodes;
        for (var idx = 0; idx < containerChildren.length; idx++) {
            var containerElement = containerChildren[idx];
            switch (containerElement.className) {
                case LAYOUT_CLASS_SECTION_GUTTER: {
                    var gutter = containerElement;
                    var gutterOffset = void 0;
                    var previousSection = gutter.previousSibling;
                    var previousSectionMinWidth = this.GetSectionMinWidth(previousSection);
                    var nextSection = gutter.nextSibling;
                    if (previousSection === container.firstChild) {
                        gutterOffset = widthRatio * previousSection.clientWidth;
                        if (gutterOffset < previousSectionMinWidth) {
                            gutterOffset = previousSectionMinWidth;
                        }
                        if (nextSection === container.lastChild) { //Issue:The gutter has wrong offset when previous gutter 
                            var nextSectionMinWidth = this.GetSectionMinWidth(nextSection);
                            var nextSectionWidth = clientSize.width - gutterOffset + gutter.clientWidth;
                            if (nextSectionWidth < nextSectionMinWidth) {
                                var gutterOffsetCalculatedFromTheRight = clientSize.width - nextSectionWidth - gutter.clientWidth;
                                if (gutterOffsetCalculatedFromTheRight < gutterOffset) {
                                    this.RecalculateSectionGuttersOffsetBackward(gutter, gutterOffsetCalculatedFromTheRight, guttesOffsetMap);
                                }
                                gutterOffset = gutterOffsetCalculatedFromTheRight;
                            }
                        }
                        guttesOffsetMap.set(gutter, gutterOffset);
                    }
                    else {
                        var previousGutter = previousSection.previousSibling;
                        var previousGutterOffset = guttesOffsetMap.get(previousGutter);
                        var previousSectionOffset = previousGutterOffset + previousGutter.clientWidth;
                        gutterOffset = widthRatio * gutter.offsetLeft;
                        var sectionWidth = gutterOffset - previousSectionOffset;
                        if (sectionWidth < previousSectionMinWidth) {
                            gutterOffset = previousSectionOffset + previousSectionMinWidth;
                        }
                        if (nextSection === container.lastChild) {
                            var nextSectionMinWidth = this.GetSectionMinWidth(nextSection);
                            var nextSectionWidth = clientSize.width - gutterOffset + gutter.clientWidth;
                            if (nextSectionWidth < nextSectionMinWidth) {
                                var gutterOffsetCalculatedFromTheRight = clientSize.width - nextSectionWidth - gutter.clientWidth;
                                if (gutterOffsetCalculatedFromTheRight < gutterOffset) {
                                    this.RecalculateSectionGuttersOffsetBackward(gutter, gutterOffsetCalculatedFromTheRight, guttesOffsetMap);
                                }
                                gutterOffset = gutterOffsetCalculatedFromTheRight;
                            }
                        }
                        guttesOffsetMap.set(gutter, gutterOffset);
                    }
                    break;
                }
                case LAYOUT_CLASS_ROW_GUTTER: {
                    var gutter = containerElement;
                    var gutterOffset = void 0;
                    var previousRow = gutter.previousSibling;
                    var previousRowMinHeight = this.GetRowMinHeight(previousRow);
                    var nextRow = gutter.nextSibling;
                    if (previousRow === container.firstChild) {
                        gutterOffset = heightRatio * previousRow.clientHeight;
                        if (gutterOffset < previousRowMinHeight) {
                            gutterOffset = previousRowMinHeight;
                        }
                        if (nextRow === container.lastChild) {
                            var nextRowMinHeight = this.GetRowMinHeight(nextRow);
                            var nextRowHeight = clientSize.height - gutterOffset + gutter.clientHeight;
                            if (nextRowHeight < nextRowMinHeight) {
                                var gutterOffsetCalculatedFromTheRight = clientSize.height - nextRowHeight - gutter.clientHeight;
                                if (gutterOffsetCalculatedFromTheRight < gutterOffset) {
                                    this.RecalculateRowGuttersOffsetBackward(gutter, gutterOffsetCalculatedFromTheRight, guttesOffsetMap);
                                }
                                gutterOffset = gutterOffsetCalculatedFromTheRight;
                            }
                        }
                        guttesOffsetMap.set(gutter, gutterOffset);
                    }
                    else {
                        var previousGutter = previousRow.previousSibling;
                        var previousGutterOffset = guttesOffsetMap.get(previousGutter);
                        var previousRowOffset = previousGutterOffset + previousGutter.clientHeight;
                        gutterOffset = heightRatio * gutter.offsetTop;
                        var rowHeight = gutterOffset - previousRowOffset;
                        if (rowHeight < previousRowMinHeight) {
                            gutterOffset = previousRowOffset + previousRowMinHeight;
                        }
                        if (nextRow === container.lastChild) {
                            var nextRowMinHeight = this.GetRowMinHeight(nextRow);
                            var nextRowHeight = clientSize.height - gutterOffset + gutter.clientHeight;
                            if (nextRowHeight < nextRowMinHeight) {
                                var gutterOffsetCalculatedFromTheRight = clientSize.height - nextRowHeight - gutter.clientHeight;
                                if (gutterOffsetCalculatedFromTheRight < gutterOffset) {
                                    this.RecalculateRowGuttersOffsetBackward(gutter, gutterOffsetCalculatedFromTheRight, guttesOffsetMap);
                                }
                                gutterOffset = gutterOffsetCalculatedFromTheRight;
                            }
                        }
                        guttesOffsetMap.set(gutter, gutterOffset);
                    }
                    break;
                }
            }
        }
        this.ResizeLayoutGutters(container, guttesOffsetMap);
        this.ResizeLayoutElements(container);
    };
    Layout.prototype.ResizeLayoutGutters = function (container, guttesOffsetMap) {
        var containerChildren = container.childNodes;
        for (var idx = 0; idx < containerChildren.length; idx++) {
            var containerElement = containerChildren[idx];
            switch (containerElement.className) {
                case LAYOUT_CLASS_SECTION_GUTTER: {
                    var gutter = containerElement;
                    var gutterOffset = guttesOffsetMap.get(gutter);
                    this.UpdateSectionGutter(gutter, gutterOffset);
                    break;
                }
                case LAYOUT_CLASS_ROW_GUTTER: {
                    var gutter = containerElement;
                    var gutterOffset = guttesOffsetMap.get(gutter);
                    this.UpdateRowGutter(gutter, gutterOffset);
                    break;
                }
            }
        }
    };
    Layout.prototype.ResizeLayoutElements = function (container) {
        var containerChildren = container.childNodes;
        for (var idx = 0; idx < containerChildren.length; idx++) {
            var containerElement = containerChildren[idx];
            switch (containerElement.className) {
                case LAYOUT_CLASS_SECTION: {
                    var section = containerElement;
                    var sectionClientSize = { width: section.clientWidth, height: section.clientHeight };
                    this.FitSection(section);
                    this.MapAndScaleLayoutGutters(section, sectionClientSize);
                    break;
                }
                case LAYOUT_CLASS_ROW: {
                    var row = containerElement;
                    var rowClientSize = { width: row.clientWidth, height: row.clientHeight };
                    this.FitRow(row);
                    this.MapAndScaleLayoutGutters(row, rowClientSize);
                    break;
                }
            }
        }
    };
    Layout.prototype.RecalculateSectionGuttersOffsetBackward = function (gutter, gutterOffset, guttesOffsetMap) {
        var parent = gutter.parentNode;
        var previousSection = gutter.previousSibling;
        if (previousSection !== parent.firstChild) {
            var previousGutter = previousSection.previousSibling;
            var sectionMinWidth = this.GetSectionMinWidth(previousSection);
            var sectionWidth = gutterOffset - guttesOffsetMap.get(previousGutter) + previousGutter.clientWidth;
            var previousGutterOffset = void 0;
            if (sectionWidth < sectionMinWidth) {
                previousGutterOffset = gutterOffset - sectionMinWidth - previousGutter.clientWidth;
                guttesOffsetMap.set(previousGutter, previousGutterOffset);
                this.RecalculateSectionGuttersOffsetBackward(previousGutter, previousGutterOffset, guttesOffsetMap);
            }
        }
    };
    Layout.prototype.RecalculateRowGuttersOffsetBackward = function (gutter, gutterOffset, guttesOffsetMap) {
        var parent = gutter.parentNode;
        var previousRow = gutter.previousSibling;
        if (previousRow !== parent.firstChild) {
            var previousGutter = previousRow.previousSibling;
            var rowMinHeight = this.GetRowMinHeight(previousRow);
            var rowHeight = gutterOffset - guttesOffsetMap.get(previousGutter) + previousGutter.clientHeight;
            var previousGutterOffset = void 0;
            if (rowHeight < rowMinHeight) {
                previousGutterOffset = gutterOffset - rowMinHeight - previousGutter.clientHeight;
                guttesOffsetMap.set(previousGutter, previousGutterOffset);
                this.RecalculateRowGuttersOffsetBackward(previousGutter, previousGutterOffset, guttesOffsetMap);
            }
        }
    };
    Layout.prototype.UpdateLayoutClientSize = function () {
        this.layoutClientSize.width = this.container.clientWidth;
        this.layoutClientSize.height = this.container.clientHeight;
    };
    Layout.prototype.ScaleLayout = function () {
        this.MapAndScaleLayoutGutters(this.container, this.layoutClientSize);
        this.UpdateLayoutClientSize();
    };
    Layout.prototype.OnDragging = function (gutter) {
        var HTMLRep = gutter.HTMLRepresentation;
        if (this.pointer) {
            var _a = this.pointer.Mouse(), mX = _a[0], mY = _a[1];
            var _b = this.pointer.GetBuffer(mX, mY), x = _b[0], y = _b[1];
            var _c = this.pointer.GetDiff(), dX = _c[0], dY = _c[1];
            switch (HTMLRep.className) {
                case LAYOUT_CLASS_SECTION_GUTTER: {
                    var leftSection = HTMLRep.previousSibling;
                    var leftSectionMinWidth = this.GetSectionMinWidth(leftSection);
                    var rightSection = HTMLRep.nextSibling;
                    var rightSectionMinWidth = this.GetSectionMinWidth(rightSection);
                    var parent_5 = HTMLRep.parentNode;
                    if (parent_5.firstChild === leftSection) {
                        if (x <= leftSectionMinWidth) {
                            x = leftSectionMinWidth;
                        }
                    }
                    else if (parent_5.lastChild === rightSection) {
                        if (parent_5.clientWidth - x + HTMLRep.clientWidth < rightSectionMinWidth) {
                            x = parent_5.clientWidth - rightSectionMinWidth - HTMLRep.clientWidth;
                        }
                    }
                    if (this.UpdateSectionGutterBackward(HTMLRep, x, false) && this.UpdateSectionGutterForward(HTMLRep, x, false)) {
                        this.UpdateSectionGutter(HTMLRep, x);
                    }
                    break;
                }
                case LAYOUT_CLASS_ROW_GUTTER: {
                    var topRow = HTMLRep.previousSibling;
                    var topRowMinHeight = this.GetRowMinHeight(topRow);
                    var bottomRow = HTMLRep.nextSibling;
                    var bottomRowMinHeight = this.GetRowMinHeight(bottomRow);
                    var parent_6 = HTMLRep.parentNode;
                    if (parent_6.firstChild === topRow) {
                        if (y <= topRowMinHeight) {
                            y = topRowMinHeight;
                        }
                    }
                    else if (parent_6.lastChild === bottomRow) {
                        if (parent_6.clientHeight - y + HTMLRep.clientHeight < bottomRowMinHeight) {
                            y = parent_6.clientHeight - bottomRowMinHeight - HTMLRep.clientHeight;
                        }
                    }
                    if (this.UpdateRowGutterBackward(HTMLRep, y, false) && this.UpdateRowGutterForward(HTMLRep, y, false)) {
                        this.UpdateRowGutter(HTMLRep, y);
                    }
                    break;
                }
            }
            this.UpdateElement(HTMLRep.parentNode);
        }
    };
    ;
    Layout.prototype.OnDragStop = function (gutter) {
        delete this.pointer;
    };
    ;
    Layout.prototype.GetLayoutInstance = function () {
        return this;
    };
    Layout.prototype.GetContextList = function () {
        var elements = this.container.querySelectorAll("." + LAYOUT_CLASS_CONTEXT);
        return elements;
    };
    Layout.prototype.GetSectionFromContext = function (context) {
        if (context) {
            var section = context.parentNode;
            return section;
        }
    };
    Layout.prototype.GetContextFromSection = function (section) {
        var context = this.GetContext(section);
        if (context.className === LAYOUT_CLASS_CONTEXT)
            return context;
        else
            return null;
    };
    Layout.prototype.SwapContextInSections = function (fromSection, toSection) {
        var contextToMove = this.GetContext(fromSection);
        if (contextToMove) {
            var contextToRemove = this.GetContext(toSection);
            if (contextToRemove) {
                fromSection.appendChild(contextToRemove);
            }
            toSection.appendChild(contextToMove);
        }
    };
    Layout.prototype.SplitVertically = function (context) {
        if (context) {
            var section = context.parentNode;
            var newSection = this.SplitSection(section);
            if (newSection) {
                this.SetLayoutClientMinSize();
                this.Update();
                return this.GetContext(newSection);
            }
        }
    };
    Layout.prototype.SplitHorizontally = function (context) {
        if (context) {
            var section = context.parentNode;
            var newSection = this.SplitRow(section);
            if (newSection) {
                this.SetLayoutClientMinSize();
                this.Update();
                return this.GetContext(newSection);
            }
        }
    };
    Layout.prototype.Join = function (context) {
        if (context) {
            var section = context.parentNode;
            var row = section.parentNode;
            if (row) {
                if (row.querySelector("." + LAYOUT_CLASS_SECTION_GUTTER) === null && row.querySelector("." + LAYOUT_CLASS_ROW_GUTTER) === null) { //Single Section in Row
                    this.Join(section);
                }
                else {
                    var sectionGutter = void 0;
                    if (row.lastChild === section) { //Last Section in Row
                        sectionGutter = section.previousSibling;
                    }
                    else { //Regular Section in Row
                        sectionGutter = section.nextSibling;
                    }
                    row.removeChild(section);
                    if (sectionGutter) {
                        row.removeChild(sectionGutter);
                    }
                    this.UpdateElement(row);
                    this.SetLayoutClientMinSize();
                }
            }
        }
    };
    Layout.prototype.Show = function () {
    };
    Layout.prototype.Hide = function () {
    };
    Layout.prototype.Update = function () {
        this.ScaleLayout();
    };
    return Layout;
}(view_1.View));
exports.Layout = Layout;
