"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var HTMLRepresentation;
(function (HTMLRepresentation_1) {
    function CreateHTMLElement(classNameProperties, parentNode, afterHTMLElement) {
        var element = (document.createElement('div'));
        if (typeof classNameProperties === 'string') {
            element.className = classNameProperties;
        }
        else {
            for (var _i = 0, _a = Object.entries(classNameProperties); _i < _a.length; _i++) {
                var _b = _a[_i], key = _b[0], value = _b[1];
                switch (key) {
                    case "className": {
                        element.className = value;
                        break;
                    }
                    case "id": {
                        element.id = value;
                        break;
                    }
                    case "attributes": {
                        for (var _c = 0, _d = Object.entries(value); _c < _d.length; _c++) {
                            var _e = _d[_c], attr_key = _e[0], attr_value = _e[1];
                            element.setAttribute(attr_key, attr_value);
                        }
                        break;
                    }
                }
            }
        }
        if (parentNode) {
            if (afterHTMLElement)
                parentNode.insertBefore(element, afterHTMLElement.nextSibling);
            else
                parentNode.appendChild(element);
        }
        return element;
    }
    HTMLRepresentation_1.CreateHTMLElement = CreateHTMLElement;
    function innerText(element, text) {
        element.innerText = text;
    }
    HTMLRepresentation_1.innerText = innerText;
    function SwapChildElements(src, dest) {
        var srcParent = src.parentNode;
        var destParent = dest.parentNode;
        if (srcParent === destParent) {
            src.before(dest);
        }
    }
    HTMLRepresentation_1.SwapChildElements = SwapChildElements;
    function RemoveChildElements(parent) {
        while (parent.firstChild) {
            parent.firstChild.remove();
        }
    }
    HTMLRepresentation_1.RemoveChildElements = RemoveChildElements;
    function isSiblingElement(element, sibling, opts) {
        var parent = element.parentNode;
        if (parent) {
            var children = parent.childNodes;
            for (var idx = 0; idx < children.length; idx++) {
                if (children[idx] === element) {
                    opts.elementFirst = true;
                }
                if (children[idx] === sibling) {
                    return true;
                }
            }
        }
        return false;
    }
    HTMLRepresentation_1.isSiblingElement = isSiblingElement;
    function CreateProcessView(className, src, parent) {
        var element = (document.createElement('webview'));
        element.className = className;
        element.setAttribute("src", src);
        element.setAttribute("nodeintegration", "");
        if (parent) {
            parent.appendChild(element);
        }
        return element;
    }
    HTMLRepresentation_1.CreateProcessView = CreateProcessView;
    var DomObject = /** @class */ (function () {
        function DomObject(HTMLRepresentation) {
            this.HTMLRepresentation = HTMLRepresentation;
        }
        return DomObject;
    }());
    HTMLRepresentation_1.DomObject = DomObject;
})(HTMLRepresentation = exports.HTMLRepresentation || (exports.HTMLRepresentation = {}));
