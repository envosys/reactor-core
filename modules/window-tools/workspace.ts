import {app, BrowserWindow, ipcMain} from "electron";
import {ipcConsoleMessage} from "./ipc-console"

export class Workspace{
  private windows: Set <BrowserWindow> = new Set;
  public constructor(){
    this.SetupAppEventListeners();
    process.env.ELECTRON_DISABLE_SECURITY_WARNINGS = 'true';
  }

  private DefaultWIndow(){
    const win = new BrowserWindow({
      show: false,
      minWidth: 800,
      minHeight: 600,
      backgroundColor: '#282c34',
      webPreferences: {
        nodeIntegration: true,
        nodeIntegrationInWorker: true
      }
    });
    win.setMenuBarVisibility(false);
    win.maximize();
    win.webContents.openDevTools();
    win.once('ready-to-show', () => {
      win.show();
    })
    win.loadFile('./modules/window-tools/html/window.html');
    win.webContents.on('did-finish-load', ()=>{
      console.log("Contents loaded");
    })

    this.windows.add(win);

    ipcMain.on('window-process-message', (event: any, arg: object) =>{
      console.log("Message from the Renderer process: " + arg);
      event.sender.send('main-process-message', 'Start loading Workspace');
    })
    ipcMain.on('log-message', (event: any, arg: ipcConsoleMessage) =>{
      console.log("[Sender: " + arg.type);
      console.log("Message: " + arg.message + "]");
      win.webContents.send('log-message', arg);
    })
  }

  private SetupAppEventListeners(){
    app.on('ready', () => {
      this.DefaultWIndow()
    }).on('window-all-closed', () => {
      if (process.platform !== 'darwin'){
        app.quit();
      }
    }).on('activate', () => {
      if (!this.windows.size){
        this.DefaultWIndow();
      }
    });
  }


}
