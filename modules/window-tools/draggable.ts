const DRAG_AND_DROP_LAYER_ID_ELEMENT = "drag-and-drop-layer";

import {LayoutManager} from "./layout-manager";
import {HTMLRepresentation} from './dom-object';

export abstract class Draggable extends HTMLRepresentation.DomObject{
  protected constructor(HTMLRepresentation: HTMLElement){
    super(HTMLRepresentation);
  }

  public OnDragStart(object?: any){};
  public OnDragging(object?: any){};
  public OnDragStop(object?: any){};

  public OnDragEnter(object?: any, listener?: any){};
  public OnDragLeave(object?: any, listener?: any){};
  public OnDrop(object?: any, listener?: any){};


}
