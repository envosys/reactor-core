"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var TAB_CLASS_ACTIVE = "tab active";
var TAB_CLASS = "tab";
var TAB_CLASS_BUTTON = "tab button";
var TAB_SAFE_WIDTH = 4;
var TAB_BUTTON_WIDTH = 24;
var view_1 = require("./view");
var dom_object_1 = require("./dom-object");
var Tab = /** @class */ (function (_super) {
    __extends(Tab, _super);
    function Tab(parentView, targetView, title) {
        var _this = _super.call(this, parentView, dom_object_1.HTMLRepresentation.CreateHTMLElement({
            className: "tab",
            attributes: {
                draggable: "true"
            }
        }, parentView.HTMLRepresentation)) || this;
        _this.title = title ? title : 'Untitled';
        dom_object_1.HTMLRepresentation.innerText(_this.HTMLRepresentation, _this.title);
        _this.active = false;
        if (targetView)
            _this.view = targetView;
        _this.HTMLRepresentation.style.width = _this.HTMLRepresentation.clientWidth + TAB_SAFE_WIDTH + "px";
        _this.Update();
        return _this;
    }
    Tab.prototype.ShowTargetView = function () {
        if (this.view)
            this.view.Show();
    };
    Tab.prototype.HideTargetView = function () {
        if (this.view)
            this.view.Hide();
    };
    Tab.prototype.Title = function (title) {
        this.title = title;
        this.Update();
    };
    Tab.prototype.GetTitle = function () {
        return this.title;
    };
    Tab.prototype.TargetView = function () {
        return this.view;
    };
    Tab.prototype.Show = function () {
    };
    Tab.prototype.Hide = function () {
    };
    Tab.prototype.Activate = function () {
        var tabBar = this.GetParentView();
        tabBar.OnTabActivated(this);
        this.active = true;
        this.Update();
    };
    ;
    Tab.prototype.OnParentViewChange = function (parentView) {
        //this.delayTargetActivation = true;
    };
    Tab.prototype.Deactivate = function () {
        this.active = false;
        this.Update();
    };
    Tab.prototype.TabTypeButton = function () {
        this.HTMLRepresentation.style.width = TAB_BUTTON_WIDTH + "px";
        this.HTMLRepresentation.className = TAB_CLASS_BUTTON;
    };
    Tab.prototype.Update = function () {
        dom_object_1.HTMLRepresentation.innerText(this.HTMLRepresentation, this.title);
        this.HTMLRepresentation.className = this.active ? TAB_CLASS_ACTIVE : TAB_CLASS;
        this.HTMLRepresentation.style.zIndex = this.active ? "999" : "0";
    };
    return Tab;
}(view_1.View));
exports.Tab = Tab;
