const LAYOUT_MARGIN = 0;
const LAYOUT_GUTTER_WIDTH = 1; //Unused

const LAYOUT_CLASS_SECTION = "layout-section";
const LAYOUT_CLASS_ROW = "layout-row";
const LAYOUT_CLASS_SECTION_GUTTER = "layout-section-gutter";
const LAYOUT_CLASS_ROW_GUTTER = "layout-row-gutter";
const LAYOUT_CLASS_CONTEXT = "context";

import {Draggable} from "./draggable";

import {Pointer, oddRound} from "./pointer";
import {LayoutManager} from "./layout-manager";
import {HTMLRepresentation} from "./dom-object";
import {View} from "./view";

export class Layout extends View{
  private container: HTMLElement;
  private pointer: Pointer | undefined;
  private layoutClientSize: {width: number, height: number} = {width: 0, height: 0};
  public constructor(container: HTMLElement){
      super(null, container);
      this.container = container;
      LayoutManager.SetupLayoutReference(this);
      this.SetupEventListeners();
  }

  public Init(){
    HTMLRepresentation.RemoveChildElements(this.container);
    let row = HTMLRepresentation.CreateHTMLElement(LAYOUT_CLASS_ROW, this.container);
    this.FitRow(row);
    let section = HTMLRepresentation.CreateHTMLElement(LAYOUT_CLASS_SECTION, row);
    this.FitSection(section);
    this.SetLayoutClientMinSize();
    this.UpdateLayoutClientSize();
    return this.CreateContext(section);
  }

  private SetupEventListeners(){
    window.addEventListener("load", (e)=>{

    });

    window.addEventListener("resize", (e)=>{
      e.preventDefault();
      this.Update();
    });
  }

  private CreateContext(section: HTMLElement){
    return HTMLRepresentation.CreateHTMLElement(LAYOUT_CLASS_CONTEXT, section);
  }

  private GetContext(section: HTMLElement){
    let context = <HTMLElement> section.querySelector("." + LAYOUT_CLASS_CONTEXT);
    return context;
  }

  private CloneContext(fromSection: HTMLElement, toSection: HTMLElement){
    let contextToClone = this.GetContext(fromSection);
    let contextCloned: HTMLElement | null = null;
    if (contextToClone){
       contextCloned = <HTMLElement> contextToClone.cloneNode(true);
       toSection.appendChild(contextCloned);
    }
    return contextCloned;
  }

  private moveContext(fromSection: HTMLElement, toSection: HTMLElement){
    let contextToMove = this.GetContext(fromSection);
    if (contextToMove){
       toSection.appendChild(contextToMove);
    }
    return contextToMove;
  }

  private FitSection(element: HTMLElement){
    let container = <HTMLElement> element.parentNode;
    let previousElement = <HTMLElement> element.previousSibling;
    let nextElement = <HTMLElement> element.nextSibling;
    if (previousElement)
      element.style.left = previousElement.offsetLeft + previousElement.clientWidth + LAYOUT_MARGIN + "px";
      else
      element.style.left = LAYOUT_MARGIN + "px";
    if (nextElement)
      element.style.width = nextElement.offsetLeft - (previousElement !== null ? (previousElement.offsetLeft + previousElement.clientWidth) : 0) - 2 * LAYOUT_MARGIN + "px";
      else
      element.style.width = container.clientWidth - (previousElement !== null ? previousElement.offsetLeft + previousElement.clientWidth : 0) - 2 * LAYOUT_MARGIN + "px";
    element.style.top = LAYOUT_MARGIN + "px";
    element.style.height = container.clientHeight - 2 * LAYOUT_MARGIN + "px";
  }

  private FitRow(element: HTMLElement){
    let container = <HTMLElement> element.parentNode;
    let previousElement = <HTMLElement> element.previousSibling;
    let nextElement = <HTMLElement> element.nextSibling;
    if (previousElement)
      element.style.top = previousElement.offsetTop + previousElement.clientHeight + LAYOUT_MARGIN + "px";
      else
      element.style.top = LAYOUT_MARGIN + "px";
    if (nextElement)
      element.style.height = nextElement.offsetTop - (previousElement !== null ? (previousElement.offsetTop + previousElement.clientHeight) : 0) - 2 * LAYOUT_MARGIN + "px";
      else
      element.style.height = container.clientHeight - (previousElement !== null ? (previousElement.offsetTop + previousElement.clientHeight) : 0) - 2 * LAYOUT_MARGIN + "px";
    element.style.left = LAYOUT_MARGIN + "px";
    element.style.width = container.clientWidth - 2 * LAYOUT_MARGIN + "px";
  }

  private CreateSectionGutter(section: HTMLElement){
    let row = <HTMLDivElement> section.parentNode;
    let gutter = HTMLRepresentation.CreateHTMLElement(LAYOUT_CLASS_SECTION_GUTTER, row, section)
    gutter.style.cursor = "col-resize";
    gutter.style.left = oddRound(section.offsetLeft + section.clientWidth / 2.0 - gutter.clientWidth / 2) + "px";
    gutter.style.height = section.clientHeight + 2 * LAYOUT_MARGIN + "px";
    LayoutManager.ObserveViewObject(LayoutManager.WrapHTML(gutter));
    let properties = {
      position: row.clientWidth / gutter.offsetLeft
    };

    return gutter;
  }

  private UpdateSectionGutter(gutter: HTMLElement, offsetLeft?: number){
    let section = <HTMLDivElement> gutter.parentNode;
    gutter.style.height = section.clientHeight + 2 * LAYOUT_MARGIN + "px";
    if (offsetLeft)
      gutter.style.left = oddRound(offsetLeft) + "px";
  }

  private SplitSection(section: HTMLElement): HTMLElement | undefined{
    let row = <HTMLDivElement> section.parentNode;
    if (row && row.className == LAYOUT_CLASS_ROW){
      let gutter = this.CreateSectionGutter(section);
      let newSection = HTMLRepresentation.CreateHTMLElement(LAYOUT_CLASS_SECTION, row, gutter);
      this.CreateContext(newSection);
      this.FitSection(section);
      this.FitSection(newSection);
      return newSection;
    }
  }

  private CreateRowGutter(section: HTMLElement){
    let row = <HTMLDivElement> section.parentNode;
    let container = <HTMLDivElement> row.parentNode;
    let gutter = HTMLRepresentation.CreateHTMLElement(LAYOUT_CLASS_ROW_GUTTER, container, row);
    gutter.style.cursor = "row-resize";
    gutter.style.top = oddRound(row.offsetTop + row.clientHeight / 2.0 - gutter.clientHeight / 2) + "px";
    gutter.style.width = container.clientWidth + "px";
    LayoutManager.ObserveViewObject(LayoutManager.WrapHTML(gutter));
    let properties = {
      position: container.clientHeight / gutter.offsetTop
    };
    return gutter;
  }

  private UpdateRowGutter(gutter: HTMLElement, offsetTop?: number | null){
    let row = <HTMLDivElement> gutter.parentNode;
    gutter.style.width = row.clientWidth + "px";
    if (offsetTop)
      gutter.style.top = oddRound(offsetTop) + "px";
  }

  private SplitRow(section: HTMLElement): HTMLElement | undefined {
    let row = <HTMLDivElement> section.parentNode;
    if (row && row.className == LAYOUT_CLASS_ROW){
      let container = <HTMLDivElement> row.parentNode;
      if (row.querySelector("." + LAYOUT_CLASS_SECTION_GUTTER) === null){
        let gutter = this.CreateRowGutter(section);
        let newRow = HTMLRepresentation.CreateHTMLElement(LAYOUT_CLASS_ROW, container, gutter);
        let rowSection = HTMLRepresentation.CreateHTMLElement(LAYOUT_CLASS_SECTION, newRow);
        this.CreateContext(rowSection);
        this.FitRow(row);
        this.FitSection(section);
        this.FitRow(newRow);
        this.FitSection(rowSection);
        return rowSection;
      }
      else{
        let newRow = HTMLRepresentation.CreateHTMLElement(LAYOUT_CLASS_ROW, section);
        let rowSection = HTMLRepresentation.CreateHTMLElement(LAYOUT_CLASS_SECTION, newRow);
        this.moveContext(section, rowSection);
        this.FitRow(newRow);
        this.FitSection(rowSection);
        return this.SplitRow(rowSection);
      }
    }
  }

  private UpdateElement(element: HTMLElement){
    let elements = element.childNodes;
    for (let idx = 0; idx < elements.length; idx++){
      let currentElement = <HTMLDivElement> elements[idx];
      switch (currentElement.className){
        case LAYOUT_CLASS_ROW:{
            this.FitRow(currentElement);
            this.UpdateElement(currentElement);
          break;
        }
        case LAYOUT_CLASS_SECTION:{
            this.FitSection(currentElement);
            this.UpdateElement(currentElement);
          break;
        }
        case LAYOUT_CLASS_ROW_GUTTER:{
            this.UpdateRowGutter(currentElement);
          break;
        }
        case LAYOUT_CLASS_SECTION_GUTTER:{
            this.UpdateSectionGutter(currentElement);
          break;
        }
        case LAYOUT_CLASS_CONTEXT:{
          return;
        }
      }
    }
  }

  private GetRowMinWithBeforeGutter(row: HTMLElement, gutter?: HTMLElement): number{
    let children = row.childNodes;
    let minWidth = 0;
    for (let idx = 0; idx < children.length; idx++){
      let element = <HTMLElement> children[idx];
      if (gutter === element){
        return minWidth;
      }
      if (element.className === LAYOUT_CLASS_SECTION){
        minWidth += this.GetSectionMinWidth(element);
      }
      if (element.className === LAYOUT_CLASS_SECTION_GUTTER){
        minWidth += element.clientWidth;
      }
    }
    return minWidth;
  }

  private GetRowMinWithAfterGutter(row: HTMLElement, gutter?: HTMLElement): number{
    let children = row.childNodes;
    let minWidth = 0;
    for (let idx = children.length - 1; idx >= 0; idx--){
      let element = <HTMLElement> children[idx];
      if (gutter === element){
        return minWidth;
      }
      if (element.className === LAYOUT_CLASS_SECTION){
        minWidth += this.GetSectionMinWidth(element);
      }
      if (element.className === LAYOUT_CLASS_SECTION_GUTTER){
        minWidth += element.clientWidth;
      }
    }
    return minWidth;
  }

  private GetSectionMinWidth(section: HTMLElement){
    let children = section.childNodes;
    let minWidth = 0;
    for (let idx = 0; idx < children.length; idx++){
      let element = <HTMLElement> children[idx];
      switch (element.className){
        case LAYOUT_CLASS_ROW:{
          minWidth = Math.max(minWidth, this.GetRowMinWithBeforeGutter(element));
          break;
        }
        case LAYOUT_CLASS_CONTEXT:{
          return parseInt(window.getComputedStyle(element).minWidth)
        }
      }
    }
    return minWidth;
  }

  private GetSectionMinHeightBeforeGutter(section: HTMLElement, gutter?: HTMLElement): number{
    let children = section.childNodes;
    let minHeight = 0;
    for (let idx = 0; idx < children.length; idx++){
      let element = <HTMLElement> children[idx];
      if (element === gutter){
        return minHeight;
      }
      switch (element.className){
        case LAYOUT_CLASS_ROW:{
          minHeight += this.GetRowMinHeight(element);
          break;
        }
        case LAYOUT_CLASS_ROW_GUTTER:{
          minHeight += element.clientHeight;
          break;
        }
        case LAYOUT_CLASS_CONTEXT:{
          return parseInt(window.getComputedStyle(element).minHeight)
        }
      }
    }
    return minHeight;
  }

  private GetSectionMinHeightAfterGutter(section: HTMLElement, gutter?: HTMLElement): number{
    let children = section.childNodes;
    let minHeight = 0;
    for (let idx = children.length - 1; idx >= 0; idx--){
      let element = <HTMLElement> children[idx];
      if (element === gutter){
        return minHeight;
      }
      switch (element.className){
        case LAYOUT_CLASS_ROW:{
          minHeight += this.GetRowMinHeight(element);
          break;
        }
        case LAYOUT_CLASS_ROW_GUTTER:{
          minHeight += element.clientHeight;
          break;
        }
        case LAYOUT_CLASS_CONTEXT:{
          return parseInt(window.getComputedStyle(element).minHeight)
        }
      }
    }
    return minHeight;
  }

  private GetRowMinHeight(row: HTMLElement){
    let children = row.childNodes;
    let minHeight = 0;
    for (let idx = 0; idx < children.length; idx++){
      let element = <HTMLElement> children[idx];
      switch (element.className){
        case LAYOUT_CLASS_SECTION:{
          minHeight = Math.max(minHeight, this.GetSectionMinHeightAfterGutter(element));
          break;
        }
      }
    }
    return minHeight;
  }

  private GetLayoutMinWidth(){
    return this.GetSectionMinWidth(this.container);
  }

  private GetLayoutMinHeight(){
    return this.GetSectionMinHeightAfterGutter(this.container);
  }

  private SetLayoutClientMinSize(){
    this.container.style.minWidth = this.GetLayoutMinWidth() + "px";
    this.container.style.minHeight = this.GetLayoutMinHeight() + "px";
  }

  public OnDragStart(gutter: View){
    let HTMLRep = gutter.HTMLRepresentation;
    delete this.pointer;
    if (!this.pointer){
      this.pointer = new Pointer();
      this.pointer.InitBuffer(HTMLRep.offsetLeft, HTMLRep.offsetTop);
    }
  };

  private UpdateSectionGutterBackward(gutter: HTMLElement, gutterOffset: number, update: boolean = true): boolean{
    let section = <HTMLElement> gutter.previousSibling;
    let sectionMinWidth = this.GetSectionMinWidth(section);
    let sectionOffset = section.offsetLeft;
    let sectionWidth = gutterOffset - sectionOffset;

    if (sectionWidth <= sectionMinWidth){
      let parent = <HTMLElement> section.parentNode;
      if (parent.firstChild === section){
        this.UpdateSectionGutter(gutter, sectionMinWidth);
        return false;
      }
      else{
        let sectionGutter = <HTMLElement> section.previousSibling;
        if (this.UpdateSectionGutterBackward(sectionGutter, gutterOffset - sectionMinWidth - gutter.clientWidth)){
          if (update){
            this.UpdateSectionGutter(gutter, gutterOffset);
          }
          return true;
        }
        else{
          this.UpdateSectionGutter(gutter, sectionOffset + sectionMinWidth);
          return false;
        }
      }
      this.RearrangeSectionGutters(section, sectionMinWidth);
    }
    else{
      if (update){
        this.UpdateSectionGutter(gutter, gutterOffset);
      }
      this.RearrangeSectionGutters(section, sectionWidth);
      return true;
    }
    return false;
  }

  private UpdateSectionGutterForward(gutter: HTMLElement, gutterOffset: number, update: boolean = true): boolean{
    let section = <HTMLElement> gutter.nextSibling;
    let sectionMinWidth = this.GetSectionMinWidth(section);
    let sectionOffset = section.offsetLeft;
    let sectionWidth = section.clientWidth - ((gutterOffset + gutter.clientWidth) - sectionOffset);
    if (sectionWidth <= sectionMinWidth){
      let parent = <HTMLElement> section.parentNode;
      if (parent.lastChild === section){
        this.UpdateSectionGutter(gutter, parent.clientWidth - sectionMinWidth - gutter.clientWidth);
        return false;
      }
      else{
        let sectionGutter = <HTMLElement> section.nextSibling;
        if (this.UpdateSectionGutterForward(sectionGutter, gutterOffset + gutter.clientWidth + sectionMinWidth)){
          if (update){
            this.UpdateSectionGutter(gutter, gutterOffset);
          }
          return true;
        }
        else{
          this.UpdateSectionGutter(gutter, sectionGutter.offsetLeft - sectionMinWidth - gutter.clientWidth);
          return false;
        }
      }
      this.RearrangeSectionGutters(section, sectionMinWidth);
    }
    else{
      if (update){
        this.UpdateSectionGutter(gutter, gutterOffset);
      }
      this.RearrangeSectionGutters(section, sectionWidth);
      return true;
    }
    return false;
  }

  private RearrangeSectionGutters(section: HTMLElement, width: number){
    let currentWidth = section.clientWidth;
    let sectionChildren = section.childNodes;
    for (let ids = 0; ids < sectionChildren.length; ids++){
      let row = <HTMLElement> sectionChildren[ids];
      if (row.className === LAYOUT_CLASS_ROW){
        let rowChildren = row.childNodes;
        if (rowChildren.length > 1){
          let section = <HTMLElement> row.lastChild;
          let gutter = <HTMLElement> section.previousSibling;
          let gutterOffset = gutter.offsetLeft;
          let sectionMinWidth = this.GetSectionMinWidth(section);
          let sectionOffset = section.offsetLeft;
          let sectionWidth = width - gutterOffset - gutter.clientWidth;
          if (sectionWidth <= sectionMinWidth){
            this.UpdateSectionGutterBackward(gutter, gutterOffset - (currentWidth - width)- gutter.clientWidth);
          }
          this.RearrangeSectionGutters(section, sectionWidth);
        }
      }
    }
  }

  private UpdateRowGutterBackward(gutter: HTMLElement, gutterOffset: number, update: boolean = true): boolean{
    let row = <HTMLElement> gutter.previousSibling;
    let rowMinHeight = this.GetRowMinHeight(row);
    let rowOffset = row.offsetTop;
    let rowHeight = gutterOffset - rowOffset;

    if (rowHeight <= rowMinHeight){
      let parent = <HTMLElement> row.parentNode;
      if (parent.firstChild === row){
        this.UpdateRowGutter(gutter, rowMinHeight);
        return false;
      }
      else{
        let rowGutter = <HTMLElement> row.previousSibling;
        if (this.UpdateRowGutterBackward(rowGutter, gutterOffset - rowMinHeight - gutter.clientHeight)){
          if (update){
            this.UpdateRowGutter(gutter, gutterOffset);
          }
          return true;
        }
        else{
          this.UpdateRowGutter(gutter, rowOffset + rowMinHeight);
          return false;
        }
      }
      this.RearrangeRowGutters(row, rowMinHeight);
    }
    else{
      if (update){
        this.UpdateRowGutter(gutter, gutterOffset);
      }
      this.RearrangeRowGutters(row, rowHeight);
      return true;
    }
    return false;
  }

  private UpdateRowGutterForward(gutter: HTMLElement, gutterOffset: number, update: boolean = true): boolean{
    let row = <HTMLElement> gutter.nextSibling;
    let rowMinHeight = this.GetRowMinHeight(row);
    let rowOffset = row.offsetTop;
    let rowHeight = row.clientHeight - ((gutterOffset + gutter.clientHeight) - rowOffset);
    if (rowHeight <= rowMinHeight){
      let parent = <HTMLElement> row.parentNode;
      if (parent.lastChild === row){
        this.UpdateRowGutter(gutter, parent.clientHeight - rowMinHeight - gutter.clientHeight);
        return false;
      }
      else{
        let rowGutter = <HTMLElement> row.nextSibling;
        if (this.UpdateRowGutterForward(rowGutter, gutterOffset + gutter.clientHeight + rowMinHeight)){
          if (update){
            this.UpdateRowGutter(gutter, gutterOffset);
          }
          return true;
        }
        else{
          this.UpdateRowGutter(gutter, rowGutter.offsetTop - rowMinHeight - gutter.clientHeight);
          return false;
        }
      }
      this.RearrangeRowGutters(row, rowMinHeight);
    }
    else{
      if (update){
        this.UpdateRowGutter(gutter, gutterOffset);
      }
      this.RearrangeRowGutters(row, rowHeight);
      return true;
    }
    return false;
  }

  private RearrangeRowGutters(row: HTMLElement, height: number){
    let currentHeight = row.clientHeight;
    let rowChildren = row.childNodes;
    for (let idr = 0; idr < rowChildren.length; idr++){
      let section = <HTMLElement> rowChildren[idr];
      if (section.className === LAYOUT_CLASS_SECTION){
        let sectionChildren = section.childNodes;
        if (sectionChildren.length > 1){
          let row = <HTMLElement> section.lastChild;
          let gutter = <HTMLElement> row.previousSibling;
          let gutterOffset = gutter.offsetTop;
          let rowMinHeight = this.GetRowMinHeight(row);
          let rowOffset = row.offsetTop;
          let rowHeight = height - gutterOffset - gutter.clientHeight;
          if (rowHeight <= rowMinHeight){
            this.UpdateRowGutterBackward(gutter, gutterOffset - (currentHeight - height) - gutter.clientHeight);
          }
          this.RearrangeRowGutters(row, rowHeight);
        }
      }
    }
  }

  private MapAndScaleLayoutGutters(container: HTMLElement, clientSize: {width: number, height: number}){
    let guttesOffsetMap: Map <HTMLElement, number> = new Map;
    let widthRatio = container.clientWidth / clientSize.width;
    let heightRatio = container.clientHeight / clientSize.height;
    let containerChildren = container.childNodes;
    for (let idx = 0; idx < containerChildren.length; idx++){
      let containerElement = <HTMLElement> containerChildren[idx];
      switch(containerElement.className){
        case LAYOUT_CLASS_SECTION_GUTTER:{
          let gutter = containerElement;
          let gutterOffset;
          let previousSection = <HTMLElement> gutter.previousSibling;
          let previousSectionMinWidth = this.GetSectionMinWidth(previousSection);
          let nextSection = <HTMLElement> gutter.nextSibling;
          if (previousSection === container.firstChild){
            gutterOffset = widthRatio * previousSection.clientWidth;
            if (gutterOffset < previousSectionMinWidth){
              gutterOffset = previousSectionMinWidth;
            }
            if (nextSection === container.lastChild){ //Issue:The gutter has wrong offset when previous gutter 
              let nextSectionMinWidth = this.GetSectionMinWidth(nextSection);
              let nextSectionWidth = clientSize.width - gutterOffset + gutter.clientWidth;
              if (nextSectionWidth < nextSectionMinWidth){
                let gutterOffsetCalculatedFromTheRight = clientSize.width - nextSectionWidth - gutter.clientWidth;
                if (gutterOffsetCalculatedFromTheRight < gutterOffset){
                  this.RecalculateSectionGuttersOffsetBackward(gutter, gutterOffsetCalculatedFromTheRight, guttesOffsetMap);
                }
                gutterOffset = gutterOffsetCalculatedFromTheRight;
              }
            }
            guttesOffsetMap.set(gutter, gutterOffset);
          }
          else{
            let previousGutter = <HTMLElement> previousSection.previousSibling;
            let previousGutterOffset = <number> guttesOffsetMap.get(previousGutter);
            let previousSectionOffset = previousGutterOffset + previousGutter.clientWidth;
            gutterOffset = widthRatio * gutter.offsetLeft;
            let sectionWidth = gutterOffset - previousSectionOffset;
            if (sectionWidth < previousSectionMinWidth){
              gutterOffset = previousSectionOffset + previousSectionMinWidth;
            }
            if (nextSection === container.lastChild){
              let nextSectionMinWidth = this.GetSectionMinWidth(nextSection);
              let nextSectionWidth = clientSize.width - gutterOffset + gutter.clientWidth;
              if (nextSectionWidth < nextSectionMinWidth){
                let gutterOffsetCalculatedFromTheRight = clientSize.width - nextSectionWidth - gutter.clientWidth;
                if (gutterOffsetCalculatedFromTheRight < gutterOffset){
                  this.RecalculateSectionGuttersOffsetBackward(gutter, gutterOffsetCalculatedFromTheRight, guttesOffsetMap);
                }
                gutterOffset = gutterOffsetCalculatedFromTheRight;
              }
            }
            guttesOffsetMap.set(gutter, gutterOffset);
          }
          break;
        }
        case LAYOUT_CLASS_ROW_GUTTER:{
          let gutter = containerElement;
          let gutterOffset;
          let previousRow = <HTMLElement> gutter.previousSibling;
          let previousRowMinHeight = this.GetRowMinHeight(previousRow);
          let nextRow = <HTMLElement> gutter.nextSibling;
          if (previousRow === container.firstChild){
            gutterOffset = heightRatio * previousRow.clientHeight;
            if (gutterOffset < previousRowMinHeight){
              gutterOffset = previousRowMinHeight;
            }
            if (nextRow === container.lastChild){
              let nextRowMinHeight = this.GetRowMinHeight(nextRow);
              let nextRowHeight = clientSize.height - gutterOffset + gutter.clientHeight;
              if (nextRowHeight < nextRowMinHeight){
                let gutterOffsetCalculatedFromTheRight = clientSize.height - nextRowHeight - gutter.clientHeight;
                if (gutterOffsetCalculatedFromTheRight < gutterOffset){
                  this.RecalculateRowGuttersOffsetBackward(gutter, gutterOffsetCalculatedFromTheRight, guttesOffsetMap);
                }
                gutterOffset = gutterOffsetCalculatedFromTheRight;
              }
            }
            guttesOffsetMap.set(gutter, gutterOffset);
          }
          else{
            let previousGutter = <HTMLElement> previousRow.previousSibling;
            let previousGutterOffset = <number> guttesOffsetMap.get(previousGutter);
            let previousRowOffset = previousGutterOffset + previousGutter.clientHeight;
            gutterOffset = heightRatio * gutter.offsetTop;
            let rowHeight = gutterOffset - previousRowOffset;
            if (rowHeight < previousRowMinHeight){
              gutterOffset = previousRowOffset + previousRowMinHeight;
            }
            if (nextRow === container.lastChild){
              let nextRowMinHeight = this.GetRowMinHeight(nextRow);
              let nextRowHeight = clientSize.height - gutterOffset + gutter.clientHeight;
              if (nextRowHeight < nextRowMinHeight){
                let gutterOffsetCalculatedFromTheRight = clientSize.height - nextRowHeight - gutter.clientHeight;
                if (gutterOffsetCalculatedFromTheRight < gutterOffset){
                  this.RecalculateRowGuttersOffsetBackward(gutter, gutterOffsetCalculatedFromTheRight, guttesOffsetMap);
                }
                gutterOffset = gutterOffsetCalculatedFromTheRight;
              }
            }
            guttesOffsetMap.set(gutter, gutterOffset);
          }
          break;
        }
      }
    }
    this.ResizeLayoutGutters(container, guttesOffsetMap);
    this.ResizeLayoutElements(container);
  }

  private ResizeLayoutGutters(container: HTMLElement, guttesOffsetMap: Map <HTMLElement, number>){
    let containerChildren = container.childNodes;
    for (let idx = 0; idx < containerChildren.length; idx++){
      let containerElement = <HTMLElement> containerChildren[idx];
      switch(containerElement.className){
        case LAYOUT_CLASS_SECTION_GUTTER:{
          let gutter = containerElement;
          let gutterOffset = <number> guttesOffsetMap.get(gutter);
          this.UpdateSectionGutter(gutter, gutterOffset);
          break;
        }
        case LAYOUT_CLASS_ROW_GUTTER:{
          let gutter = containerElement;
          let gutterOffset = <number> guttesOffsetMap.get(gutter);
          this.UpdateRowGutter(gutter, gutterOffset);
          break;
        }
      }
    }
  }

  private ResizeLayoutElements(container: HTMLElement){
    let containerChildren = container.childNodes;
    for (let idx = 0; idx < containerChildren.length; idx++){
      let containerElement = <HTMLElement> containerChildren[idx];
      switch(containerElement.className){
        case LAYOUT_CLASS_SECTION:{
          let section = containerElement;
          let sectionClientSize = {width: section.clientWidth, height: section.clientHeight};
          this.FitSection(section);
          this.MapAndScaleLayoutGutters(section, sectionClientSize);
          break;
        }
        case LAYOUT_CLASS_ROW:{
          let row = containerElement;
          let rowClientSize = {width: row.clientWidth, height: row.clientHeight};
          this.FitRow(row);
          this.MapAndScaleLayoutGutters(row, rowClientSize);
          break;
        }
      }
    }
  }

  private RecalculateSectionGuttersOffsetBackward(gutter: HTMLElement, gutterOffset:number, guttesOffsetMap: Map <HTMLElement, number>){
    let parent = <HTMLElement> gutter.parentNode;
    let previousSection = <HTMLElement> gutter.previousSibling;
    if (previousSection !== parent.firstChild){
      let previousGutter = <HTMLElement> previousSection.previousSibling;
      let sectionMinWidth = this.GetSectionMinWidth(previousSection);
      let sectionWidth = gutterOffset - <number> guttesOffsetMap.get(previousGutter) + previousGutter.clientWidth;
      let previousGutterOffset;
      if (sectionWidth < sectionMinWidth){
        previousGutterOffset = gutterOffset - sectionMinWidth - previousGutter.clientWidth;
        guttesOffsetMap.set(previousGutter, previousGutterOffset);
        this.RecalculateSectionGuttersOffsetBackward(previousGutter, previousGutterOffset, guttesOffsetMap);
      }
    }
  }

  private RecalculateRowGuttersOffsetBackward(gutter: HTMLElement, gutterOffset:number, guttesOffsetMap: Map <HTMLElement, number>){
    let parent = <HTMLElement> gutter.parentNode;
    let previousRow = <HTMLElement> gutter.previousSibling;
    if (previousRow !== parent.firstChild){
      let previousGutter = <HTMLElement> previousRow.previousSibling;
      let rowMinHeight = this.GetRowMinHeight(previousRow);
      let rowHeight = gutterOffset - <number> guttesOffsetMap.get(previousGutter) + previousGutter.clientHeight;
      let previousGutterOffset;
      if (rowHeight < rowMinHeight){
        previousGutterOffset = gutterOffset - rowMinHeight - previousGutter.clientHeight;
        guttesOffsetMap.set(previousGutter, previousGutterOffset);
        this.RecalculateRowGuttersOffsetBackward(previousGutter, previousGutterOffset, guttesOffsetMap);
      }
    }
  }

  private UpdateLayoutClientSize(){
    this.layoutClientSize.width = this.container.clientWidth;
    this.layoutClientSize.height = this.container.clientHeight;
  }

  private ScaleLayout(){
    this.MapAndScaleLayoutGutters(this.container, this.layoutClientSize);
    this.UpdateLayoutClientSize();
  }

  public OnDragging(gutter: View){
    let HTMLRep = gutter.HTMLRepresentation;
    if (this.pointer){
      let [mX, mY] = this.pointer.Mouse();
      let [x, y] = this.pointer.GetBuffer(mX, mY);
      let [dX, dY] = this.pointer.GetDiff();
      switch (HTMLRep.className){
        case LAYOUT_CLASS_SECTION_GUTTER:{
          let leftSection = <HTMLElement> HTMLRep.previousSibling;
          let leftSectionMinWidth = this.GetSectionMinWidth(leftSection);
          let rightSection = <HTMLElement> HTMLRep.nextSibling;
          let rightSectionMinWidth = this.GetSectionMinWidth(rightSection);
          let parent = <HTMLElement> HTMLRep.parentNode;
          if (parent.firstChild === leftSection){
            if (x <= leftSectionMinWidth){
              x = leftSectionMinWidth;
            }
          }
          else if (parent.lastChild === rightSection){
            if (parent.clientWidth - x + HTMLRep.clientWidth < rightSectionMinWidth){
              x = parent.clientWidth - rightSectionMinWidth - HTMLRep.clientWidth;
            }
          }
          if (this.UpdateSectionGutterBackward(HTMLRep, x, false) && this.UpdateSectionGutterForward(HTMLRep, x, false)){
            this.UpdateSectionGutter(HTMLRep, x);
          }
          break;
        }
        case LAYOUT_CLASS_ROW_GUTTER:{
          let topRow = <HTMLElement> HTMLRep.previousSibling;
          let topRowMinHeight = this.GetRowMinHeight(topRow);
          let bottomRow = <HTMLElement> HTMLRep.nextSibling;
          let bottomRowMinHeight = this.GetRowMinHeight(bottomRow);
          let parent = <HTMLElement> HTMLRep.parentNode;
          if (parent.firstChild === topRow){
            if (y <= topRowMinHeight){
              y = topRowMinHeight;
            }
          }
          else if (parent.lastChild === bottomRow){
            if (parent.clientHeight - y + HTMLRep.clientHeight < bottomRowMinHeight){
              y = parent.clientHeight - bottomRowMinHeight - HTMLRep.clientHeight;
            }
          }
          if (this.UpdateRowGutterBackward(HTMLRep, y, false) && this.UpdateRowGutterForward(HTMLRep, y, false)){
            this.UpdateRowGutter(HTMLRep, y);
          }
          break;
        }
      }
    this.UpdateElement(<HTMLElement> HTMLRep.parentNode);
    }
  };

  public OnDragStop(gutter: View){
    delete this.pointer;
  };

  public GetLayoutInstance(){
    return this;
  }

  public GetContextList(){
    let elements = this.container.querySelectorAll("." + LAYOUT_CLASS_CONTEXT);
    return elements;
  }

  public GetSectionFromContext(context: HTMLElement){
    if (context){
      let section = <HTMLElement> context.parentNode;
      return section;
    }
  }

  public GetContextFromSection(section: HTMLElement){
    let context = this.GetContext(section);
    if (context.className === LAYOUT_CLASS_CONTEXT)
      return context;
    else
      return null;
  }

  public SwapContextInSections(fromSection: HTMLElement, toSection: HTMLElement){
    let contextToMove = this.GetContext(fromSection);
    if (contextToMove){
      let contextToRemove = this.GetContext(toSection);
      if (contextToRemove){
        fromSection.appendChild(contextToRemove);
      }
      toSection.appendChild(contextToMove);
    }
  }

  public SplitVertically(context: HTMLElement | undefined){
    if (context){
      let section = <HTMLElement> context.parentNode;
      let newSection = this.SplitSection(section);
      if (newSection){
        this.SetLayoutClientMinSize();
        this.Update();
        return this.GetContext(newSection);
      }
    }
  }

  public SplitHorizontally(context: HTMLElement | undefined){
    if (context){
      let section = <HTMLElement> context.parentNode;
      let newSection = this.SplitRow(section);
      if (newSection){
        this.SetLayoutClientMinSize();
        this.Update();
        return this.GetContext(newSection);
      }
    }
  }

  public Join(context: HTMLElement | undefined){ // Sections Right/Bottom Join or Last Section Left/Top Join
    if (context){
      let section = <HTMLElement> context.parentNode;
      let row = <HTMLElement> section.parentNode;
      if (row){
        if (row.querySelector("." + LAYOUT_CLASS_SECTION_GUTTER) === null && row.querySelector("." + LAYOUT_CLASS_ROW_GUTTER) === null){   //Single Section in Row
          this.Join(section);
        }
        else{
          let sectionGutter;
          if (row.lastChild === section){   //Last Section in Row
            sectionGutter = section.previousSibling;
          }
          else{   //Regular Section in Row
            sectionGutter = section.nextSibling;
          }
          row.removeChild(section);
          if (sectionGutter){
            row.removeChild(sectionGutter)
          }
          this.UpdateElement(row);
          this.SetLayoutClientMinSize();
        }
      }
    }
  }

  public Show(){

  }

  public Hide(){

  }

  public Update(){
    this.ScaleLayout();
  }

}
