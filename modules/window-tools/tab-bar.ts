import {View, ViewEventResponces, ViewEventExceptions} from "./view";
import {Layout} from "./layout";
import {Area} from "./area";
import {Pointer} from "./pointer";
import {Tab} from "./tab";
import {HTMLRepresentation} from "./dom-object";
import {LayoutManager} from "./layout-manager";
import {ContextMenu} from "./context-menu";

import {ProcessView} from "./process-view";

export class TabBar extends View{
  private pointer: Pointer | null;
  private tabCollection: Set<Tab> = new Set;
  private contentArea: Area | undefined;
  private buttonNew: Tab;
  public constructor(parentView: View, contentArea?: Area){
    super(parentView, HTMLRepresentation.CreateHTMLElement("tab-bar", parentView.HTMLRepresentation));
    this.pointer = null;
    this.contentArea = contentArea;
    LayoutManager.RegisterEventListener(this);
    this.buttonNew = new Tab(this, null, "＋");
    this.buttonNew.TabTypeButton();
    this.SetupEventListnersForButton(this.buttonNew);
  }

  public SetContentArea(area: Area){
    area.SetController(this);
    this.contentArea = area;
  }

  public Tab(targetView: View, title: string ){
    let tab = new Tab(this, targetView, title);
    this.SetupEventListners(tab);
    LayoutManager.ObserveViewObject(tab);
    this.InsertTab(tab);
    tab.Activate();
    tab.ShowTargetView();
    this.Update();
  }

  public InsertTab(tab: Tab){
    this.tabCollection.add(tab);
  }

  public RemoveTab(tab: Tab){
    this.tabCollection.delete(tab);
  }


  private SetupEventListners(tab: Tab){
    tab.HTMLRepresentation.addEventListener("click", (e) =>{
        e.preventDefault();
        delete this.pointer;
        tab.Activate();
        tab.ShowTargetView();
      });

      tab.HTMLRepresentation.addEventListener("contextmenu", (e)=>{
          let contextMenu: ContextMenu = LayoutManager.CreateContextMenu();
          contextMenu.AddMenuItem(this.DeleteView, {
            name: 'CloseTab',
            label: 'Close this Tab',
            title: 'Close Tab'
        })
      })

    tab.HTMLRepresentation.addEventListener("mousedown", (e) =>{
        e.preventDefault();
        delete this.pointer;
        tab.Activate();
        tab.ShowTargetView();
      });
  }

  private SetupEventListnersForButton(button: Tab){
    button.HTMLRepresentation.addEventListener("click", (e)=>{
      let parent = this.GetParentView();
      if (parent){
        let tabView = LayoutManager.GetTabViewForHTMLRepresentation(parent.HTMLRepresentation);
        if (tabView){
          LayoutManager.CreateDropDownMenu(this, button);
          //tabView.CreateProcessView(ProcessView, "Process View");
        }
      }
    })
  }

  private PlaceTabInOrder(tabHTMLRepresentation: HTMLElement){
    let tabs = this.HTMLRepresentation.childNodes;
    for (let idx = 0; idx < tabs.length; idx++){
      let currTab = <HTMLElement> tabs[idx];
      if (tabHTMLRepresentation.clientLeft + tabHTMLRepresentation.clientWidth < currTab.clientLeft + 2 * currTab.clientWidth / 3)
        HTMLRepresentation.SwapChildElements(tabHTMLRepresentation, currTab);
    }
  }


  private CheckTabOverlayPosition(tabHTMLRepresentation: HTMLElement){
    let tabs = this.HTMLRepresentation.childNodes;
    for (let idx = 0; idx < tabs.length; idx++){
      let currTab = <HTMLElement> tabs[idx];
      if (tabHTMLRepresentation === currTab && this.pointer){
        let [X] = this.pointer.GetVector();
        if (X < 0 && idx > 0){
          let prevTab = <HTMLElement> tabs[idx - 1];
          if (Math.abs(X) > 2 * prevTab.clientWidth / 3){
            let prevTabLeft = prevTab.offsetLeft;
            prevTab.style.left = prevTabLeft + currTab.clientWidth + "px";
            this.pointer.resetVectorPivot(prevTabLeft, 0);
            HTMLRepresentation.SwapChildElements(prevTab, currTab);
          }
        }
        else if (X > 0 && idx < tabs.length - 1){
          let nextTab = <HTMLElement> tabs[idx + 1];
          if (X > nextTab.clientWidth / 3){
            let nextTabLeft = nextTab.offsetLeft;
            nextTab.style.left = nextTabLeft - currTab.clientWidth + "px";
            this.pointer.resetVectorPivot(nextTabLeft, 0);
            HTMLRepresentation.SwapChildElements(currTab, nextTab);
          }
        }
      }
    }
  }

  public OnDragStart(tab: View){
    if (!this.pointer){
      this.pointer = new Pointer();
      let TabHTMLRepresentation = tab.HTMLRepresentation;
      this.pointer.InitBuffer(TabHTMLRepresentation.offsetLeft, TabHTMLRepresentation.offsetTop);
    }
  }

  public OnDragging(tab: View){
    if (this.pointer){
      let [mX, mY] = this.pointer.Mouse();
      let [X] = this.pointer.GetBuffer(mX, mY);
      let TabHTMLRepresentation = tab.HTMLRepresentation;
      TabHTMLRepresentation.style.left = X > 0 ?  X + "px": 0 + "px";
      this.CheckTabOverlayPosition(TabHTMLRepresentation);
    }
  }

  public OnDragStop(tab: View){
    if (this.pointer){
      let [X] = this.pointer.GetInitPosition();
      tab.HTMLRepresentation.style.left = X + "px";
    }
    delete this.pointer;
    this.Update();
  }

  public OnDragEnter(tab: Tab){
    tab.SetParentView(this);
  }

  public OnObservableViewChange(tab: Tab){
    this.OnDragStop(tab);
  }

  public OnObservableViewDidChange(tab: Tab){
    this.HandleTabDragEnter(tab);
  }

  private HandleTabDragEnter(tab: Tab){
    this.InsertTab(tab);
    tab.Activate();
    //TO_DO Should be something like this.pointer.Reset()
    if (!this.pointer){
      this.pointer = new Pointer();
      let TabHTMLRepresentation = tab.HTMLRepresentation;
      let [relX] = this.pointer.MouseRelativeToElement(this.HTMLRepresentation);
      let relOffsetLeft = relX - TabHTMLRepresentation.clientWidth / 3;
      let tabOffsetLeft = relOffsetLeft > 0 ? relOffsetLeft: 0;
      TabHTMLRepresentation.style.left = tabOffsetLeft + "px";
      TabHTMLRepresentation.style.top = 0 + "px";
      this.PlaceTabInOrder(TabHTMLRepresentation);
      this.Update();
      this.pointer.InitBuffer(tabOffsetLeft, 0);
      this.pointer.resetVectorPivot(TabHTMLRepresentation.offsetLeft, 0);
      TabHTMLRepresentation.style.left = tabOffsetLeft + "px";
    }
  }

  public OnObservableViewException(tab: Tab, exception: ViewEventExceptions){
    if (exception === ViewEventExceptions.ON_CHANGE_PARENT_VIEW_WITH_THE_SAME_VIEW){
      this.HandleTabDragEnter(tab);
    }
  }

  public OnDragLeave(tab: Tab){
    delete this.pointer;
    this.RemoveTab(tab);
    this.ActivateDefaultTab();
    this.Update();
  }

  public OnDrop(tab: Tab){
    delete this.pointer;
    tab.Activate();
    if (this.contentArea && tab.ParentDidChange())
      this.contentArea.RelocateChildView(tab.TargetView());
    this.Update();
  }


  public TabCollection(){
    return this.tabCollection;
  }

  public Show(){

  }

  public Hide(){

  }

  public SetActiveTab(tab: Tab){
    tab.Activate();
  }

  public OnTabActivated(tab: Tab){
    tab.ShowTargetView();
    this.tabCollection.forEach((tabFromCollection) =>{
      if (tab !== tabFromCollection){
        tabFromCollection.Deactivate();
        let area = tab.TargetView().GetParentView();
        let sectionFromArea = area ? area.GetParentView() : null;
        let HTMLRepSectionFromArea = sectionFromArea ? sectionFromArea.HTMLRepresentation : null;
        let sectionFromTabBar = this.GetParentView();
        let HTMLRepSectionFromTabBar = sectionFromTabBar ? sectionFromTabBar.HTMLRepresentation : null;
        if (HTMLRepSectionFromArea === HTMLRepSectionFromTabBar){
          tabFromCollection.HideTargetView();
        }
      }
    })
  }

  public ActivateDefaultTab(){
    let idx = 0;
    let firstTab: Tab;
    if (firstTab = Array.from(this.tabCollection)[0]){
      firstTab.Activate();
      firstTab.ShowTargetView();
    };
  }

  private findInArray(array: Array<HTMLElement>, element: HTMLElement){
    let found = false;
    for (let idx = 0; idx < array.length; idx++){
      if (array[idx] === element)
      found = true;
    }
    return found;
  }

  public UpdateButton(){
    let buttonHTMLRep = this.buttonNew.HTMLRepresentation;
    this.HTMLRepresentation.appendChild(buttonHTMLRep);
  }

  public Update(){
    let tabsInCollection = Array.from(this.tabCollection);
    let tabsHTMLRepInCollection = new Array;
    this.UpdateButton();
    for (let idx = 0; idx < tabsInCollection.length; idx++){
      tabsHTMLRepInCollection.push(tabsInCollection[idx].HTMLRepresentation);
    }
    let tabs = this.HTMLRepresentation.childNodes;
    if (tabs.length == 1){
      let tab = <HTMLElement> tabs[0];
      tab.style.left = 0 + "px";
      tab.style.top = 0 + "px";
    }
    else if (tabs.length > 1){
      let prevTab = <HTMLElement> tabs[0];
      let nextTab = <HTMLElement> tabs[1];
      if (this.findInArray(tabsHTMLRepInCollection, prevTab)){
        prevTab.style.left = 0 + "px";
        prevTab.style.top = 0 + "px";
      }
      else{
        nextTab.style.left = 0 + "px";
        nextTab.style.top = 0 + "px";
      }
      for (let idx = 1; idx < tabs.length; idx++){
        let tab = <HTMLElement> tabs[idx];
        if (this.findInArray(tabsHTMLRepInCollection, tab) || tab === this.buttonNew.HTMLRepresentation){
          if (this.findInArray(tabsHTMLRepInCollection, prevTab)){
            tab.style.left = prevTab.offsetLeft + prevTab.clientWidth + "px";
            prevTab = tab;
          }
          else{
            tab.style.left = 0 + "px";
            prevTab = tab;
          }
          tab.style.top = 0 + "px";
        }
      }
    }
  }

}
