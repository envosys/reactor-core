"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var view_1 = require("../window-tools/view");
var dom_object_1 = require("../window-tools/dom-object");
var ProcessView = /** @class */ (function (_super) {
    __extends(ProcessView, _super);
    function ProcessView(parentView, src) {
        if (src === void 0) { src = "about:blank"; }
        var _this = _super.call(this, parentView, dom_object_1.HTMLRepresentation.CreateProcessView("viewport", src, parentView.HTMLRepresentation)) || this;
        _this.isShown = true;
        _this.HTMLRepresentation.addEventListener('console-message', function (e) {
            console.log(e);
        });
        return _this;
    }
    ProcessView.prototype.Show = function () {
        if (!this.isShown) {
            this.HTMLRepresentation.style.display = "flex";
            this.isShown = true;
        }
    };
    ProcessView.prototype.Hide = function () {
        if (this.isShown) {
            this.HTMLRepresentation.style.display = "none";
            this.isShown = false;
        }
    };
    ProcessView.prototype.Update = function () {
        throw new Error("Method not implemented.");
    };
    return ProcessView;
}(view_1.View));
exports.ProcessView = ProcessView;
