"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var view_1 = require("./view");
var dom_object_1 = require("./dom-object");
var layout_manager_1 = require("./layout-manager");
var pointer_1 = require("./pointer");
var HighLightFrameMode;
(function (HighLightFrameMode) {
    HighLightFrameMode[HighLightFrameMode["HighLightHide"] = 0] = "HighLightHide";
    HighLightFrameMode[HighLightFrameMode["HighLightFill"] = 1] = "HighLightFill";
    HighLightFrameMode[HighLightFrameMode["HighLightLeft"] = 2] = "HighLightLeft";
    HighLightFrameMode[HighLightFrameMode["HighLightRight"] = 3] = "HighLightRight";
    HighLightFrameMode[HighLightFrameMode["HighLightTop"] = 4] = "HighLightTop";
    HighLightFrameMode[HighLightFrameMode["HighLightBottom"] = 5] = "HighLightBottom";
})(HighLightFrameMode || (HighLightFrameMode = {}));
var Area = /** @class */ (function (_super) {
    __extends(Area, _super);
    function Area(parentView) {
        var _this = _super.call(this, parentView, dom_object_1.HTMLRepresentation.CreateHTMLElement("content-area", parentView.HTMLRepresentation)) || this;
        _this.pointer = null;
        layout_manager_1.LayoutManager.RegisterEventListener(_this);
        _this.frameHighlight = _this.CreateHighlightFrame();
        _this.ShowHighLightFrame(HighLightFrameMode.HighLightHide);
        _this.lastHighlightModeState = HighLightFrameMode.HighLightHide;
        return _this;
    }
    Area.prototype.CreateHighlightFrame = function () {
        var higlightFrame = dom_object_1.HTMLRepresentation.CreateHTMLElement("highlight-frame", this.HTMLRepresentation);
        return higlightFrame;
    };
    Area.prototype.ShowHighLightFrame = function (mode) {
        this.frameHighlight.removeAttribute("style");
        this.lastHighlightModeState = mode;
        switch (mode) {
            case HighLightFrameMode.HighLightHide: {
                this.frameHighlight.style.display = "none";
                break;
            }
            case HighLightFrameMode.HighLightFill: {
                this.frameHighlight.style.display = "flex";
                this.frameHighlight.style.left = "0px";
                this.frameHighlight.style.right = "0px";
                this.frameHighlight.style.top = "0px";
                this.frameHighlight.style.bottom = "0px";
                break;
            }
            case HighLightFrameMode.HighLightLeft: {
                this.frameHighlight.style.display = "flex";
                this.frameHighlight.style.width = "50%";
                this.frameHighlight.style.height = "100%";
                this.frameHighlight.style.left = "0px";
                break;
            }
            case HighLightFrameMode.HighLightRight: {
                this.frameHighlight.style.display = "flex";
                this.frameHighlight.style.width = "50%";
                this.frameHighlight.style.height = "100%";
                this.frameHighlight.style.right = "0px";
                break;
            }
            case HighLightFrameMode.HighLightTop: {
                this.frameHighlight.style.display = "flex";
                this.frameHighlight.style.height = "50%";
                this.frameHighlight.style.width = "100%";
                this.frameHighlight.style.top = "0px";
                break;
            }
            case HighLightFrameMode.HighLightBottom: {
                this.frameHighlight.style.display = "flex";
                this.frameHighlight.style.height = "50%";
                this.frameHighlight.style.width = "100%";
                this.frameHighlight.style.bottom = "0px";
                break;
            }
        }
    };
    Area.prototype.HideHighlightFrame = function () {
        this.ShowHighLightFrame(HighLightFrameMode.HighLightHide);
    };
    Area.prototype.SetTabBarStyle = function (tabBarHTMLRep, isInitial) {
        if (isInitial) {
            tabBarHTMLRep.style.overflow = "hidden";
        }
        else {
            tabBarHTMLRep.style.overflow = "visible";
        }
    };
    Area.prototype.SetTabStyle = function (tabHTMLRep, isInitial) {
        if (isInitial) {
            tabHTMLRep.className = "tab active";
        }
        else {
            tabHTMLRep.className = "tab active hover";
        }
    };
    Area.prototype.SetTabBarStyleForTab = function (tab) {
        var tabBar = tab.GetParentView();
        if (tabBar) {
            var tabBarHTMLRep = tabBar.HTMLRepresentation;
            var tabHTMLRep = tab.HTMLRepresentation;
            this.SetTabBarStyle(tabBarHTMLRep, true);
            this.SetTabStyle(tabHTMLRep, true);
        }
    };
    Area.prototype.OnHighlightFillDrop = function (tab) {
        if (this.controller) {
            tab.SetParentView(this.controller);
            this.controller.OnDrop(tab);
        }
        this.HideHighlightFrame();
    };
    Area.prototype.OnHighlightLeftDrop = function (tab) {
        var parent = this.GetParentView();
        if (parent) {
            var tabView = layout_manager_1.LayoutManager.GetTabViewForHTMLRepresentation(parent.HTMLRepresentation);
            if (tabView) {
                var leftTabView = tabView.CreateLeftSideTabView();
                var controller = leftTabView.TabBar();
                tab.SetParentView(controller);
                controller.OnDrop(tab);
                this.HideHighlightFrame();
            }
        }
    };
    Area.prototype.OnHighlightRightDrop = function (tab) {
        var parent = this.GetParentView();
        if (parent) {
            var tabView = layout_manager_1.LayoutManager.GetTabViewForHTMLRepresentation(parent.HTMLRepresentation);
            if (tabView) {
                var rightTabView = tabView.CreateRightSideTabView();
                var controller = rightTabView.TabBar();
                tab.SetParentView(controller);
                controller.OnDrop(tab);
                this.HideHighlightFrame();
            }
        }
    };
    Area.prototype.OnHighlightTopDrop = function (tab) {
        var parent = this.GetParentView();
        if (parent) {
            var tabView = layout_manager_1.LayoutManager.GetTabViewForHTMLRepresentation(parent.HTMLRepresentation);
            if (tabView) {
                var topTabView = tabView.CreateUpperSideTabView();
                var controller = topTabView.TabBar();
                tab.SetParentView(controller);
                controller.OnDrop(tab);
                this.HideHighlightFrame();
            }
        }
    };
    Area.prototype.OnHighlightBottomDrop = function (tab) {
        var parent = this.GetParentView();
        if (parent) {
            var tabView = layout_manager_1.LayoutManager.GetTabViewForHTMLRepresentation(parent.HTMLRepresentation);
            if (tabView) {
                var bottomTabView = tabView.CreateLowerSideTabView();
                var controller = bottomTabView.TabBar();
                tab.SetParentView(controller);
                controller.OnDrop(tab);
                this.HideHighlightFrame();
            }
        }
    };
    Area.prototype.SetController = function (controller) {
        this.controller = controller;
    };
    Area.prototype.OnDragEnter = function (tab) {
        this.pointer = new pointer_1.Pointer();
        var TabHTMLRepresentation = tab.HTMLRepresentation;
        var tabBar = tab.GetParentView();
        if (tabBar) {
            var tabBarHTMLRep = tabBar.HTMLRepresentation;
            this.SetTabBarStyle(tabBarHTMLRep, false);
            this.SetTabStyle(tab.HTMLRepresentation, false);
            var _a = this.pointer.MouseRelativeToElement(tabBarHTMLRep), relX = _a[0], relY = _a[1];
            var relOffsetLeftX = relX - TabHTMLRepresentation.clientWidth / 3;
            var relOffsetLeftY = relY - TabHTMLRepresentation.clientHeight / 2;
            TabHTMLRepresentation.style.left = relOffsetLeftX + "px";
            TabHTMLRepresentation.style.top = relOffsetLeftY + "px";
            this.pointer.InitBuffer(relOffsetLeftX, relOffsetLeftY);
            this.pointer.resetVectorPivot(TabHTMLRepresentation.offsetLeft, TabHTMLRepresentation.offsetTop);
            tab.HTMLRepresentation.style.zIndex = "999";
        }
    };
    Area.prototype.OnDragging = function (tab) {
        if (this.pointer) {
            var _a = this.pointer.Mouse(), mX = _a[0], mY = _a[1];
            var _b = this.pointer.GetBuffer(mX, mY), X = _b[0], Y = _b[1];
            var TabHTMLRepresentation = tab.HTMLRepresentation;
            TabHTMLRepresentation.style.left = X + "px";
            TabHTMLRepresentation.style.top = Y + "px";
            var _c = this.pointer.MouseRelativeToElement(this.HTMLRepresentation), relX = _c[0], relY = _c[1];
            var ratioX = relX / this.HTMLRepresentation.clientWidth;
            var ratioY = relY / this.HTMLRepresentation.clientHeight;
            if (ratioX < 0.25)
                this.ShowHighLightFrame(HighLightFrameMode.HighLightLeft);
            else if (ratioX > 0.75)
                this.ShowHighLightFrame(HighLightFrameMode.HighLightRight);
            else if (ratioY < 0.25)
                this.ShowHighLightFrame(HighLightFrameMode.HighLightTop);
            else if (ratioY > 0.75)
                this.ShowHighLightFrame(HighLightFrameMode.HighLightBottom);
            else
                this.ShowHighLightFrame(HighLightFrameMode.HighLightFill);
        }
    };
    Area.prototype.OnDrop = function (tab) {
        this.SetTabBarStyleForTab(tab);
        switch (this.lastHighlightModeState) {
            case HighLightFrameMode.HighLightFill: {
                this.OnHighlightFillDrop(tab);
                break;
            }
            case HighLightFrameMode.HighLightLeft: {
                this.OnHighlightLeftDrop(tab);
                break;
            }
            case HighLightFrameMode.HighLightRight: {
                this.OnHighlightRightDrop(tab);
                break;
            }
            case HighLightFrameMode.HighLightTop: {
                this.OnHighlightTopDrop(tab);
                break;
            }
            case HighLightFrameMode.HighLightBottom: {
                this.OnHighlightBottomDrop(tab);
                break;
            }
            default: {
                this.OnHighlightFillDrop(tab);
            }
        }
    };
    Area.prototype.OnObservableViewChange = function (view) {
    };
    Area.prototype.OnObservableViewDidChange = function (view) {
    };
    Area.prototype.OnChildViewAboutToRelocate = function (view) {
    };
    Area.prototype.OnChildViewRelocate = function (view) {
        if (this.views.size < 1) {
            var tabView = this.GetParentView();
            if (tabView) {
                tabView.Release();
            }
        }
    };
    Area.prototype.OnChildViewDidRelocate = function (view) {
    };
    Area.prototype.OnDragLeave = function (tab) {
        this.HideHighlightFrame();
        delete this.pointer;
    };
    Area.prototype.Show = function () {
    };
    Area.prototype.Hide = function () {
    };
    Area.prototype.Update = function () {
        console.log("Area Update...");
    };
    return Area;
}(view_1.View));
exports.Area = Area;
