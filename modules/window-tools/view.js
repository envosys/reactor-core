"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var draggable_1 = require("./draggable");
var ViewEventResponces;
(function (ViewEventResponces) {
    ViewEventResponces[ViewEventResponces["ACCEPT_VIEW_CHANGE"] = 0] = "ACCEPT_VIEW_CHANGE";
    ViewEventResponces[ViewEventResponces["REJECT_VIEW_CHANGE"] = 1] = "REJECT_VIEW_CHANGE";
})(ViewEventResponces = exports.ViewEventResponces || (exports.ViewEventResponces = {}));
var ViewEventExceptions;
(function (ViewEventExceptions) {
    ViewEventExceptions[ViewEventExceptions["ON_CHANGE_PARENT_VIEW_WITH_THE_SAME_VIEW"] = 0] = "ON_CHANGE_PARENT_VIEW_WITH_THE_SAME_VIEW";
    ViewEventExceptions[ViewEventExceptions["ON_CHANGE_PARENT_VIEW_WITH_ITSELF"] = 1] = "ON_CHANGE_PARENT_VIEW_WITH_ITSELF";
    ViewEventExceptions[ViewEventExceptions["ON_CHANGE_PARENT_VIEW_WITH_UNDEFINED"] = 2] = "ON_CHANGE_PARENT_VIEW_WITH_UNDEFINED";
})(ViewEventExceptions = exports.ViewEventExceptions || (exports.ViewEventExceptions = {}));
var View = /** @class */ (function (_super) {
    __extends(View, _super);
    function View(parentView, HTMLRepresentation) {
        var _this = _super.call(this, HTMLRepresentation) || this;
        _this.views = new Set;
        _this.parentDidChange = false;
        if (parentView)
            parentView.AddChildView(_this);
        _this.parentView = parentView;
        return _this;
    }
    View.prototype.AddChildView = function (view) {
        this.views.add(view);
    };
    View.prototype.RemoveChildView = function (view) {
        this.views.delete(view);
    };
    View.prototype.Release = function () {
        this.views.forEach(function (view) {
            view.Release();
        });
    };
    View.prototype.DeleteView = function () {
        this.Release();
    };
    View.prototype.RelocateChildView = function (view) {
        var parentView = view.GetParentView();
        if (parentView) {
            parentView.OnChildViewAboutToRelocate(view);
        }
        view.SetParentView(this);
        if (parentView) {
            parentView.OnChildViewRelocate(view);
        }
        this.OnChildViewDidRelocate(view);
    };
    View.prototype.SetParentView = function (parentView) {
        if (this.parentView) {
            if (parentView === this) {
                this.parentView.OnObservableViewException(this, ViewEventExceptions.ON_CHANGE_PARENT_VIEW_WITH_ITSELF);
            }
            else if (parentView !== this.parentView) {
                if (this.parentView.OnObservableViewAboutToChange(parentView) !== ViewEventResponces.REJECT_VIEW_CHANGE) {
                    this.parentView.RemoveChildView(this);
                    this.OnParentViewChange(this.parentView);
                    this.parentDidChange = true;
                    var replacedView = this.parentView;
                    this.parentView = parentView;
                    this.parentView.AddChildView(this);
                    this.parentView.HTMLRepresentation.append(this.HTMLRepresentation);
                    replacedView.OnObservableViewChange(this);
                    this.parentView.OnObservableViewDidChange(this);
                }
            }
            else {
                this.parentView.OnObservableViewException(this, ViewEventExceptions.ON_CHANGE_PARENT_VIEW_WITH_THE_SAME_VIEW);
            }
        }
    };
    View.prototype.GetParentView = function () {
        return this.parentView;
    };
    View.prototype.OnObservableViewAboutToChange = function (view) {
        return ViewEventResponces.ACCEPT_VIEW_CHANGE;
    };
    View.prototype.OnObservableViewChange = function (view) { };
    ; //Second -> Call for current Parent
    View.prototype.OnObservableViewDidChange = function (view) { };
    ; //Third -> Call for new Parent
    View.prototype.OnParentViewChange = function (view) { };
    ;
    View.prototype.OnChildViewAboutToRelocate = function (view) { }; //First Call -> for current Parent
    View.prototype.OnChildViewRelocate = function (view) { };
    View.prototype.OnChildViewDidRelocate = function (view) { }; //Forth -> Final Call for new Parent
    View.prototype.ParentDidChange = function () {
        if (this.parentDidChange) {
            this.parentDidChange = false;
            return true;
        }
        else
            return false;
    };
    View.prototype.OnObservableViewException = function (view, exception) { };
    ;
    return View;
}(draggable_1.Draggable));
exports.View = View;
