export namespace HTMLRepresentation{

  export function CreateHTMLElement(classNameProperties: string | object, parentNode?: HTMLElement, afterHTMLElement?: HTMLElement){
    let element = <HTMLElement> (document.createElement('div'));
    if (typeof classNameProperties === 'string'){
      element.className = classNameProperties;
    }
    else{
      for (let [key, value] of Object.entries(classNameProperties)) {
        switch (key){
          case "className":{
            element.className = value;
            break;
          }
          case "id":{
            element.id = value;
            break;
          }
          case "attributes":{
            for (let [attr_key, attr_value] of Object.entries(<object> value)) {
              element.setAttribute(attr_key, attr_value);
            }
            break;
          }
        }
      }
    }
    if (parentNode){
      if (afterHTMLElement)
        parentNode.insertBefore(element, afterHTMLElement.nextSibling);
      else
        parentNode.appendChild(element);
    }
    return element;
  }

  export function innerText(element: HTMLElement, text: string){
    element.innerText = text;
  }

  export function SwapChildElements(src: HTMLElement, dest: HTMLElement){
    let srcParent = src.parentNode;
    let destParent = dest.parentNode;
    if (srcParent === destParent){
      src.before(dest);
    }
  }

  export function RemoveChildElements(parent: HTMLElement){
    while (parent.firstChild){
      parent.firstChild.remove();
    }
  }

  export function isSiblingElement(element: HTMLElement, sibling: HTMLElement, opts: {elementFirst: boolean}){
    let parent = element.parentNode;
    if (parent){
      let children = parent.childNodes;
      for (let idx = 0; idx < children.length; idx++){
        if (children[idx] === element){
          opts.elementFirst = true;
        }
        if (children[idx] === sibling){
          return true;
        }
      }
    }
    return false;
  }

  export function CreateProcessView(className: string, src: string, parent?: HTMLElement | null){
    let element = <HTMLElement> (document.createElement('webview'));
    element.className = className;
    element.setAttribute("src", src);
    element.setAttribute("nodeintegration", "");
    if (parent){
      parent.appendChild(element);
    }
    return element;
  }

  export abstract class DomObject{
    readonly HTMLRepresentation: HTMLElement;
    protected constructor(HTMLRepresentation: HTMLElement){
      this.HTMLRepresentation = HTMLRepresentation;
    }
  }
}
