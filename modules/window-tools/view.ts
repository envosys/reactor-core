import {Draggable} from "./draggable";
import {Layout} from "./layout";

export enum ViewEventResponces{
  ACCEPT_VIEW_CHANGE,
  REJECT_VIEW_CHANGE
}

export enum ViewEventExceptions{
  ON_CHANGE_PARENT_VIEW_WITH_THE_SAME_VIEW,
  ON_CHANGE_PARENT_VIEW_WITH_ITSELF,
  ON_CHANGE_PARENT_VIEW_WITH_UNDEFINED
}

export abstract class View extends Draggable{
    protected parentView: View | null;
    protected views: Set<View> = new Set;
    private parentDidChange: boolean = false;
    protected constructor(parentView: View | null, HTMLRepresentation: HTMLElement){
      super(HTMLRepresentation);
      if (parentView)
        parentView.AddChildView(this);
      this.parentView = parentView;
    }

    abstract Show(): void;
    abstract Hide(): void;
    abstract Update(): void;

    public AddChildView(view:View){
      this.views.add(view);
    }

    public RemoveChildView(view: View){
      this.views.delete(view);
    }

    public Release(){
      this.views.forEach((view) =>{
        view.Release();
      });
    }

    public DeleteView(){
      this.Release();
    }

    public RelocateChildView(view: View){
      let parentView = view.GetParentView();
      if (parentView){
        parentView.OnChildViewAboutToRelocate(view);
      }
      view.SetParentView(this);
      if (parentView){
        parentView.OnChildViewRelocate(view);
      }
      this.OnChildViewDidRelocate(view);
    }

    public SetParentView(parentView: View){
      if (this.parentView){
        if (parentView === this){
          this.parentView.OnObservableViewException(this, ViewEventExceptions.ON_CHANGE_PARENT_VIEW_WITH_ITSELF);
        }
        else if (parentView !== this.parentView){
          if (this.parentView.OnObservableViewAboutToChange(parentView) !== ViewEventResponces.REJECT_VIEW_CHANGE){
            this.parentView.RemoveChildView(this);
            this.OnParentViewChange(this.parentView);
            this.parentDidChange = true;
            let replacedView = this.parentView;
            this.parentView = parentView;
            this.parentView.AddChildView(this);
            this.parentView.HTMLRepresentation.append(this.HTMLRepresentation);
            replacedView.OnObservableViewChange(this);
            this.parentView.OnObservableViewDidChange(this);
          }
        }
        else{
            this.parentView.OnObservableViewException(this, ViewEventExceptions.ON_CHANGE_PARENT_VIEW_WITH_THE_SAME_VIEW);
        }
      }
    }

    public GetParentView(){
      return this.parentView;
    }

    public OnObservableViewAboutToChange(view: View){
      return ViewEventResponces.ACCEPT_VIEW_CHANGE;
    }

    public OnObservableViewChange(view: View){}; //Second -> Call for current Parent

    public OnObservableViewDidChange(view: View){}; //Third -> Call for new Parent

    public OnParentViewChange(view: View){};

    public OnChildViewAboutToRelocate(view: View){}  //First Call -> for current Parent

    public OnChildViewRelocate(view: View){}

    public OnChildViewDidRelocate(view: View){} //Forth -> Final Call for new Parent

    public ParentDidChange(){
      if (this.parentDidChange){
        this.parentDidChange = false;
        return true;
      }
      else
        return false;
    }

    public OnObservableViewException(view: View, exception: ViewEventExceptions){};

}
