import {View} from "../window-tools/view"
import {Layout} from "../window-tools/layout"
import {HTMLRepresentation} from "../window-tools/dom-object";

export class ProcessView extends View{
  private isShown: boolean = true;
  public constructor(parentView: View, src: string = "about:blank"){
    super(parentView,
       HTMLRepresentation.CreateProcessView("viewport", src, parentView.HTMLRepresentation)
    );
    this.HTMLRepresentation.addEventListener('console-message', (e: Event) => {
      console.log(e);
    })
  }

    Show(): void {
      if (!this.isShown){
        this.HTMLRepresentation.style.display = "flex";
        this.isShown = true;
      }
    }

    Hide(): void {
      if (this.isShown){
        this.HTMLRepresentation.style.display = "none";
        this.isShown = false;
      }
    }

    Update(): void {
      throw new Error("Method not implemented.");
    }
}
