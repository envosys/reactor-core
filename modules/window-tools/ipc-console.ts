import {ipcRenderer} from "electron";

export interface ipcConsoleMessage{
  type: any,
  message: string
}

export class IpcConsole{
  private type: any;
  public constructor (type: any){
    this.type = type;
  }

  public log(msg: any){
    let arg: ipcConsoleMessage = {
      type: this.type,
      message: msg
    }
    ipcRenderer.send('log-message', arg);
  }
}
