"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var view_1 = require("./view");
var pointer_1 = require("./pointer");
var tab_1 = require("./tab");
var dom_object_1 = require("./dom-object");
var layout_manager_1 = require("./layout-manager");
var TabBar = /** @class */ (function (_super) {
    __extends(TabBar, _super);
    function TabBar(parentView, contentArea) {
        var _this = _super.call(this, parentView, dom_object_1.HTMLRepresentation.CreateHTMLElement("tab-bar", parentView.HTMLRepresentation)) || this;
        _this.tabCollection = new Set;
        _this.pointer = null;
        _this.contentArea = contentArea;
        layout_manager_1.LayoutManager.RegisterEventListener(_this);
        _this.buttonNew = new tab_1.Tab(_this, null, "＋");
        _this.buttonNew.TabTypeButton();
        _this.SetupEventListnersForButton(_this.buttonNew);
        return _this;
    }
    TabBar.prototype.SetContentArea = function (area) {
        area.SetController(this);
        this.contentArea = area;
    };
    TabBar.prototype.Tab = function (targetView, title) {
        var tab = new tab_1.Tab(this, targetView, title);
        this.SetupEventListners(tab);
        layout_manager_1.LayoutManager.ObserveViewObject(tab);
        this.InsertTab(tab);
        tab.Activate();
        tab.ShowTargetView();
        this.Update();
    };
    TabBar.prototype.InsertTab = function (tab) {
        this.tabCollection.add(tab);
    };
    TabBar.prototype.RemoveTab = function (tab) {
        this.tabCollection.delete(tab);
    };
    TabBar.prototype.SetupEventListners = function (tab) {
        var _this = this;
        tab.HTMLRepresentation.addEventListener("click", function (e) {
            e.preventDefault();
            delete _this.pointer;
            tab.Activate();
            tab.ShowTargetView();
        });
        tab.HTMLRepresentation.addEventListener("contextmenu", function (e) {
            var contextMenu = layout_manager_1.LayoutManager.CreateContextMenu();
            contextMenu.AddMenuItem(_this.DeleteView, {
                name: 'CloseTab',
                label: 'Close this Tab',
                title: 'Close Tab'
            });
        });
        tab.HTMLRepresentation.addEventListener("mousedown", function (e) {
            e.preventDefault();
            delete _this.pointer;
            tab.Activate();
            tab.ShowTargetView();
        });
    };
    TabBar.prototype.SetupEventListnersForButton = function (button) {
        var _this = this;
        button.HTMLRepresentation.addEventListener("click", function (e) {
            var parent = _this.GetParentView();
            if (parent) {
                var tabView = layout_manager_1.LayoutManager.GetTabViewForHTMLRepresentation(parent.HTMLRepresentation);
                if (tabView) {
                    layout_manager_1.LayoutManager.CreateDropDownMenu(_this, button);
                    //tabView.CreateProcessView(ProcessView, "Process View");
                }
            }
        });
    };
    TabBar.prototype.PlaceTabInOrder = function (tabHTMLRepresentation) {
        var tabs = this.HTMLRepresentation.childNodes;
        for (var idx = 0; idx < tabs.length; idx++) {
            var currTab = tabs[idx];
            if (tabHTMLRepresentation.clientLeft + tabHTMLRepresentation.clientWidth < currTab.clientLeft + 2 * currTab.clientWidth / 3)
                dom_object_1.HTMLRepresentation.SwapChildElements(tabHTMLRepresentation, currTab);
        }
    };
    TabBar.prototype.CheckTabOverlayPosition = function (tabHTMLRepresentation) {
        var tabs = this.HTMLRepresentation.childNodes;
        for (var idx = 0; idx < tabs.length; idx++) {
            var currTab = tabs[idx];
            if (tabHTMLRepresentation === currTab && this.pointer) {
                var X = this.pointer.GetVector()[0];
                if (X < 0 && idx > 0) {
                    var prevTab = tabs[idx - 1];
                    if (Math.abs(X) > 2 * prevTab.clientWidth / 3) {
                        var prevTabLeft = prevTab.offsetLeft;
                        prevTab.style.left = prevTabLeft + currTab.clientWidth + "px";
                        this.pointer.resetVectorPivot(prevTabLeft, 0);
                        dom_object_1.HTMLRepresentation.SwapChildElements(prevTab, currTab);
                    }
                }
                else if (X > 0 && idx < tabs.length - 1) {
                    var nextTab = tabs[idx + 1];
                    if (X > nextTab.clientWidth / 3) {
                        var nextTabLeft = nextTab.offsetLeft;
                        nextTab.style.left = nextTabLeft - currTab.clientWidth + "px";
                        this.pointer.resetVectorPivot(nextTabLeft, 0);
                        dom_object_1.HTMLRepresentation.SwapChildElements(currTab, nextTab);
                    }
                }
            }
        }
    };
    TabBar.prototype.OnDragStart = function (tab) {
        if (!this.pointer) {
            this.pointer = new pointer_1.Pointer();
            var TabHTMLRepresentation = tab.HTMLRepresentation;
            this.pointer.InitBuffer(TabHTMLRepresentation.offsetLeft, TabHTMLRepresentation.offsetTop);
        }
    };
    TabBar.prototype.OnDragging = function (tab) {
        if (this.pointer) {
            var _a = this.pointer.Mouse(), mX = _a[0], mY = _a[1];
            var X = this.pointer.GetBuffer(mX, mY)[0];
            var TabHTMLRepresentation = tab.HTMLRepresentation;
            TabHTMLRepresentation.style.left = X > 0 ? X + "px" : 0 + "px";
            this.CheckTabOverlayPosition(TabHTMLRepresentation);
        }
    };
    TabBar.prototype.OnDragStop = function (tab) {
        if (this.pointer) {
            var X = this.pointer.GetInitPosition()[0];
            tab.HTMLRepresentation.style.left = X + "px";
        }
        delete this.pointer;
        this.Update();
    };
    TabBar.prototype.OnDragEnter = function (tab) {
        tab.SetParentView(this);
    };
    TabBar.prototype.OnObservableViewChange = function (tab) {
        this.OnDragStop(tab);
    };
    TabBar.prototype.OnObservableViewDidChange = function (tab) {
        this.HandleTabDragEnter(tab);
    };
    TabBar.prototype.HandleTabDragEnter = function (tab) {
        this.InsertTab(tab);
        tab.Activate();
        //TO_DO Should be something like this.pointer.Reset()
        if (!this.pointer) {
            this.pointer = new pointer_1.Pointer();
            var TabHTMLRepresentation = tab.HTMLRepresentation;
            var relX = this.pointer.MouseRelativeToElement(this.HTMLRepresentation)[0];
            var relOffsetLeft = relX - TabHTMLRepresentation.clientWidth / 3;
            var tabOffsetLeft = relOffsetLeft > 0 ? relOffsetLeft : 0;
            TabHTMLRepresentation.style.left = tabOffsetLeft + "px";
            TabHTMLRepresentation.style.top = 0 + "px";
            this.PlaceTabInOrder(TabHTMLRepresentation);
            this.Update();
            this.pointer.InitBuffer(tabOffsetLeft, 0);
            this.pointer.resetVectorPivot(TabHTMLRepresentation.offsetLeft, 0);
            TabHTMLRepresentation.style.left = tabOffsetLeft + "px";
        }
    };
    TabBar.prototype.OnObservableViewException = function (tab, exception) {
        if (exception === view_1.ViewEventExceptions.ON_CHANGE_PARENT_VIEW_WITH_THE_SAME_VIEW) {
            this.HandleTabDragEnter(tab);
        }
    };
    TabBar.prototype.OnDragLeave = function (tab) {
        delete this.pointer;
        this.RemoveTab(tab);
        this.ActivateDefaultTab();
        this.Update();
    };
    TabBar.prototype.OnDrop = function (tab) {
        delete this.pointer;
        tab.Activate();
        if (this.contentArea && tab.ParentDidChange())
            this.contentArea.RelocateChildView(tab.TargetView());
        this.Update();
    };
    TabBar.prototype.TabCollection = function () {
        return this.tabCollection;
    };
    TabBar.prototype.Show = function () {
    };
    TabBar.prototype.Hide = function () {
    };
    TabBar.prototype.SetActiveTab = function (tab) {
        tab.Activate();
    };
    TabBar.prototype.OnTabActivated = function (tab) {
        var _this = this;
        tab.ShowTargetView();
        this.tabCollection.forEach(function (tabFromCollection) {
            if (tab !== tabFromCollection) {
                tabFromCollection.Deactivate();
                var area = tab.TargetView().GetParentView();
                var sectionFromArea = area ? area.GetParentView() : null;
                var HTMLRepSectionFromArea = sectionFromArea ? sectionFromArea.HTMLRepresentation : null;
                var sectionFromTabBar = _this.GetParentView();
                var HTMLRepSectionFromTabBar = sectionFromTabBar ? sectionFromTabBar.HTMLRepresentation : null;
                if (HTMLRepSectionFromArea === HTMLRepSectionFromTabBar) {
                    tabFromCollection.HideTargetView();
                }
            }
        });
    };
    TabBar.prototype.ActivateDefaultTab = function () {
        var idx = 0;
        var firstTab;
        if (firstTab = Array.from(this.tabCollection)[0]) {
            firstTab.Activate();
            firstTab.ShowTargetView();
        }
        ;
    };
    TabBar.prototype.findInArray = function (array, element) {
        var found = false;
        for (var idx = 0; idx < array.length; idx++) {
            if (array[idx] === element)
                found = true;
        }
        return found;
    };
    TabBar.prototype.UpdateButton = function () {
        var buttonHTMLRep = this.buttonNew.HTMLRepresentation;
        this.HTMLRepresentation.appendChild(buttonHTMLRep);
    };
    TabBar.prototype.Update = function () {
        var tabsInCollection = Array.from(this.tabCollection);
        var tabsHTMLRepInCollection = new Array;
        this.UpdateButton();
        for (var idx = 0; idx < tabsInCollection.length; idx++) {
            tabsHTMLRepInCollection.push(tabsInCollection[idx].HTMLRepresentation);
        }
        var tabs = this.HTMLRepresentation.childNodes;
        if (tabs.length == 1) {
            var tab = tabs[0];
            tab.style.left = 0 + "px";
            tab.style.top = 0 + "px";
        }
        else if (tabs.length > 1) {
            var prevTab = tabs[0];
            var nextTab = tabs[1];
            if (this.findInArray(tabsHTMLRepInCollection, prevTab)) {
                prevTab.style.left = 0 + "px";
                prevTab.style.top = 0 + "px";
            }
            else {
                nextTab.style.left = 0 + "px";
                nextTab.style.top = 0 + "px";
            }
            for (var idx = 1; idx < tabs.length; idx++) {
                var tab = tabs[idx];
                if (this.findInArray(tabsHTMLRepInCollection, tab) || tab === this.buttonNew.HTMLRepresentation) {
                    if (this.findInArray(tabsHTMLRepInCollection, prevTab)) {
                        tab.style.left = prevTab.offsetLeft + prevTab.clientWidth + "px";
                        prevTab = tab;
                    }
                    else {
                        tab.style.left = 0 + "px";
                        prevTab = tab;
                    }
                    tab.style.top = 0 + "px";
                }
            }
        }
    };
    return TabBar;
}(view_1.View));
exports.TabBar = TabBar;
