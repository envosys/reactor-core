"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var electron_1 = require("electron");
var Workspace = /** @class */ (function () {
    function Workspace() {
        this.windows = new Set;
        this.SetupAppEventListeners();
        process.env.ELECTRON_DISABLE_SECURITY_WARNINGS = 'true';
    }
    Workspace.prototype.DefaultWIndow = function () {
        var win = new electron_1.BrowserWindow({
            show: false,
            minWidth: 800,
            minHeight: 600,
            backgroundColor: '#282c34',
            webPreferences: {
                nodeIntegration: true,
                nodeIntegrationInWorker: true
            }
        });
        win.setMenuBarVisibility(false);
        win.maximize();
        win.webContents.openDevTools();
        win.once('ready-to-show', function () {
            win.show();
        });
        win.loadFile('./modules/window-tools/html/window.html');
        win.webContents.on('did-finish-load', function () {
            console.log("Contents loaded");
        });
        this.windows.add(win);
        electron_1.ipcMain.on('window-process-message', function (event, arg) {
            console.log("Message from the Renderer process: " + arg);
            event.sender.send('main-process-message', 'Start loading Workspace');
        });
        electron_1.ipcMain.on('log-message', function (event, arg) {
            console.log("[Sender: " + arg.type);
            console.log("Message: " + arg.message + "]");
            win.webContents.send('log-message', arg);
        });
    };
    Workspace.prototype.SetupAppEventListeners = function () {
        var _this = this;
        electron_1.app.on('ready', function () {
            _this.DefaultWIndow();
        }).on('window-all-closed', function () {
            if (process.platform !== 'darwin') {
                electron_1.app.quit();
            }
        }).on('activate', function () {
            if (!_this.windows.size) {
                _this.DefaultWIndow();
            }
        });
    };
    return Workspace;
}());
exports.Workspace = Workspace;
