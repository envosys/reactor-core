const DRAG_EVENT_LAYER = "drag-event-layer";
import {ipcRenderer} from "electron";

import {Draggable} from "./draggable";
import {Layout} from "./layout";
import {View} from "./view";
import {ProcessView} from "./process-view";
import {TabBar} from "./tab-bar";
import {Area} from "./area";
import {Pane} from "./pane";
import {ContextMenu} from "./context-menu";

export namespace LayoutManager{

  enum LayoutEventStates{
    Waiting,
    Ready,
    Drag
  }

  let LayoutReference: Layout | undefined;
  let EventLayerReference: HTMLElement = <HTMLElement> document.getElementById(DRAG_EVENT_LAYER);

  let SharedEventListeners: Set<View> = new Set;
  let SharedObservableViewObject: View | undefined;
  let SharedActiveEventListener: View | undefined;
  let SharedEventState: LayoutEventStates = LayoutEventStates.Waiting;

  let ClientXY = {x: 0, y: 0};

  //This is an abstraction of Layout Context Array - the glue
  let TabViewCollection: Set<TabView> = new Set;


  EventLayerReference.addEventListener("mousemove", (e) =>{
    e.preventDefault();
    ClientXY.x = e.clientX;
    ClientXY.y = e.clientY;
    ProcessEvents();
  })

  EventLayerReference.addEventListener("mouseup", (e) =>{
    e.preventDefault();
    CompleteEvents();
    DeactivateEventLayer();
  })

  export function GetMouse(){
    return ClientXY;
  }

  function ProcessEvents(){
      //Check and Set an ActiveEventListener
      let newListener: View | undefined;
      if (SharedActiveEventListener !== LayoutReference){
        if (newListener = ActiveEventListenerShouldChange()){
          if (SharedActiveEventListener){
            let replacedListener = SharedActiveEventListener;
            SharedActiveEventListener = <View> newListener;
            replacedListener.OnDragLeave(SharedObservableViewObject);
            SharedActiveEventListener.OnDragEnter(SharedObservableViewObject);
          }
          else{
            SharedActiveEventListener = <View> newListener;
          }
        }
      }

      if (SharedEventState == LayoutEventStates.Ready){
        SharedActiveEventListener?.OnDragStart(SharedObservableViewObject);
        SharedEventState = LayoutEventStates.Drag;
      }
      else if (SharedEventState == LayoutEventStates.Drag){
        SharedActiveEventListener?.OnDragging(SharedObservableViewObject);
      }
  };

  function CompleteEvents(){
      if (SharedEventState == LayoutEventStates.Drag){
        if (SharedActiveEventListener){
          SharedActiveEventListener.OnDragStop(SharedObservableViewObject);
          SharedActiveEventListener.OnDrop(SharedObservableViewObject);
        }
      }
      else{
        //TO-DO OnClick SharedActiveEventListener?.OnClick(SharedObservableViewObject);
      }
      SharedActiveEventListener = undefined;
      SharedObservableViewObject = undefined;
      SharedEventState = LayoutEventStates.Waiting;
  };

  function ActiveEventListenerShouldChange(){
    for (let listener of Array.from(SharedEventListeners)){
      let boundingBox = listener.HTMLRepresentation.getBoundingClientRect();
      if (listener !== SharedActiveEventListener){
        if (ClientXY.x > boundingBox.left && ClientXY.x < boundingBox.right &&
            ClientXY.y > boundingBox.top && ClientXY.y < boundingBox.bottom){
          return listener;
        }
      }
    }
  }

  function ActivateEventLayer(element: HTMLElement){
    EventLayerReference.style.cursor = element.style.cursor;
    EventLayerReference.style.display = "block";
  }

  function DeactivateEventLayer(){
    EventLayerReference.style.cursor = "initial";
    EventLayerReference.style.display = "none";
  }

  export function SetupLayoutReference(layoutRef: Layout){
    if (!LayoutReference){
      LayoutReference = layoutRef;
    }
  }

  export function GetLayoutReference(){
    return LayoutReference;
  }

  export function WrapHTML(HTMLRepresentation: HTMLElement){
    return new HTMLWrapper(HTMLRepresentation);
  }

  export function RegisterEventListener(listener: View){
    SharedEventListeners.add(listener);
  }

  export function UnregisterEventListener(listener: View){
    SharedEventListeners.delete(listener);
  }

  export function ObserveViewObject(observable: View){
    observable.HTMLRepresentation.addEventListener("mousedown", (e) =>{
      e.preventDefault();
      SharedObservableViewObject = observable;
      ActivateEventLayer(observable.HTMLRepresentation);
      SharedEventState = LayoutEventStates.Ready;
      if (!observable.GetParentView()){ //If there is no parent view, Layout become an active event listener
        SharedActiveEventListener = LayoutReference;
      }
    })
  }

  export function CreateTabView(context: HTMLElement){
    return new TabView(context);
  }

  export function GetTabViewForHTMLRepresentation(element: HTMLElement): TabView | undefined{
    let tabViewCollectionArray = Array.from(TabViewCollection);
    for (let idx = 0; idx < tabViewCollectionArray.length; idx++){
      let tabView = tabViewCollectionArray[idx];
      let tabViewHTMLRep = tabView.HTMLRepresentation;
      if (tabViewHTMLRep === element)
        return tabView;
    }
    return undefined;
  }

  export function CreateDropDownMenu(controller: View, anchorView: View){
    let dropDownMenu = new Pane(<View> controller.GetParentView(), anchorView);
    return dropDownMenu;
  }

  export function CreateContextMenu(){
    return new ContextMenu();
  }

  // TabView Class is higher Abstraction of Context HTMLElement from layout.ts Structure
  class TabView extends View{
    private tabBar: TabBar;
    private contentArea: Area;
    constructor(HTMLRepresentation: HTMLElement){
      super(null, HTMLRepresentation);
      //this.HTMLRepresentation.style.minWidth = "64px";
      this.tabBar = new TabBar(this);
      this.contentArea = new Area(this);
      this.tabBar.SetContentArea(this.contentArea);
      TabViewCollection.add(this);
    }

    public TabBar(){
      return this.tabBar;
    }

    public Release(){
      let layout = <Layout> LayoutManager.GetLayoutReference();
      layout.Join(this.HTMLRepresentation);
      super.Release();
    }

    public ContentArea(){
      return this.contentArea;
    }

    public CreateProcessView(processViewClass: typeof ProcessView, title: string = "Untitled"){
      let view = new processViewClass(this.contentArea);
      this.tabBar.Tab(view, title);
    }

    public CreateLeftSideTabView(){
      let leftContext = this.HTMLRepresentation;
      let layout = <Layout> LayoutManager.GetLayoutReference();

      let rightContext = layout.SplitVertically(leftContext);
      let rightSection: HTMLElement | undefined;
      if (rightContext){
        rightSection = layout.GetSectionFromContext(rightContext);
      }
      let leftSection = layout.GetSectionFromContext(leftContext);
      if (leftSection && rightSection){
        layout.SwapContextInSections(leftSection, rightSection);
      }
      leftContext = <HTMLElement> rightContext;

      let leftTabView = LayoutManager.CreateTabView(leftContext);
      return leftTabView;
    }

    public CreateRightSideTabView(){
      let leftContext = this.HTMLRepresentation;
      let layout = <Layout> LayoutManager.GetLayoutReference();
      let rightContext = <HTMLElement> layout.SplitVertically(leftContext);
      let rightTabView = LayoutManager.CreateTabView(rightContext);
      return rightTabView;
    }

    public CreateUpperSideTabView(){
      let upperContext = this.HTMLRepresentation;
      let layout = <Layout> LayoutManager.GetLayoutReference();
      let lowerContext = layout.SplitHorizontally(upperContext);
      let upperSection = layout.GetSectionFromContext(upperContext); // Must be after layout.SplitHorizontally(upperContext);
      let lowerSection: HTMLElement | undefined;
      if (lowerContext){
        lowerSection = layout.GetSectionFromContext(lowerContext);
      }
      if (lowerSection && upperSection){
        layout.SwapContextInSections(lowerSection, upperSection);
      }
      upperContext = <HTMLElement> lowerContext;
      let upperTabView = LayoutManager.CreateTabView(upperContext);
      return upperTabView;
    }

    public CreateLowerSideTabView(){
      let upperContext = this.HTMLRepresentation;
      let layout = <Layout> LayoutManager.GetLayoutReference();
      let lowerContext = <HTMLElement> layout.SplitHorizontally(upperContext);
      let lowerTabView = LayoutManager.CreateTabView(lowerContext);
      return lowerTabView;
    }

    public Show(){}
    public Hide(){}
    public Update(){}

  }

  // HTMLWrapper Class is higher Abstraction of Context HTMLElement from layout.ts Structure
  class HTMLWrapper extends View{
    constructor(HTMLRepresentation: HTMLElement){
      super(null, HTMLRepresentation);
    }

    public Show(){}
    public Hide(){}
    public Update(){}
  }

}
