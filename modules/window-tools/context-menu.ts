import {View, ViewEventResponces, ViewEventExceptions} from "./view";
import {Pane} from "./pane"
import {HTMLRepresentation} from "./dom-object";
import {LayoutManager} from "./layout-manager"
import {Pointer} from "./pointer"

export interface ContextMenuItemOptions{
  name: string,
  label: string,
  title?: string
}

enum ContextMenuItemTypes{
  'Action',
  'Submenu',
  'Delimiter',
}

class ContextMenuItemAction{
  private actionFunc: (arg0: any)=> void;
  private args: Array <any> | null;
  public constructor(func: (arg0: any)=> void, args: Array <any> | null){
    this.actionFunc = func;
    this.args = args;
  }

  public Call(){
    this.actionFunc(this.args);
  }
}

export class MenuItem{
  private action: ContextMenuItemAction | ContextMenu | undefined;
  private type: ContextMenuItemTypes;
  private options: ContextMenuItemOptions | undefined;
  public constructor(type: ContextMenuItemTypes, action?: ContextMenuItemAction | ContextMenu, options?: ContextMenuItemOptions){
    this.type = type;
    this.options = options;
    switch (type){
      case ContextMenuItemTypes.Action:{
        this.action = action;
        break;
      }
      case ContextMenuItemTypes.Submenu:{

        break;
      }
      case ContextMenuItemTypes.Delimiter:{

        break;
      }
    }
  }

  public Execute(): void{
    if (this.type === ContextMenuItemTypes.Action && this.action){
      let action = <ContextMenuItemAction> this.action;
      action.Call();
    }
  }

  public isAction(): Boolean{
    return this.type === ContextMenuItemTypes.Action? true : false;
  }

  public isDelimiter(): Boolean{
    return this.type === ContextMenuItemTypes.Delimiter? true : false;
  }

  public isSubmenu(): Boolean{
    return this.type === ContextMenuItemTypes.Submenu? true : false;
  }

  public Type(): ContextMenuItemTypes{
    return this.type;
  }

  public Options(): ContextMenuItemOptions | undefined{
    return this.options;
  }
}

export class ContextMenu extends Pane{
  private menuItems: Map <string, MenuItem>;
  public constructor(){
    let pointer = new Pointer();
    super(<View> LayoutManager.GetLayoutReference(), [pointer.Mouse()[0], pointer.Mouse()[1]]);
    this.menuItems = new Map();
    this.Hide();
  }

  public AddMenuItem(actionFunc: (arg0: any)=> void | ContextMenuItemAction, options: ContextMenuItemOptions){
    let actionOptions: ContextMenuItemOptions = {name: '', label: '', title: ''};
    for (let [key, value] of Object.entries(options)){
      switch (key){
        case "name":{
          actionOptions.name = value;
          break;
        }
        case "label":{
          actionOptions.label = value;
          break;
        }
        case "title":{
          actionOptions.title = value;
          break;
        }
      }
    }
    let Action = new ContextMenuItemAction(actionFunc, null);
    let menuItem = new MenuItem(ContextMenuItemTypes.Action, Action, actionOptions);
    this.menuItems.set(actionOptions.name, menuItem);
    this.Update();
  }

  public AddDelimiter(){
    let menuItem = new MenuItem(ContextMenuItemTypes.Delimiter);
    this.menuItems.set(name, menuItem);
  }

  public AddSubMenu(contextMenu: ContextMenu){
    let menuItem = new MenuItem(ContextMenuItemTypes.Submenu, contextMenu);
    this.menuItems.set(name, menuItem);
  }

  public Update(){
    let HTMLRep = this.HTMLRepresentation;
    this.menuItems.forEach((item, name, map) =>{
      console.log('Name: ' + name);
      let opts = item.Options();
      if (opts != undefined){
        console.log('Label: ' + opts.label);
        console.log('Title: ' + opts.title);
      }
    })
  }
}
