let mouseX: number, mouseY: number;

document.addEventListener("mousemove", (e)=>{
  mouseX = e.clientX;
  mouseY = e.clientY;
})

function MouseCoordinates(){
  return [mouseX, mouseY];
}

export class Pointer{
  private X = 0;
  private Y = 0;
  private initX = 0;
  private initY = 0;
  private diffX = 0;
  private diffY = 0;
  private bufferX = 0;
  private bufferY = 0;
  private vPivotX = 0;
  private vPivotY = 0;
  private vectorRegister: Map <any, number>;
  public constructor(x: number = 0, y: number = 0){
    this.X = x;
    this.Y = y;
    this.vectorRegister = new Map;
  }

  public InitBuffer(x: number, y: number){
    this.bufferX = this.initX = this.vPivotX = x;
    this.bufferY = this.initY = this.vPivotY = y;
  }

  public GetPosition(x: number, y: number){
    this.Update(x, y);
    return [this.X, this.Y];
  }

  public Mouse(){
    return MouseCoordinates();
  }

  public ConstantVectorInRegister(key: any, value: number, reset: boolean = false){
    if (!this.vectorRegister.has(key) || reset){
      this.vectorRegister.set(key, value);
    }
    return <number> this.vectorRegister.get(key);
  }

  public MouseRelativeToElement(element: HTMLElement){
    let boundingBox = element.getBoundingClientRect();
    let coords = this.Mouse();
    return [coords[0] - boundingBox.left, coords[1] - boundingBox.top];
  }

  public GetDiff(){
    return [this.diffX, this.diffY];
  }

  public GetBuffer(x: number, y: number){
    this.Update(x, y);
    return [this.bufferX, this.bufferY];
  }

  public GetInitPosition(){
      return [this.initX, this.initY];
  }

  public GetVector(){
    return [this.bufferX - this.vPivotX, this.bufferY - this.vPivotY];
  }

  public GetVectorPivot(){
    return [this.vPivotX, this.vPivotY];
  }

  public resetVectorPivot(x: number, y: number){
    this.vPivotX = x;
    this.vPivotY = y;
  }

  public Update(x: number, y: number){
    this.diffX = x - (this.X != 0 ? this.X: x);
    this.diffY = y - (this.Y != 0 ? this.Y: y);
    this.bufferX += this.diffX;
    this.bufferY += this.diffY;
    this.X = x;
    this.Y = y;
  }
}

export function oddRound(n: number){
  return 2 * Math.floor(n/2) + 1;
  //return Math.round(n);
  //return n;
}
