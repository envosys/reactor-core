import {View} from "./view";
import {Layout} from "./layout";
import {HTMLRepresentation} from "./dom-object";
import {LayoutManager} from "./layout-manager";
import {TabBar} from "./tab-bar";
import {Tab} from "./tab";
import {Pointer} from "./pointer";


enum HighLightFrameMode{
  HighLightHide,
  HighLightFill,
  HighLightLeft,
  HighLightRight,
  HighLightTop,
  HighLightBottom
}

export class Area extends View{
  private controller: TabBar | undefined;
  private frameHighlight: HTMLElement;
  private lastHighlightModeState: HighLightFrameMode;
  private pointer: Pointer | null = null;
  public constructor(parentView: View){
    super(parentView, HTMLRepresentation.CreateHTMLElement("content-area", parentView.HTMLRepresentation));
    LayoutManager.RegisterEventListener(this);
    this.frameHighlight = this.CreateHighlightFrame();
    this.ShowHighLightFrame(HighLightFrameMode.HighLightHide);
    this.lastHighlightModeState = HighLightFrameMode.HighLightHide;
  }

  private CreateHighlightFrame(){
    let higlightFrame : HTMLElement = HTMLRepresentation.CreateHTMLElement("highlight-frame", this.HTMLRepresentation);
    return higlightFrame;
  }

  private ShowHighLightFrame(mode: HighLightFrameMode){
    this.frameHighlight.removeAttribute("style");
    this.lastHighlightModeState = mode;
    switch(mode){
      case HighLightFrameMode.HighLightHide:{
        this.frameHighlight.style.display = "none";
        break;
      }
      case HighLightFrameMode.HighLightFill:{
          this.frameHighlight.style.display = "flex";
          this.frameHighlight.style.left = "0px";
          this.frameHighlight.style.right = "0px";
          this.frameHighlight.style.top = "0px";
          this.frameHighlight.style.bottom = "0px";
        break;
      }
      case HighLightFrameMode.HighLightLeft:{
        this.frameHighlight.style.display = "flex";
        this.frameHighlight.style.width = "50%";
        this.frameHighlight.style.height = "100%";
        this.frameHighlight.style.left = "0px";
        break;
      }
      case HighLightFrameMode.HighLightRight:{
        this.frameHighlight.style.display = "flex";
        this.frameHighlight.style.width = "50%";
        this.frameHighlight.style.height = "100%";
        this.frameHighlight.style.right = "0px";
        break;
      }
      case HighLightFrameMode.HighLightTop:{
        this.frameHighlight.style.display = "flex";
        this.frameHighlight.style.height = "50%";
        this.frameHighlight.style.width = "100%";
        this.frameHighlight.style.top = "0px";
        break;
      }
      case HighLightFrameMode.HighLightBottom:{
        this.frameHighlight.style.display = "flex";
        this.frameHighlight.style.height = "50%";
        this.frameHighlight.style.width = "100%";
        this.frameHighlight.style.bottom = "0px";
        break;
      }
    }
  }

  private HideHighlightFrame(){
    this.ShowHighLightFrame(HighLightFrameMode.HighLightHide);
  }

  private SetTabBarStyle(tabBarHTMLRep: HTMLElement, isInitial: boolean){
    if (isInitial){
      tabBarHTMLRep.style.overflow = "hidden";
    }
    else{
      tabBarHTMLRep.style.overflow = "visible";
    }
  }

  private SetTabStyle(tabHTMLRep: HTMLElement, isInitial: boolean){
    if (isInitial){
      tabHTMLRep.className = "tab active";
    }
    else{
      tabHTMLRep.className = "tab active hover";
    }
  }

  private SetTabBarStyleForTab(tab: Tab){
    let tabBar = tab.GetParentView();
    if (tabBar){
      let tabBarHTMLRep = tabBar.HTMLRepresentation;
      let tabHTMLRep = tab.HTMLRepresentation;
      this.SetTabBarStyle(tabBarHTMLRep, true);
      this.SetTabStyle(tabHTMLRep, true);
    }
  }

  private OnHighlightFillDrop(tab: Tab){
    if (this.controller){
      tab.SetParentView(this.controller);
      this.controller.OnDrop(tab);
    }
    this.HideHighlightFrame();
  }

  private OnHighlightLeftDrop(tab: Tab){
    let parent = this.GetParentView();
    if (parent){
      let tabView = LayoutManager.GetTabViewForHTMLRepresentation(parent.HTMLRepresentation);
      if (tabView){
        let leftTabView = tabView.CreateLeftSideTabView();
        let controller = leftTabView.TabBar();
        tab.SetParentView(controller);
        controller.OnDrop(tab);
        this.HideHighlightFrame();
      }
    }
  }

  private OnHighlightRightDrop(tab: Tab){
    let parent = this.GetParentView();
    if (parent){
      let tabView = LayoutManager.GetTabViewForHTMLRepresentation(parent.HTMLRepresentation);
      if (tabView){
        let rightTabView = tabView.CreateRightSideTabView();
        let controller = rightTabView.TabBar();
        tab.SetParentView(controller);
        controller.OnDrop(tab);
        this.HideHighlightFrame();
      }
    }
  }

  private OnHighlightTopDrop(tab: Tab){
    let parent = this.GetParentView();
    if (parent){
      let tabView = LayoutManager.GetTabViewForHTMLRepresentation(parent.HTMLRepresentation);
      if (tabView){
        let topTabView = tabView.CreateUpperSideTabView();
        let controller = topTabView.TabBar();
        tab.SetParentView(controller);
        controller.OnDrop(tab);
        this.HideHighlightFrame();
      }
    }
  }

  private OnHighlightBottomDrop(tab: Tab){
    let parent = this.GetParentView();
    if (parent){
      let tabView = LayoutManager.GetTabViewForHTMLRepresentation(parent.HTMLRepresentation);
      if (tabView){
        let bottomTabView = tabView.CreateLowerSideTabView();
        let controller = bottomTabView.TabBar();
        tab.SetParentView(controller);
        controller.OnDrop(tab);
        this.HideHighlightFrame();
      }
    }
  }

  public SetController(controller: TabBar){
    this.controller = controller;
  }

  public OnDragEnter(tab: Tab){
    this.pointer = new Pointer();
    let TabHTMLRepresentation = tab.HTMLRepresentation;
    let tabBar = tab.GetParentView();
    if (tabBar){
      let tabBarHTMLRep = tabBar.HTMLRepresentation;
      this.SetTabBarStyle(tabBarHTMLRep, false);
      this.SetTabStyle(tab.HTMLRepresentation, false);
      let [relX, relY] = this.pointer.MouseRelativeToElement(tabBarHTMLRep);
      let relOffsetLeftX = relX - TabHTMLRepresentation.clientWidth / 3;
      let relOffsetLeftY = relY - TabHTMLRepresentation.clientHeight / 2;
      TabHTMLRepresentation.style.left = relOffsetLeftX + "px";
      TabHTMLRepresentation.style.top = relOffsetLeftY + "px";
      this.pointer.InitBuffer(relOffsetLeftX, relOffsetLeftY);
      this.pointer.resetVectorPivot(TabHTMLRepresentation.offsetLeft, TabHTMLRepresentation.offsetTop);
      tab.HTMLRepresentation.style.zIndex = "999";
    }
  }

  public OnDragging(tab: Tab){
    if (this.pointer){
      let [mX, mY] = this.pointer.Mouse();
      let [X, Y] = this.pointer.GetBuffer(mX, mY);
      let TabHTMLRepresentation = tab.HTMLRepresentation;
      TabHTMLRepresentation.style.left = X + "px";
      TabHTMLRepresentation.style.top = Y + "px";
      let [relX, relY] = this.pointer.MouseRelativeToElement(this.HTMLRepresentation);
      let ratioX = relX / this.HTMLRepresentation.clientWidth;
      let ratioY = relY / this.HTMLRepresentation.clientHeight;
      if (ratioX < 0.25)
        this.ShowHighLightFrame(HighLightFrameMode.HighLightLeft);
      else if (ratioX > 0.75)
        this.ShowHighLightFrame(HighLightFrameMode.HighLightRight);
      else if (ratioY < 0.25)
        this.ShowHighLightFrame(HighLightFrameMode.HighLightTop);
      else if (ratioY > 0.75)
        this.ShowHighLightFrame(HighLightFrameMode.HighLightBottom);
      else
        this.ShowHighLightFrame(HighLightFrameMode.HighLightFill);

    }
  }

  public OnDrop(tab: Tab){
    this.SetTabBarStyleForTab(tab);
    switch (this.lastHighlightModeState){
      case HighLightFrameMode.HighLightFill:{
        this.OnHighlightFillDrop(tab);
        break;
      }
      case HighLightFrameMode.HighLightLeft:{
        this.OnHighlightLeftDrop(tab);
        break;
      }
      case HighLightFrameMode.HighLightRight:{
        this.OnHighlightRightDrop(tab);
        break;
      }
      case HighLightFrameMode.HighLightTop:{
        this.OnHighlightTopDrop(tab);
        break;
      }
      case HighLightFrameMode.HighLightBottom:{
        this.OnHighlightBottomDrop(tab);
        break;
      }
      default:{
        this.OnHighlightFillDrop(tab);
      }
    }
  }

  public OnObservableViewChange(view: View){ //Second -> Call for current Parent

  }

  public OnObservableViewDidChange(view: View){ //Third -> Call for new Parent

  }

  public OnChildViewAboutToRelocate(view: View){  //First Call -> for current Parent
  }

  public OnChildViewRelocate(view: View){
    if (this.views.size < 1){
      let tabView = this.GetParentView();
      if (tabView){
        tabView.Release();
      }
    }
  }

  public OnChildViewDidRelocate(view: View){ //Forth -> Final Call for new Parent

  }

  public OnDragLeave(tab: Tab){
    this.HideHighlightFrame();
    delete this.pointer;
  }


  public Show(){
  }

  public Hide(){
  }

  public Update(){
    console.log("Area Update...");
  }

}
