"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var layout_1 = require("../layout");
var process_view_1 = require("../process-view");
var viewport_1 = require("../../3d-core/viewport");
var layout_manager_1 = require("../layout-manager");
var electron_1 = require("electron");
electron_1.ipcRenderer.send('window-process-message', 'Window Ready');
electron_1.ipcRenderer.on('main-process-message', function (event, arg) {
    console.log("Message from the Main process: " + arg);
    //InitLayout();
    LayoutExample();
});
electron_1.ipcRenderer.on('log-message', function (event, arg) {
    console.log("[Sender: " + arg.type);
    console.log("Message: " + arg.message + "]");
});
function InitLayout() {
    var container = document.getElementById("layout");
    var WindowLayout = new layout_1.Layout(container);
    var context = WindowLayout.Init();
    var tabView = layout_manager_1.LayoutManager.CreateTabView(context);
    tabView.CreateProcessView(viewport_1.Viewport, "Viewport");
}
function LayoutExample() {
    var container = document.getElementById("layout");
    var WindowLayout = new layout_1.Layout(container);
    var leftContext = WindowLayout.Init();
    var rightContext = WindowLayout.SplitVertically(leftContext);
    var topContext = rightContext;
    var bottomContext = WindowLayout.SplitHorizontally(topContext);
    //Left Panel
    var MyLeftTabPanel = layout_manager_1.LayoutManager.CreateTabView(leftContext);
    MyLeftTabPanel.CreateProcessView(viewport_1.Viewport, "Perspective View");
    MyLeftTabPanel.CreateProcessView(viewport_1.Viewport, "Plan View");
    MyLeftTabPanel.CreateProcessView(viewport_1.Viewport, "Section View");
    //Right Top Panel
    var MyRightTabPanel = layout_manager_1.LayoutManager.CreateTabView(topContext);
    MyRightTabPanel.CreateProcessView(viewport_1.Viewport, "Parallel View");
    MyRightTabPanel.CreateProcessView(viewport_1.Viewport, "Camera View");
    MyRightTabPanel.CreateProcessView(viewport_1.Viewport, "Elevation View");
    // Right Bottom Panel
    var MyBottomTabPanel = layout_manager_1.LayoutManager.CreateTabView(bottomContext);
    MyBottomTabPanel.CreateProcessView(process_view_1.ProcessView, "Untitled");
}
