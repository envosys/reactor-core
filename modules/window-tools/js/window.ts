import {Layout} from "../layout";
import {TabBar} from "../tab-bar";
import {View} from "../view";
import {Area} from "../area";
import {ProcessView} from "../process-view";
import {Viewport} from "../../3d-core/viewport";
import {LayoutManager} from "../layout-manager";
import {ipcRenderer} from "electron";
import {IpcConsole, ipcConsoleMessage} from "../ipc-console";

ipcRenderer.send('window-process-message', 'Window Ready');
ipcRenderer.on('main-process-message', (event: any, arg: object) =>{
  console.log("Message from the Main process: " + arg);
  //InitLayout();
  LayoutExample();
});

ipcRenderer.on('log-message', (event: any, arg: ipcConsoleMessage) =>{
  console.log("[Sender: " + arg.type);
  console.log("Message: " + arg.message + "]");
});

function InitLayout(){
  let container = document.getElementById("layout");
  let WindowLayout = new Layout(<HTMLElement> container);
  let context = WindowLayout.Init();
  let tabView = LayoutManager.CreateTabView(context);
  tabView.CreateProcessView(Viewport, "Viewport");
}

function LayoutExample(){
  let container = document.getElementById("layout");
  let WindowLayout = new Layout(<HTMLElement> container);

  let leftContext = WindowLayout.Init();
  let rightContext = WindowLayout.SplitVertically(leftContext);
  let topContext = rightContext;
  let bottomContext = WindowLayout.SplitHorizontally(topContext);

  //Left Panel
  let MyLeftTabPanel = LayoutManager.CreateTabView(<HTMLElement> leftContext);
  MyLeftTabPanel.CreateProcessView(Viewport, "Perspective View");
  MyLeftTabPanel.CreateProcessView(Viewport, "Plan View");
  MyLeftTabPanel.CreateProcessView(Viewport, "Section View");

  //Right Top Panel
  let MyRightTabPanel = LayoutManager.CreateTabView(<HTMLElement> topContext);
  MyRightTabPanel.CreateProcessView(Viewport, "Parallel View");
  MyRightTabPanel.CreateProcessView(Viewport, "Camera View");
  MyRightTabPanel.CreateProcessView(Viewport, "Elevation View");

  // Right Bottom Panel
  let MyBottomTabPanel = LayoutManager.CreateTabView(<HTMLElement> bottomContext);
  MyBottomTabPanel.CreateProcessView(ProcessView, "Untitled");
}
