"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var view_1 = require("./view");
var dom_object_1 = require("./dom-object");
var Pane = /** @class */ (function (_super) {
    __extends(Pane, _super);
    function Pane(parentView, anchorViewPosition, options) {
        var _this = _super.call(this, parentView, dom_object_1.HTMLRepresentation.CreateHTMLElement("pane", parentView.HTMLRepresentation)) || this;
        if (anchorViewPosition === undefined) {
            var HTMLRep = parentView.HTMLRepresentation;
            _this.SetPosition(HTMLRep.offsetLeft, HTMLRep.offsetTop + HTMLRep.clientHeight);
        }
        else if (anchorViewPosition instanceof view_1.View) {
            var HTMLRep = anchorViewPosition.HTMLRepresentation;
            _this.SetPosition(HTMLRep.offsetLeft, HTMLRep.offsetTop + HTMLRep.clientHeight);
        }
        else {
            var X = anchorViewPosition[0], Y = anchorViewPosition[1];
            _this.SetPosition(X, Y);
        }
        return _this;
    }
    Pane.prototype.SetPosition = function (left, top) {
        var parent = this.GetParentView();
        var offsetLeft = left;
        var offsetTop = top;
        if (parent) {
            var HTMLRep = parent.HTMLRepresentation;
            console.log(HTMLRep);
            if (HTMLRep.clientWidth <= left + this.HTMLRepresentation.clientWidth) {
                offsetLeft = HTMLRep.clientWidth - this.HTMLRepresentation.clientWidth;
            }
            if (HTMLRep.clientHeight <= top + this.HTMLRepresentation.clientHeight) {
                offsetTop = HTMLRep.clientHeight - this.HTMLRepresentation.clientHeight;
            }
        }
        this.HTMLRepresentation.style.left = offsetLeft + "px";
        this.HTMLRepresentation.style.top = offsetTop + "px";
    };
    Pane.prototype.Show = function () {
        this.HTMLRepresentation.style.display = 'block';
    };
    Pane.prototype.Hide = function () {
        this.HTMLRepresentation.style.display = 'none';
    };
    Pane.prototype.Update = function () {
    };
    return Pane;
}(view_1.View));
exports.Pane = Pane;
