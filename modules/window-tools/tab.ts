const TAB_CLASS_ACTIVE = "tab active";
const TAB_CLASS = "tab";
const TAB_CLASS_BUTTON = "tab button";
const TAB_SAFE_WIDTH = 4;
const TAB_BUTTON_WIDTH = 24;

import {View} from "./view";
import {Layout} from "./layout";
import {HTMLRepresentation} from "./dom-object";
import {TabBar} from "./tab-bar";


export class Tab extends View{
  private title: string;
  private active: Boolean;
  private view: View | undefined;
  public constructor(parentView: View, targetView?: View | null, title?: string){
    super(parentView, HTMLRepresentation.CreateHTMLElement({
        className: "tab",
        attributes: {
          draggable: "true"
        }
      }, parentView.HTMLRepresentation),
    );
    this.title = title ? title : 'Untitled';
    HTMLRepresentation.innerText(this.HTMLRepresentation, this.title);
    this.active = false;
    if (targetView) this.view = targetView;
    this.HTMLRepresentation.style.width = this.HTMLRepresentation.clientWidth + TAB_SAFE_WIDTH + "px";
    this.Update();
  }


  public ShowTargetView(){
    if (this.view)
      this.view.Show();
  }

  public HideTargetView(){
    if (this.view)
      this.view.Hide();
  }

  public Title(title: string){
    this.title = title;
    this.Update();
  }

  public GetTitle(){
    return this.title;
  }

  public TargetView(){
    return <View> this.view;
  }

  public Show(){

  }

  public Hide(){

  }

  public Activate(){
    let tabBar : TabBar = <TabBar> this.GetParentView();
    tabBar.OnTabActivated(this);
    this.active = true;
    this.Update();
  };

  public OnParentViewChange(parentView: View){
    //this.delayTargetActivation = true;
  }

  public Deactivate(){
    this.active = false;
    this.Update();
  }

  public TabTypeButton(){
    this.HTMLRepresentation.style.width = TAB_BUTTON_WIDTH + "px";
    this.HTMLRepresentation.className = TAB_CLASS_BUTTON;
  }

  public Update(){
    HTMLRepresentation.innerText(this.HTMLRepresentation, this.title);
    this.HTMLRepresentation.className = this.active?TAB_CLASS_ACTIVE:TAB_CLASS;
    this.HTMLRepresentation.style.zIndex = this.active?"999":"0";
  }


}
