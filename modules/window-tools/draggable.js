"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var DRAG_AND_DROP_LAYER_ID_ELEMENT = "drag-and-drop-layer";
var dom_object_1 = require("./dom-object");
var Draggable = /** @class */ (function (_super) {
    __extends(Draggable, _super);
    function Draggable(HTMLRepresentation) {
        return _super.call(this, HTMLRepresentation) || this;
    }
    Draggable.prototype.OnDragStart = function (object) { };
    ;
    Draggable.prototype.OnDragging = function (object) { };
    ;
    Draggable.prototype.OnDragStop = function (object) { };
    ;
    Draggable.prototype.OnDragEnter = function (object, listener) { };
    ;
    Draggable.prototype.OnDragLeave = function (object, listener) { };
    ;
    Draggable.prototype.OnDrop = function (object, listener) { };
    ;
    return Draggable;
}(dom_object_1.HTMLRepresentation.DomObject));
exports.Draggable = Draggable;
