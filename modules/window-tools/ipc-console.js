"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var electron_1 = require("electron");
var IpcConsole = /** @class */ (function () {
    function IpcConsole(type) {
        this.type = type;
    }
    IpcConsole.prototype.log = function (msg) {
        var arg = {
            type: this.type,
            message: msg
        };
        electron_1.ipcRenderer.send('log-message', arg);
    };
    return IpcConsole;
}());
exports.IpcConsole = IpcConsole;
