import {View, ViewEventResponces, ViewEventExceptions} from "./view";
import {HTMLRepresentation} from "./dom-object";
import {LayoutManager} from "./layout-manager"
import {Layout} from "./layout"
import {Pointer} from "./pointer";

export class Pane extends View{
  public constructor(parentView: View, anchorViewPosition?: View | [number, number], options?: {}){
    super(parentView, HTMLRepresentation.CreateHTMLElement("pane", parentView.HTMLRepresentation));
    if (anchorViewPosition === undefined){
      let HTMLRep = parentView.HTMLRepresentation;
      this.SetPosition(HTMLRep.offsetLeft, HTMLRep.offsetTop + HTMLRep.clientHeight);
    }
    else if (anchorViewPosition instanceof View){
      let HTMLRep = anchorViewPosition.HTMLRepresentation;
      this.SetPosition(HTMLRep.offsetLeft, HTMLRep.offsetTop + HTMLRep.clientHeight);
    }
    else{
      let [X, Y] = anchorViewPosition;
      this.SetPosition(X, Y);
    }
  }

  public SetPosition(left: number, top: number){
    let parent = this.GetParentView();
    let offsetLeft: number = left;
    let offsetTop: number = top;
    if (parent){
      let HTMLRep = parent.HTMLRepresentation;
      console.log(HTMLRep);
      if (HTMLRep.clientWidth <= left + this.HTMLRepresentation.clientWidth){
        offsetLeft = HTMLRep.clientWidth - this.HTMLRepresentation.clientWidth;
      }
      if (HTMLRep.clientHeight <= top + this.HTMLRepresentation.clientHeight){
        offsetTop = HTMLRep.clientHeight - this.HTMLRepresentation.clientHeight;
      }
    }
    this.HTMLRepresentation.style.left = offsetLeft + "px";
    this.HTMLRepresentation.style.top = offsetTop + "px";
  }

  public Show(){
    this.HTMLRepresentation.style.display = 'block';
  }

  public Hide(){
    this.HTMLRepresentation.style.display = 'none';
  }

  public Update(){

  }
}
