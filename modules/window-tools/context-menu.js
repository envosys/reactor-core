"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var pane_1 = require("./pane");
var layout_manager_1 = require("./layout-manager");
var pointer_1 = require("./pointer");
var ContextMenuItemTypes;
(function (ContextMenuItemTypes) {
    ContextMenuItemTypes[ContextMenuItemTypes["Action"] = 0] = "Action";
    ContextMenuItemTypes[ContextMenuItemTypes["Submenu"] = 1] = "Submenu";
    ContextMenuItemTypes[ContextMenuItemTypes["Delimiter"] = 2] = "Delimiter";
})(ContextMenuItemTypes || (ContextMenuItemTypes = {}));
var ContextMenuItemAction = /** @class */ (function () {
    function ContextMenuItemAction(func, args) {
        this.actionFunc = func;
        this.args = args;
    }
    ContextMenuItemAction.prototype.Call = function () {
        this.actionFunc(this.args);
    };
    return ContextMenuItemAction;
}());
var MenuItem = /** @class */ (function () {
    function MenuItem(type, action, options) {
        this.type = type;
        this.options = options;
        switch (type) {
            case ContextMenuItemTypes.Action: {
                this.action = action;
                break;
            }
            case ContextMenuItemTypes.Submenu: {
                break;
            }
            case ContextMenuItemTypes.Delimiter: {
                break;
            }
        }
    }
    MenuItem.prototype.Execute = function () {
        if (this.type === ContextMenuItemTypes.Action && this.action) {
            var action = this.action;
            action.Call();
        }
    };
    MenuItem.prototype.isAction = function () {
        return this.type === ContextMenuItemTypes.Action ? true : false;
    };
    MenuItem.prototype.isDelimiter = function () {
        return this.type === ContextMenuItemTypes.Delimiter ? true : false;
    };
    MenuItem.prototype.isSubmenu = function () {
        return this.type === ContextMenuItemTypes.Submenu ? true : false;
    };
    MenuItem.prototype.Type = function () {
        return this.type;
    };
    MenuItem.prototype.Options = function () {
        return this.options;
    };
    return MenuItem;
}());
exports.MenuItem = MenuItem;
var ContextMenu = /** @class */ (function (_super) {
    __extends(ContextMenu, _super);
    function ContextMenu() {
        var _this = this;
        var pointer = new pointer_1.Pointer();
        _this = _super.call(this, layout_manager_1.LayoutManager.GetLayoutReference(), [pointer.Mouse()[0], pointer.Mouse()[1]]) || this;
        _this.menuItems = new Map();
        _this.Hide();
        return _this;
    }
    ContextMenu.prototype.AddMenuItem = function (actionFunc, options) {
        var actionOptions = { name: '', label: '', title: '' };
        for (var _i = 0, _a = Object.entries(options); _i < _a.length; _i++) {
            var _b = _a[_i], key = _b[0], value = _b[1];
            switch (key) {
                case "name": {
                    actionOptions.name = value;
                    break;
                }
                case "label": {
                    actionOptions.label = value;
                    break;
                }
                case "title": {
                    actionOptions.title = value;
                    break;
                }
            }
        }
        var Action = new ContextMenuItemAction(actionFunc, null);
        var menuItem = new MenuItem(ContextMenuItemTypes.Action, Action, actionOptions);
        this.menuItems.set(actionOptions.name, menuItem);
        this.Update();
    };
    ContextMenu.prototype.AddDelimiter = function () {
        var menuItem = new MenuItem(ContextMenuItemTypes.Delimiter);
        this.menuItems.set(name, menuItem);
    };
    ContextMenu.prototype.AddSubMenu = function (contextMenu) {
        var menuItem = new MenuItem(ContextMenuItemTypes.Submenu, contextMenu);
        this.menuItems.set(name, menuItem);
    };
    ContextMenu.prototype.Update = function () {
        var HTMLRep = this.HTMLRepresentation;
        this.menuItems.forEach(function (item, name, map) {
            console.log('Name: ' + name);
            var opts = item.Options();
            if (opts != undefined) {
                console.log('Label: ' + opts.label);
                console.log('Title: ' + opts.title);
            }
        });
    };
    return ContextMenu;
}(pane_1.Pane));
exports.ContextMenu = ContextMenu;
