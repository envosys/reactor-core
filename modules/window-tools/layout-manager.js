"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var DRAG_EVENT_LAYER = "drag-event-layer";
var view_1 = require("./view");
var tab_bar_1 = require("./tab-bar");
var area_1 = require("./area");
var pane_1 = require("./pane");
var context_menu_1 = require("./context-menu");
var LayoutManager;
(function (LayoutManager) {
    var LayoutEventStates;
    (function (LayoutEventStates) {
        LayoutEventStates[LayoutEventStates["Waiting"] = 0] = "Waiting";
        LayoutEventStates[LayoutEventStates["Ready"] = 1] = "Ready";
        LayoutEventStates[LayoutEventStates["Drag"] = 2] = "Drag";
    })(LayoutEventStates || (LayoutEventStates = {}));
    var LayoutReference;
    var EventLayerReference = document.getElementById(DRAG_EVENT_LAYER);
    var SharedEventListeners = new Set;
    var SharedObservableViewObject;
    var SharedActiveEventListener;
    var SharedEventState = LayoutEventStates.Waiting;
    var ClientXY = { x: 0, y: 0 };
    //This is an abstraction of Layout Context Array - the glue
    var TabViewCollection = new Set;
    EventLayerReference.addEventListener("mousemove", function (e) {
        e.preventDefault();
        ClientXY.x = e.clientX;
        ClientXY.y = e.clientY;
        ProcessEvents();
    });
    EventLayerReference.addEventListener("mouseup", function (e) {
        e.preventDefault();
        CompleteEvents();
        DeactivateEventLayer();
    });
    function GetMouse() {
        return ClientXY;
    }
    LayoutManager.GetMouse = GetMouse;
    function ProcessEvents() {
        //Check and Set an ActiveEventListener
        var newListener;
        if (SharedActiveEventListener !== LayoutReference) {
            if (newListener = ActiveEventListenerShouldChange()) {
                if (SharedActiveEventListener) {
                    var replacedListener = SharedActiveEventListener;
                    SharedActiveEventListener = newListener;
                    replacedListener.OnDragLeave(SharedObservableViewObject);
                    SharedActiveEventListener.OnDragEnter(SharedObservableViewObject);
                }
                else {
                    SharedActiveEventListener = newListener;
                }
            }
        }
        if (SharedEventState == LayoutEventStates.Ready) {
            SharedActiveEventListener === null || SharedActiveEventListener === void 0 ? void 0 : SharedActiveEventListener.OnDragStart(SharedObservableViewObject);
            SharedEventState = LayoutEventStates.Drag;
        }
        else if (SharedEventState == LayoutEventStates.Drag) {
            SharedActiveEventListener === null || SharedActiveEventListener === void 0 ? void 0 : SharedActiveEventListener.OnDragging(SharedObservableViewObject);
        }
    }
    ;
    function CompleteEvents() {
        if (SharedEventState == LayoutEventStates.Drag) {
            if (SharedActiveEventListener) {
                SharedActiveEventListener.OnDragStop(SharedObservableViewObject);
                SharedActiveEventListener.OnDrop(SharedObservableViewObject);
            }
        }
        else {
            //TO-DO OnClick SharedActiveEventListener?.OnClick(SharedObservableViewObject);
        }
        SharedActiveEventListener = undefined;
        SharedObservableViewObject = undefined;
        SharedEventState = LayoutEventStates.Waiting;
    }
    ;
    function ActiveEventListenerShouldChange() {
        for (var _i = 0, _a = Array.from(SharedEventListeners); _i < _a.length; _i++) {
            var listener = _a[_i];
            var boundingBox = listener.HTMLRepresentation.getBoundingClientRect();
            if (listener !== SharedActiveEventListener) {
                if (ClientXY.x > boundingBox.left && ClientXY.x < boundingBox.right &&
                    ClientXY.y > boundingBox.top && ClientXY.y < boundingBox.bottom) {
                    return listener;
                }
            }
        }
    }
    function ActivateEventLayer(element) {
        EventLayerReference.style.cursor = element.style.cursor;
        EventLayerReference.style.display = "block";
    }
    function DeactivateEventLayer() {
        EventLayerReference.style.cursor = "initial";
        EventLayerReference.style.display = "none";
    }
    function SetupLayoutReference(layoutRef) {
        if (!LayoutReference) {
            LayoutReference = layoutRef;
        }
    }
    LayoutManager.SetupLayoutReference = SetupLayoutReference;
    function GetLayoutReference() {
        return LayoutReference;
    }
    LayoutManager.GetLayoutReference = GetLayoutReference;
    function WrapHTML(HTMLRepresentation) {
        return new HTMLWrapper(HTMLRepresentation);
    }
    LayoutManager.WrapHTML = WrapHTML;
    function RegisterEventListener(listener) {
        SharedEventListeners.add(listener);
    }
    LayoutManager.RegisterEventListener = RegisterEventListener;
    function UnregisterEventListener(listener) {
        SharedEventListeners.delete(listener);
    }
    LayoutManager.UnregisterEventListener = UnregisterEventListener;
    function ObserveViewObject(observable) {
        observable.HTMLRepresentation.addEventListener("mousedown", function (e) {
            e.preventDefault();
            SharedObservableViewObject = observable;
            ActivateEventLayer(observable.HTMLRepresentation);
            SharedEventState = LayoutEventStates.Ready;
            if (!observable.GetParentView()) { //If there is no parent view, Layout become an active event listener
                SharedActiveEventListener = LayoutReference;
            }
        });
    }
    LayoutManager.ObserveViewObject = ObserveViewObject;
    function CreateTabView(context) {
        return new TabView(context);
    }
    LayoutManager.CreateTabView = CreateTabView;
    function GetTabViewForHTMLRepresentation(element) {
        var tabViewCollectionArray = Array.from(TabViewCollection);
        for (var idx = 0; idx < tabViewCollectionArray.length; idx++) {
            var tabView = tabViewCollectionArray[idx];
            var tabViewHTMLRep = tabView.HTMLRepresentation;
            if (tabViewHTMLRep === element)
                return tabView;
        }
        return undefined;
    }
    LayoutManager.GetTabViewForHTMLRepresentation = GetTabViewForHTMLRepresentation;
    function CreateDropDownMenu(controller, anchorView) {
        var dropDownMenu = new pane_1.Pane(controller.GetParentView(), anchorView);
        return dropDownMenu;
    }
    LayoutManager.CreateDropDownMenu = CreateDropDownMenu;
    function CreateContextMenu() {
        return new context_menu_1.ContextMenu();
    }
    LayoutManager.CreateContextMenu = CreateContextMenu;
    // TabView Class is higher Abstraction of Context HTMLElement from layout.ts Structure
    var TabView = /** @class */ (function (_super) {
        __extends(TabView, _super);
        function TabView(HTMLRepresentation) {
            var _this = _super.call(this, null, HTMLRepresentation) || this;
            //this.HTMLRepresentation.style.minWidth = "64px";
            _this.tabBar = new tab_bar_1.TabBar(_this);
            _this.contentArea = new area_1.Area(_this);
            _this.tabBar.SetContentArea(_this.contentArea);
            TabViewCollection.add(_this);
            return _this;
        }
        TabView.prototype.TabBar = function () {
            return this.tabBar;
        };
        TabView.prototype.Release = function () {
            var layout = LayoutManager.GetLayoutReference();
            layout.Join(this.HTMLRepresentation);
            _super.prototype.Release.call(this);
        };
        TabView.prototype.ContentArea = function () {
            return this.contentArea;
        };
        TabView.prototype.CreateProcessView = function (processViewClass, title) {
            if (title === void 0) { title = "Untitled"; }
            var view = new processViewClass(this.contentArea);
            this.tabBar.Tab(view, title);
        };
        TabView.prototype.CreateLeftSideTabView = function () {
            var leftContext = this.HTMLRepresentation;
            var layout = LayoutManager.GetLayoutReference();
            var rightContext = layout.SplitVertically(leftContext);
            var rightSection;
            if (rightContext) {
                rightSection = layout.GetSectionFromContext(rightContext);
            }
            var leftSection = layout.GetSectionFromContext(leftContext);
            if (leftSection && rightSection) {
                layout.SwapContextInSections(leftSection, rightSection);
            }
            leftContext = rightContext;
            var leftTabView = LayoutManager.CreateTabView(leftContext);
            return leftTabView;
        };
        TabView.prototype.CreateRightSideTabView = function () {
            var leftContext = this.HTMLRepresentation;
            var layout = LayoutManager.GetLayoutReference();
            var rightContext = layout.SplitVertically(leftContext);
            var rightTabView = LayoutManager.CreateTabView(rightContext);
            return rightTabView;
        };
        TabView.prototype.CreateUpperSideTabView = function () {
            var upperContext = this.HTMLRepresentation;
            var layout = LayoutManager.GetLayoutReference();
            var lowerContext = layout.SplitHorizontally(upperContext);
            var upperSection = layout.GetSectionFromContext(upperContext); // Must be after layout.SplitHorizontally(upperContext);
            var lowerSection;
            if (lowerContext) {
                lowerSection = layout.GetSectionFromContext(lowerContext);
            }
            if (lowerSection && upperSection) {
                layout.SwapContextInSections(lowerSection, upperSection);
            }
            upperContext = lowerContext;
            var upperTabView = LayoutManager.CreateTabView(upperContext);
            return upperTabView;
        };
        TabView.prototype.CreateLowerSideTabView = function () {
            var upperContext = this.HTMLRepresentation;
            var layout = LayoutManager.GetLayoutReference();
            var lowerContext = layout.SplitHorizontally(upperContext);
            var lowerTabView = LayoutManager.CreateTabView(lowerContext);
            return lowerTabView;
        };
        TabView.prototype.Show = function () { };
        TabView.prototype.Hide = function () { };
        TabView.prototype.Update = function () { };
        return TabView;
    }(view_1.View));
    // HTMLWrapper Class is higher Abstraction of Context HTMLElement from layout.ts Structure
    var HTMLWrapper = /** @class */ (function (_super) {
        __extends(HTMLWrapper, _super);
        function HTMLWrapper(HTMLRepresentation) {
            return _super.call(this, null, HTMLRepresentation) || this;
        }
        HTMLWrapper.prototype.Show = function () { };
        HTMLWrapper.prototype.Hide = function () { };
        HTMLWrapper.prototype.Update = function () { };
        return HTMLWrapper;
    }(view_1.View));
})(LayoutManager = exports.LayoutManager || (exports.LayoutManager = {}));
