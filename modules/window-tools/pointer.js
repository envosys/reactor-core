"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mouseX, mouseY;
document.addEventListener("mousemove", function (e) {
    mouseX = e.clientX;
    mouseY = e.clientY;
});
function MouseCoordinates() {
    return [mouseX, mouseY];
}
var Pointer = /** @class */ (function () {
    function Pointer(x, y) {
        if (x === void 0) { x = 0; }
        if (y === void 0) { y = 0; }
        this.X = 0;
        this.Y = 0;
        this.initX = 0;
        this.initY = 0;
        this.diffX = 0;
        this.diffY = 0;
        this.bufferX = 0;
        this.bufferY = 0;
        this.vPivotX = 0;
        this.vPivotY = 0;
        this.X = x;
        this.Y = y;
        this.vectorRegister = new Map;
    }
    Pointer.prototype.InitBuffer = function (x, y) {
        this.bufferX = this.initX = this.vPivotX = x;
        this.bufferY = this.initY = this.vPivotY = y;
    };
    Pointer.prototype.GetPosition = function (x, y) {
        this.Update(x, y);
        return [this.X, this.Y];
    };
    Pointer.prototype.Mouse = function () {
        return MouseCoordinates();
    };
    Pointer.prototype.ConstantVectorInRegister = function (key, value, reset) {
        if (reset === void 0) { reset = false; }
        if (!this.vectorRegister.has(key) || reset) {
            this.vectorRegister.set(key, value);
        }
        return this.vectorRegister.get(key);
    };
    Pointer.prototype.MouseRelativeToElement = function (element) {
        var boundingBox = element.getBoundingClientRect();
        var coords = this.Mouse();
        return [coords[0] - boundingBox.left, coords[1] - boundingBox.top];
    };
    Pointer.prototype.GetDiff = function () {
        return [this.diffX, this.diffY];
    };
    Pointer.prototype.GetBuffer = function (x, y) {
        this.Update(x, y);
        return [this.bufferX, this.bufferY];
    };
    Pointer.prototype.GetInitPosition = function () {
        return [this.initX, this.initY];
    };
    Pointer.prototype.GetVector = function () {
        return [this.bufferX - this.vPivotX, this.bufferY - this.vPivotY];
    };
    Pointer.prototype.GetVectorPivot = function () {
        return [this.vPivotX, this.vPivotY];
    };
    Pointer.prototype.resetVectorPivot = function (x, y) {
        this.vPivotX = x;
        this.vPivotY = y;
    };
    Pointer.prototype.Update = function (x, y) {
        this.diffX = x - (this.X != 0 ? this.X : x);
        this.diffY = y - (this.Y != 0 ? this.Y : y);
        this.bufferX += this.diffX;
        this.bufferY += this.diffY;
        this.X = x;
        this.Y = y;
    };
    return Pointer;
}());
exports.Pointer = Pointer;
function oddRound(n) {
    return 2 * Math.floor(n / 2) + 1;
    //return Math.round(n);
    //return n;
}
exports.oddRound = oddRound;
