"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var three_1 = require("three");
var three_2 = require("three");
var three_3 = require("three");
var three_4 = require("three");
var three_5 = require("three");
var three_6 = require("three");
var ipc_console_1 = require("../window-tools/ipc-console");
var fs_1 = __importDefault(require("fs"));
var path_1 = __importDefault(require("path"));
var console = new ipc_console_1.IpcConsole(__filename);
var pvshPath = path_1.default.join(__dirname, 'shaders', 'point.vert');
var pfshPath = path_1.default.join(__dirname, 'shaders', 'point.frag');
var pointVertexShader = fs_1.default.readFileSync(pvshPath, 'utf8');
var pointFragmentShader = fs_1.default.readFileSync(pfshPath, 'utf8');
var Curve = /** @class */ (function (_super) {
    __extends(Curve, _super);
    function Curve() {
        var _this = _super.call(this) || this;
        _this.curvePoints = new Array;
        _this.type = "curve";
        return _this;
    }
    Curve.prototype.AddPoint = function (position) {
        var color = new three_6.Color(0xffffff);
        var scales = new Float32Array(1);
        scales[0] = 1;
        var geometry = new three_2.BufferGeometry();
        geometry.setAttribute('position', new three_4.Float32BufferAttribute(position.toArray(), 3));
        geometry.setAttribute('scale', new three_4.Float32BufferAttribute(scales, 1));
        var material = new three_3.ShaderMaterial({
            uniforms: {
                color: { value: color }
            },
            vertexShader: pointVertexShader,
            fragmentShader: pointFragmentShader
        });
        material.depthTest = false;
        var point = new three_5.Points(geometry, material);
        this.add(point);
        return this.curvePoints.push(point);
    };
    Curve.prototype.PointAtIndex = function (index) {
        if (index >= 0 || index < this.curvePoints.length) {
            return this.curvePoints[index];
        }
        return null;
    };
    Curve.prototype.PointsArray = function () {
        return this.curvePoints;
    };
    Curve.prototype.hasPoint = function (point) {
        for (var idx = 0; idx < this.curvePoints.length; idx++) {
            if (this.curvePoints[idx] === point) {
                return true;
            }
        }
        return false;
    };
    Curve.prototype.Calculate = function () {
        var tt = [0.0, 0.5, 1.0];
        var steps = 12;
        for (var t = 0; t < 1; t += 1 / steps) {
            var q1 = new Array;
            for (var i = 1; i < this.curvePoints.length; i++) {
                var C0 = this.curvePoints[i - 1];
                var lagr = (tt[i] - t) / (tt[i] - tt[0]);
                q1[i - 1] = C0.position.multiplyScalar(lagr);
            }
            console.log(q1.length);
        }
    };
    Curve.prototype.Update = function () {
    };
    return Curve;
}(three_1.Object3D));
exports.Curve = Curve;
