"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ThreeJS = __importStar(require("three"));
var Grid = /** @class */ (function () {
    function Grid(spacing, linesNumber, majorLinesEveryNth, adaptive) {
        if (spacing === void 0) { spacing = 1.0; }
        if (linesNumber === void 0) { linesNumber = 100; }
        if (majorLinesEveryNth === void 0) { majorLinesEveryNth = 10; }
        if (adaptive === void 0) { adaptive = true; }
        this.spacing = spacing;
        this.linesNumber = linesNumber;
        this.majorLinesEveryNth = majorLinesEveryNth;
        this.adaptive = adaptive;
        this.borderColor = new ThreeJS.Color(0x111111);
        this.gridColor = new ThreeJS.Color(0x21252b);
        this.majorLinesColor = new ThreeJS.Color(0x181a1f);
        var halfSize = linesNumber * spacing / 2;
        var vertices = new Array();
        var colors = new Array();
        var color;
        for (var idx = 0, offset = -halfSize, idc = 0; idx <= linesNumber; idx++, offset += spacing) {
            vertices.push(-halfSize, 0.0, offset, halfSize, 0.0, offset);
            vertices.push(offset, 0.0, -halfSize, offset, 0.0, halfSize);
            color = this.gridColor;
            if (idx % majorLinesEveryNth === 0)
                color = this.majorLinesColor;
            if (idx === 0 || idx === linesNumber)
                color = this.borderColor;
            color.toArray(colors, idc);
            idc += 3;
            color.toArray(colors, idc);
            idc += 3;
            color.toArray(colors, idc);
            idc += 3;
            color.toArray(colors, idc);
            idc += 3;
        }
        var geometry = new ThreeJS.BufferGeometry();
        geometry.setAttribute('position', new ThreeJS.Float32BufferAttribute(vertices, 3));
        geometry.setAttribute('color', new ThreeJS.Float32BufferAttribute(colors, 3));
        this.vertexCol = geometry.attributes.color;
        this.vertexCol.usage = ThreeJS.DynamicDrawUsage;
        var material = new ThreeJS.LineBasicMaterial({
            vertexColors: ThreeJS.VertexColors,
            fog: false
        });
        var grid = new ThreeJS.LineSegments(geometry, material);
        this.gridObject = new ThreeJS.Object3D();
        this.gridObject = grid;
    }
    Grid.prototype.Object = function () {
        return this.gridObject;
    };
    Grid.prototype.Update = function (camera) {
        var relDistance = camera.position.distanceTo(this.gridObject.position);
        var color;
        var halfSize = this.linesNumber * this.spacing / 2;
        for (var idx = 0, offset = -halfSize, idc = 0; idx <= this.linesNumber; idx++, offset += this.spacing) {
            if (idx % this.majorLinesEveryNth != 0 || idx != 0 || idx != this.linesNumber) {
                color = this.gridColor;
                var vCol = this.vertexCol.array;
            }
        }
    };
    return Grid;
}());
exports.Grid = Grid;
