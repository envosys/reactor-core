"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ThreeJS = __importStar(require("three"));
var World = /** @class */ (function () {
    function World(context) {
        var _this = this;
        this.scene = new ThreeJS.Scene();
        //this.scene.background = new ThreeJS.Color(0x282c34);
        this.perspective = new ThreeJS.PerspectiveCamera(50, window.innerWidth / window.innerHeight, 1e-6, 1e27);
        this.orthographic = new ThreeJS.OrthographicCamera(-10.0, 10.0, 10.0, -10.0, 1e-6, 1e27);
        this.camera = this.perspective;
        this.renderer = new ThreeJS.WebGLRenderer({
            antialias: true,
            logarithmicDepthBuffer: true,
        });
        this.renderer.setSize(window.innerWidth, window.innerHeight);
        if (context)
            context.appendChild(this.renderer.domElement);
        else
            document.body.appendChild(this.renderer.domElement);
        this.scene.fog = new ThreeJS.FogExp2(0x282c34, 0.0028);
        this.renderer.setClearColor(this.scene.fog.color, 1);
        window.addEventListener("resize", function (e) {
            if (_this.camera) {
                _this.camera = _this.camera;
                _this.camera.aspect = window.innerWidth / window.innerHeight;
            }
            _this.camera.updateProjectionMatrix();
            _this.renderer.setSize(window.innerWidth, window.innerHeight);
        });
    }
    World.prototype.Scene = function () {
        return this.scene;
    };
    World.prototype.Renderer = function () {
        return this.renderer;
    };
    World.prototype.Camera = function () {
        return this.camera;
    };
    return World;
}());
exports.World = World;
