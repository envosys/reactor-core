import {Object3D} from "three"
import {BufferGeometry} from "three"
import {BufferAttribute} from "three"
import {ShaderMaterial} from "three"
import {Float32BufferAttribute} from "three"
import {Vector3} from "three"
import {Points} from "three"
import {Color} from "three"
import {PointsMaterial} from "three"
import {IpcConsole} from "../window-tools/ipc-console";

import fs from "fs";
import path from "path";

let console = new IpcConsole(__filename);

let pvshPath = path.join(__dirname, 'shaders', 'point.vert');
let pfshPath = path.join(__dirname, 'shaders', 'point.frag');
let pointVertexShader = fs.readFileSync(pvshPath,'utf8');
let pointFragmentShader = fs.readFileSync(pfshPath,'utf8');

export class Curve extends Object3D{
  private curvePoints: Array <Points> = new Array;
  public constructor(){
    super();
    this.type = "curve";
  }

  public AddPoint(position: Vector3): number{
    let color = new Color(0xffffff);
    let scales: Float32Array = new Float32Array(1);
    scales[0] = 1;
    let geometry = new BufferGeometry();
    geometry.setAttribute('position', new Float32BufferAttribute(position.toArray(), 3));
    geometry.setAttribute('scale', new Float32BufferAttribute(scales, 1));
    let material = new ShaderMaterial({
      uniforms: {
        color: {value: color}
      },
      vertexShader: pointVertexShader,
      fragmentShader: pointFragmentShader
    });
    material.depthTest = false;
    let point = new Points(geometry, material);
    this.add(point);
    return this.curvePoints.push(point);
  }

  public PointAtIndex(index: number): Points | null{
    if (index >= 0 || index < this.curvePoints.length){
      return this.curvePoints[index];
    }
    return null;
  }

  public PointsArray(){
    return this.curvePoints;
  }

  public hasPoint(point: Points): boolean{
    for (let idx = 0; idx < this.curvePoints.length; idx++){
      if (this.curvePoints[idx] === point){
        return true;
      }
    }
    return false;
  }

  public Calculate(){
    let tt = [0.0, 0.5, 1.0];
    let steps = 12;
    for (let t = 0; t < 1; t += 1 / steps){
      let q1: Array <Vector3> = new Array;
      for (let i = 1; i < this.curvePoints.length; i++){
        let C0 = this.curvePoints[i - 1];      
        let lagr = (tt[i] - t) / (tt[i] - tt[0]);
        q1[i - 1] = C0.position.multiplyScalar(lagr);
      }
      console.log(q1.length);
    }
  }

  public Update(){

  }

  //End of class Curve
}
