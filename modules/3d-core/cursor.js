"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ThreeJS = __importStar(require("three"));
var three_1 = require("three");
var three_2 = require("three");
var three_3 = require("three");
var three_4 = require("three");
var three_5 = require("three");
var three_6 = require("three");
var Cursor = /** @class */ (function () {
    function Cursor(camera) {
        this.camera = camera;
        this.position = new ThreeJS.Vector3(0.0, 0.0, 0.0);
        this.vertices = new Array();
        this.colors = new Array();
        var color;
        this.vertices.push(-10.0, 0.0, 0.0, 10.0, 0.0, 0.0);
        color = new three_6.Color(0x239B56);
        color.toArray(this.colors, 0);
        color.toArray(this.colors, 3);
        this.vertices.push(0.0, -10.0, 0.0, 0.0, 10.0, 0.0);
        color = new three_6.Color(0x2874A6);
        color.toArray(this.colors, 6);
        color.toArray(this.colors, 9);
        this.vertices.push(0.0, 0.0, -10.0, 0.0, 0.0, 10.0);
        color = new three_6.Color(0xB03A2E);
        color.toArray(this.colors, 12);
        color.toArray(this.colors, 15);
        this.geometry = new three_5.BufferGeometry();
        this.geometry.setAttribute('position', new three_4.Float32BufferAttribute(this.vertices, 3));
        this.geometry.setAttribute('color', new three_4.Float32BufferAttribute(this.colors, 3));
        this.vertexPos = this.geometry.attributes.position;
        this.vertexPos.usage = ThreeJS.DynamicDrawUsage;
        var material = new three_3.LineBasicMaterial({
            vertexColors: three_2.VertexColors,
            fog: false
        });
        this.lineSegments = new three_1.LineSegments(this.geometry, material);
        this.parentTransform = new ThreeJS.Object3D();
        this.parentTransform.add(this.lineSegments);
    }
    Cursor.prototype.Object = function () {
        return this.parentTransform;
    };
    Cursor.prototype.SetPosition = function (position) {
        this.position = position;
    };
    Cursor.prototype.Hide = function () {
        this.parentTransform.visible = false;
    };
    Cursor.prototype.Show = function () {
        this.parentTransform.visible = true;
    };
    Cursor.prototype.Update = function () {
        this.parentTransform.position.set(this.position.x, this.position.y, this.position.z);
        this.camera.position.distanceTo(this.position);
        var relSize = this.camera.position.distanceTo(this.position) * 0.05;
        var idk = 0;
        this.vertexPos.setX(idk, -relSize);
        idk += 2;
        this.vertexPos.setY(idk, -relSize);
        idk += 2;
        this.vertexPos.setZ(idk, -relSize);
        idk = 1;
        this.vertexPos.setX(idk, relSize);
        idk += 2;
        this.vertexPos.setY(idk, relSize);
        idk += 2;
        this.vertexPos.setZ(idk, relSize);
        idk = 1;
        this.vertexPos.needsUpdate = true;
    };
    return Cursor;
}());
exports.Cursor = Cursor;
