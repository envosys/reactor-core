import * as ThreeJS from "three"

export class Grid{
  private gridObject: ThreeJS.Object3D;
  private vertexCol: ThreeJS.BufferAttribute;
  private borderColor = new ThreeJS.Color(0x111111);
  private gridColor = new ThreeJS.Color(0x21252b);
  private majorLinesColor = new ThreeJS.Color(0x181a1f);
  constructor(
    readonly spacing: number = 1.0,
    readonly linesNumber: number = 100,
    readonly majorLinesEveryNth: number = 10,
    readonly adaptive: boolean = true
  ){
    let halfSize = linesNumber * spacing / 2;
    let vertices = new Array();
    let colors = new Array();
    let color: ThreeJS.Color;

    for(let idx = 0, offset = - halfSize, idc = 0; idx <= linesNumber; idx++, offset += spacing){
      vertices.push(-halfSize, 0.0, offset, halfSize, 0.0, offset);
      vertices.push(offset, 0.0, -halfSize, offset, 0.0, halfSize);
      color = this.gridColor;
      if (idx % majorLinesEveryNth === 0)
        color = this.majorLinesColor;
      if (idx === 0 || idx === linesNumber)
        color = this.borderColor;
      color.toArray(colors, idc); idc += 3;
      color.toArray(colors, idc); idc += 3;
      color.toArray(colors, idc); idc += 3;
      color.toArray(colors, idc); idc += 3;
    }
    let geometry = new ThreeJS.BufferGeometry();
    geometry.setAttribute('position', new ThreeJS.Float32BufferAttribute(vertices, 3));
    geometry.setAttribute('color', new ThreeJS.Float32BufferAttribute(colors, 3));

    this.vertexCol = <ThreeJS.BufferAttribute> geometry.attributes.color;
    this.vertexCol.usage = ThreeJS.DynamicDrawUsage;

    let material = new ThreeJS.LineBasicMaterial({
      vertexColors: ThreeJS.VertexColors,
      fog: false
    })

    let grid = new ThreeJS.LineSegments(geometry, material);
    this.gridObject = new ThreeJS.Object3D();
    this.gridObject = grid;
  }

  public Object(){
    return this.gridObject;
  }

  public Update(camera: ThreeJS.Camera){
    let relDistance = camera.position.distanceTo(this.gridObject.position);
    let color: ThreeJS.Color;
    let halfSize = this.linesNumber * this.spacing / 2;
    for(let idx = 0, offset = - halfSize, idc = 0; idx <= this.linesNumber; idx++, offset += this.spacing){
      if (idx % this.majorLinesEveryNth != 0 || idx != 0 || idx != this.linesNumber){
        color = this.gridColor;
        let vCol = this.vertexCol.array;


      }
    }
  }

}
