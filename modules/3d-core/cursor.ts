import * as ThreeJS from "three"

import { LineSegments } from "three";
import { VertexColors } from "three";
import { LineBasicMaterial } from "three";
import { Float32BufferAttribute } from "three";
import { BufferGeometry } from "three";
import { Color } from "three";


export class Cursor{
  private vertices: Array<number>;
  private colors: Array<number>;
  private position: ThreeJS.Vector3;
  private geometry: BufferGeometry;
  private lineSegments: LineSegments;
  private parentTransform: ThreeJS.Object3D;
  private vertexPos: ThreeJS.BufferAttribute;
  private camera: ThreeJS.Camera;
  public constructor(camera: ThreeJS.Camera){
    this.camera = camera;
    this.position = new ThreeJS.Vector3(0.0, 0.0, 0.0);

    this.vertices = new Array();
    this.colors = new Array();
    let color;

    this.vertices.push(-10.0, 0.0, 0.0, 10.0, 0.0, 0.0);
    color = new Color(0x239B56);
    color.toArray(this.colors, 0);
    color.toArray(this.colors, 3);

    this.vertices.push(0.0, -10.0, 0.0, 0.0, 10.0, 0.0);
    color = new Color(0x2874A6);
    color.toArray(this.colors, 6);
    color.toArray(this.colors, 9);

    this.vertices.push(0.0, 0.0, -10.0, 0.0, 0.0, 10.0);
    color = new Color(0xB03A2E);
    color.toArray(this.colors, 12);
    color.toArray(this.colors, 15);

    this.geometry = new BufferGeometry();
    this.geometry.setAttribute( 'position', new Float32BufferAttribute(this.vertices, 3));
  	this.geometry.setAttribute( 'color', new Float32BufferAttribute(this.colors, 3));

    this.vertexPos = <ThreeJS.BufferAttribute> this.geometry.attributes.position;
    this.vertexPos.usage = ThreeJS.DynamicDrawUsage;

    let material = new LineBasicMaterial({
      vertexColors: VertexColors,
      fog: false
    });

    this.lineSegments = new LineSegments(this.geometry, material);

    this.parentTransform = new ThreeJS.Object3D();
    this.parentTransform.add(this.lineSegments);
  }

  public Object(){
    return this.parentTransform;
  }

  public SetPosition(position: ThreeJS.Vector3){
    this.position = position;
  }

  public Hide(){
    this.parentTransform.visible = false;
  }

  public Show(){
    this.parentTransform.visible = true;
  }

  public Update(){
    this.parentTransform.position.set(this.position.x, this.position.y, this.position.z);
    this.camera.position.distanceTo(this.position);

    let relSize = this.camera.position.distanceTo(this.position) * 0.05;
    let idk = 0;
    this.vertexPos.setX(idk, -relSize); idk += 2;
    this.vertexPos.setY(idk, -relSize); idk += 2;
    this.vertexPos.setZ(idk, -relSize); idk = 1;

    this.vertexPos.setX(idk, relSize); idk += 2;
    this.vertexPos.setY(idk, relSize); idk += 2;
    this.vertexPos.setZ(idk, relSize); idk = 1;
    this.vertexPos.needsUpdate = true;
  }
}
