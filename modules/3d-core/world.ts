import * as ThreeJS from "three"

export class World{
  private scene: ThreeJS.Scene;
  private camera: ThreeJS.PerspectiveCamera | ThreeJS.OrthographicCamera;
  private perspective: ThreeJS.PerspectiveCamera;
  private orthographic: ThreeJS.OrthographicCamera;
  private renderer: ThreeJS.WebGLRenderer;

  public constructor(context?: HTMLElement){
    this.scene = new ThreeJS.Scene();
    //this.scene.background = new ThreeJS.Color(0x282c34);
    this.perspective = new ThreeJS.PerspectiveCamera(50, window.innerWidth / window.innerHeight, 1e-6, 1e27);
    this.orthographic = new ThreeJS.OrthographicCamera(-10.0, 10.0, 10.0, -10.0, 1e-6, 1e27);
    this.camera = this.perspective;
    this.renderer = new ThreeJS.WebGLRenderer({
      antialias: true,
      logarithmicDepthBuffer: true,
    });

    this.renderer.setSize(window.innerWidth, window.innerHeight);
    if (context)
      context.appendChild(this.renderer.domElement);
    else
      document.body.appendChild(this.renderer.domElement);

    this.scene.fog = new ThreeJS.FogExp2( 0x282c34, 0.0028 );
    this.renderer.setClearColor(this.scene.fog.color, 1);

    window.addEventListener("resize", (e) => {
      if (this.camera as ThreeJS.PerspectiveCamera){
        this.camera = <ThreeJS.PerspectiveCamera> this.camera;
        this.camera.aspect = window.innerWidth / window.innerHeight;
      }
      this.camera.updateProjectionMatrix();
      this.renderer.setSize(window.innerWidth, window.innerHeight);
    });

  }

  public Scene(){
    return this.scene;
  }

  public Renderer(){
    return this.renderer;
  }

  public Camera(){
    return this.camera;
  }

}
