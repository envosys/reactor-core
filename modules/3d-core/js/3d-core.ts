import * as ThreeJS from "three"
import {World} from "../world"
import {Grid} from "../grid"
import {Cursor} from "../cursor"
import {Curve} from "../curve"
let OrbitControls = require('three-orbit-controls')(ThreeJS)
import {IpcConsole} from "../../window-tools/ipc-console";

let console = new IpcConsole(__filename);

console.log("Message from 3D Engine");

let PWorld = new World();
let renderer = PWorld.Renderer();
let scene = PWorld.Scene();
let camera = PWorld.Camera();
let mouse = new ThreeJS.Vector2();
let controls = new OrbitControls(camera, renderer.domElement);

renderer.domElement.addEventListener("mousemove", (e)=>{
  e.preventDefault();

  mouse.x = ( e.clientX / window.innerWidth ) * 2 - 1;
  mouse.y = - ( e.clientY / window.innerHeight ) * 2 + 1;
});

renderer.domElement.addEventListener("mousedown", (e)=>{
  worldCursor.Hide();
})

renderer.domElement.addEventListener("mouseout", (e)=>{
  worldCursor.Hide();
})

renderer.domElement.addEventListener("mouseup", (e)=>{
  worldCursor.Show();
})

renderer.domElement.addEventListener("mouseover", (e)=>{
  worldCursor.Show();
})

renderer.domElement.addEventListener("click", (e)=>{
  e.preventDefault();
  //console.log("Click");
});

//World Grid Test
let worldGrid = new Grid();
scene.add(worldGrid.Object());


//Curve Test
let curve = new Curve();
curve.AddPoint(new ThreeJS.Vector3(0, 0, 0));
curve.AddPoint(new ThreeJS.Vector3(10, 0, 10));
curve.AddPoint(new ThreeJS.Vector3(20, 0, 0));
curve.Calculate();
scene.add(curve);

let plane = new ThreeJS.Mesh(new ThreeJS.PlaneBufferGeometry(50000, 50000, 10, 10),
                             new ThreeJS.MeshBasicMaterial({
                              color: 0x248f24,
                              alphaTest: 0,
                              visible: false,
                              side: ThreeJS.DoubleSide
                            }));

plane.setRotationFromAxisAngle(new ThreeJS.Vector3(1.0, 0.0, 0.0), ThreeJS.MathUtils.degToRad(-90.0));
scene.add(plane);


let worldCursor = new Cursor(camera);
scene.add(worldCursor.Object());
worldCursor.SetPosition(new ThreeJS.Vector3(10.0, 10.0, 10.0));
worldCursor.Hide();

camera.position.set(0.0, 10.0, -50.0);
controls.update();

let raycaster = new ThreeJS.Raycaster();

function animate(){
  requestAnimationFrame(animate);

  raycaster.setFromCamera(mouse, camera);
  let intersections = raycaster.intersectObject(plane);
  let intersection = ( intersections.length ) > 0 ? intersections[ 0 ] : null;
  if (intersection)
    worldCursor.SetPosition(intersection.point);

  worldCursor.Update();
  controls.update();

  renderer.render(scene, camera);
}

animate();
