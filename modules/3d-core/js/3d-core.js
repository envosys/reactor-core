"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ThreeJS = __importStar(require("three"));
var world_1 = require("../world");
var grid_1 = require("../grid");
var cursor_1 = require("../cursor");
var curve_1 = require("../curve");
var OrbitControls = require('three-orbit-controls')(ThreeJS);
var ipc_console_1 = require("../../window-tools/ipc-console");
var console = new ipc_console_1.IpcConsole(__filename);
console.log("Message from 3D Engine");
var PWorld = new world_1.World();
var renderer = PWorld.Renderer();
var scene = PWorld.Scene();
var camera = PWorld.Camera();
var mouse = new ThreeJS.Vector2();
var controls = new OrbitControls(camera, renderer.domElement);
renderer.domElement.addEventListener("mousemove", function (e) {
    e.preventDefault();
    mouse.x = (e.clientX / window.innerWidth) * 2 - 1;
    mouse.y = -(e.clientY / window.innerHeight) * 2 + 1;
});
renderer.domElement.addEventListener("mousedown", function (e) {
    worldCursor.Hide();
});
renderer.domElement.addEventListener("mouseout", function (e) {
    worldCursor.Hide();
});
renderer.domElement.addEventListener("mouseup", function (e) {
    worldCursor.Show();
});
renderer.domElement.addEventListener("mouseover", function (e) {
    worldCursor.Show();
});
renderer.domElement.addEventListener("click", function (e) {
    e.preventDefault();
    //console.log("Click");
});
//World Grid Test
var worldGrid = new grid_1.Grid();
scene.add(worldGrid.Object());
//Curve Test
var curve = new curve_1.Curve();
curve.AddPoint(new ThreeJS.Vector3(0, 0, 0));
curve.AddPoint(new ThreeJS.Vector3(10, 0, 10));
curve.AddPoint(new ThreeJS.Vector3(20, 0, 0));
curve.Calculate();
scene.add(curve);
var plane = new ThreeJS.Mesh(new ThreeJS.PlaneBufferGeometry(50000, 50000, 10, 10), new ThreeJS.MeshBasicMaterial({
    color: 0x248f24,
    alphaTest: 0,
    visible: false,
    side: ThreeJS.DoubleSide
}));
plane.setRotationFromAxisAngle(new ThreeJS.Vector3(1.0, 0.0, 0.0), ThreeJS.MathUtils.degToRad(-90.0));
scene.add(plane);
var worldCursor = new cursor_1.Cursor(camera);
scene.add(worldCursor.Object());
worldCursor.SetPosition(new ThreeJS.Vector3(10.0, 10.0, 10.0));
worldCursor.Hide();
camera.position.set(0.0, 10.0, -50.0);
controls.update();
var raycaster = new ThreeJS.Raycaster();
function animate() {
    requestAnimationFrame(animate);
    raycaster.setFromCamera(mouse, camera);
    var intersections = raycaster.intersectObject(plane);
    var intersection = (intersections.length) > 0 ? intersections[0] : null;
    if (intersection)
        worldCursor.SetPosition(intersection.point);
    worldCursor.Update();
    controls.update();
    renderer.render(scene, camera);
}
animate();
