import {ProcessView} from "../window-tools/process-view"
import {View} from "../window-tools/view"
import {Layout} from "../window-tools/layout"
import {HTMLRepresentation} from "../window-tools/dom-object";

export class Viewport extends ProcessView{
  public constructor(parentView: View){
    super(parentView, "../../3d-core/html/3d-core.html");
  }

}
