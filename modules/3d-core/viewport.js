"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var process_view_1 = require("../window-tools/process-view");
var Viewport = /** @class */ (function (_super) {
    __extends(Viewport, _super);
    function Viewport(parentView) {
        return _super.call(this, parentView, "../../3d-core/html/3d-core.html") || this;
    }
    return Viewport;
}(process_view_1.ProcessView));
exports.Viewport = Viewport;
